import { atom, selector } from "recoil";
import { recoilPersist } from 'recoil-persist'

const { persistAtom } = recoilPersist()

export const loading = atom({
    key: 'loading',
    default: false
})

export const userData = atom({
    key: 'NavbarComponent',
    default: [],
    effects_UNSTABLE: [persistAtom],
})

// export const postList = atom({
//     key: "id",
//     default: [
//         {
//             id: 1,
//             username: "John Doe",
//             avatar: "/sample-2.png",
//             quote: "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Natus error, dignissimos minus laudantium ab quam nemo sapiente id. Praesentium, laborum officiis.",
//             attachments: ["/sample-6.png"],
//             likes: 20,
//             shares: 0,
//             comments: [],
//         },
//         {
//             id: 2,
//             username: "Gordon Ramsey",
//             avatar: "/sample-1.png",
//             quote: "Have a nice day",
//             attachments: ["/sample-1.png", "/sample-2.png"],
//             likes: 12,
//             shares: 2,
//             comments: [
//                 {
//                     username: "Derry S",
//                     avatar: "/sample-4.png",
//                     jabatan: "Vice President",
//                     quote: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Vel rerum illo magnam ratione quae. At vel ea totam rerum dignissimos molestiae reprehenderit earum unde repellendus consectetur nulla saepe porro facilis nostrum esse fuga, est omnis",
//                     loves: 4,
//                     reply: [
//                         {
//                             username: "Saksa",
//                             avatar: "/sample-5.png",
//                             jabatan: "Karyawan Biasa",
//                             quote: "lorem ipsum dolore, kartolo pedot janji diam, selamaya wksaiesa constem context sapooerer fuga raja tarausa",
//                         },
//                     ],
//                 },
//             ],
//         },
//     ],
// });

export const contentFeeds = atom({
    key: "content",
    default: null
});
