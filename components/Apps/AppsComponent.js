import { Tab } from "@headlessui/react";
import {
    DesktopComputerIcon,
    DeviceMobileIcon,
    SearchIcon,
    StarIcon,
    UserIcon,
    HeartIcon,
} from "@heroicons/react/solid";

import {
    StarIcon as StarIconOutline,
    HeartIcon as HeartIconOutline,
} from "@heroicons/react/outline";
import { useEffect, useState } from "react";
import Link from "next/link";
import { useAuth } from "../../hooks/auth";
import axios from "axios";
import request from "../../utils/request";
import { parseCookies } from "nookies";
import { useRouter } from "next/router";
import { mutate } from "swr";
import Loader from "../Loader";

function classNames(...classes) {
    return classes.filter(Boolean).join(" ");
}

export default function AppsComponent({ apps:data, categories }) {
    const router = useRouter()
    // const [workspace] = useState([
    //     ...apps.filter((app) => app.is_workspace == true),
    // ]);
    const [inputSearch, setInputSearch] = useState('')
    const [apps, setApps] = useState([])
    // const [popularApps] = useState([...apps.filter((app) => app.rate > -1)]);
    // const [webApps] = useState([...apps.filter((app) => app.category.name == 'Web Apps')])
    // const [androidApps] = useState([...apps.filter((app) => app.category.name == 'Android')])
    // const [iosApps] = useState([...apps.filter((app) => app.category.name == 'IOS')])
    const [loadingData, setLoadingData] = useState(false)
    const cookies = parseCookies();
    const jwt = { cookies };

    useEffect(() => {
        setApps(data)
    }, [data])

    const handleWorkspace = async (id) => {
        await axios({
            method: "POST",
            url: request.postWorkspace+id,
            headers: {
                Authorization: `Bearer ${jwt.cookies.token}`,
            },
        })
        
        mutate(request.filterApps)
    }

    const handleFilterApps = async (e) => {
        let timeout = null
        clearTimeout(timeout)
        setLoadingData(true)


        timeout = setTimeout(function () {
            setInputSearch(e.target.value)
                axios.get(request.filterApps+inputSearch, {
                    headers: {
                        Authorization: `Bearer ${jwt.cookies.token}`,
                    },
                }).then(res => setApps(res.data.data))
                .finally(() => setLoadingData(false))
        }, 1000);

    }

    return (
        <Tab.Group as={`div`} className="">
            <Tab.List className="bg-white flex gap-3 items-center justify-between border border-[#E9E9ED] shadow-md rounded-xl pl-5 pr-2">
                <div className="apps__tab flex gap-3 overflow-x-auto sm:overflow-x-hidden">
                    {categories.map((category, index) => (
                        <Tab
                            key={index}
                            className={({ selected }) =>
                                classNames(
                                    "py-[.8rem] px-2 text-[.7rem] lg:text-sm font-medium duration-300 ease-linear",
                                    "hover:text-[#002DF5] outline-none min-w-max",
                                    selected
                                        ? "border-b-2 border-[#002DF5] text-[#002DF5]"
                                        : "text-[#818598] border-b-2 border-transparent"
                                )
                            }
                        >
                            <span>{category.name}</span>
                        </Tab>
                    ))}
                </div>

                <div className="hidden sm:block input__groups">
                    <div className="input__groups-icon">
                        <SearchIcon className="input__icon" />
                    </div>
                    <input
                        type="text"
                        id="search"
                        className="input__group sm:w-48 md:w-36 lg:w-64"
                        placeholder="Seacrh here ..."
                        value={inputSearch}
                        onKeyUp={(e) => handleFilterApps(e)}
                        onChange={(e) => setInputSearch(e.target.value)}
                    />
                </div>
            </Tab.List>
            {loadingData && (
                <div className="relative">
                    <div className="absolute top-2 right-[50%]">
                        <Loader />
                    </div>
                </div>
            )}
            <Tab.Panels className="mt-6 min-h-[90vh]">
                <Tab.Panel className={classNames("focus:outline-none")}>
                    <div>
                        <div>
                            <h3 className="font-bold text-[#ABAEBA] sm:text-[.9rem] md:text-[1rem] lg:text-lg mb-4">
                                My Workspaces
                            </h3>

                            <div className="grid grid-cols-[_repeat(2,_minmax(80px,_1fr))] sm:grid-cols-[_repeat(3,_minmax(80px,_1fr))] md:grid-cols-[_repeat(3,_minmax(120px,_1fr))] lg:grid-cols-[_repeat(4,_minmax(120px,_1fr))] gap-3 sm:gap-5 auto-rows-[_1fr]">
                                {apps.map(
                                    (app, idx) =>
                                        app.is_workspace == true && (
                                            <div key={idx}>
                                                <div
                                                    className={
                                                        "flex bg-white flex-col rounded-xl hover:bg-[#E2EEFF] duration-200 border border-slate-200 relative"
                                                    }
                                                >
                                                    <div className="bg-white w-full rounded-t-xl aspect-square mb-[1rem]">
                                                        <img
                                                            className="object-contain w-full rounded-t-lg h-[130px]"
                                                            layout="fill"
                                                            src={app.icon}
                                                        />
                                                    </div>

                                                    <div
                                                        className={
                                                            `cursor-pointer love w-fit rounded-full p-[.25rem] lg:p-[.35rem] hover:bg-red-500 duration-200 absolute top-0 right-0 mt-[.35rem] mr-[.35rem] lg:mt-[.5rem] lg:mr-[.5rem] ` +
                                                            (app.is_workspace ==
                                                            true
                                                                ? "bg-red-500"
                                                                : "bg-white")
                                                        }
                                                        onClick={() =>
                                                            handleWorkspace(
                                                                app.id
                                                            )
                                                        }
                                                    >
                                                        {app.is_workspace ==
                                                            1 && (
                                                            <HeartIcon className="w-3 lg:w-4 text-white" />
                                                        )}
                                                        {app.is_workspace ==
                                                            0 && (
                                                            <HeartIconOutline className="love__icon w-3 lg:w-4 text-slate-400 duration-200" />
                                                        )}
                                                    </div>
                                                    <Link
                                                        href={`/apps/${app.slug}`}
                                                        className="cursor-pointer"
                                                    >
                                                        <div className="px-4 mb-[1rem] flex flex-col gap-1 cursor-pointer">
                                                            <p className="text-[.7rem] lg:text-[.8rem] font-bold truncate">
                                                                {app.name}
                                                            </p>
                                                            <div className="flex flex-row gap-1 text-[.6rem] sm:text-[.75rem] items-center text-[#818598]">
                                                                {app.category
                                                                    .name ==
                                                                    "Web Apps" && (
                                                                    <DesktopComputerIcon className="w-[.65rem] sm:w-[.75rem] lg:w-[.85rem]" />
                                                                )}

                                                                {app.category
                                                                    .name !=
                                                                    "Web Apps" && (
                                                                    <DeviceMobileIcon className="w-[.65rem] sm:w-[.75rem] lg:w-[.85rem]" />
                                                                )}

                                                                <span>
                                                                    {
                                                                        app
                                                                            .category
                                                                            .name
                                                                    }
                                                                </span>
                                                            </div>
                                                            <div className="flex flex-row gap-3 justify-between">
                                                                <div className="flex flex-row">
                                                                    {(function () {
                                                                        const view =
                                                                            [];
                                                                        for (
                                                                            let i = 1;
                                                                            i <=
                                                                            5;
                                                                            i++
                                                                        ) {
                                                                            if (
                                                                                i <=
                                                                                app.rate
                                                                            )
                                                                                view.push(
                                                                                    <StarIcon
                                                                                        key={
                                                                                            i
                                                                                        }
                                                                                        className="w-[.7rem] sm:w-[.8rem] lg:w-[1rem] text-[#FAA705]"
                                                                                    />
                                                                                );
                                                                            else
                                                                                view.push(
                                                                                    <StarIconOutline
                                                                                        key={
                                                                                            i
                                                                                        }
                                                                                        className="w-[.6rem] sm:w-[.7rem] lg:w-[.9rem] text-[#818598]"
                                                                                    />
                                                                                );
                                                                        }
                                                                        return view;
                                                                    })()}
                                                                </div>

                                                                <div className="flex flex-row text-[#818598] items-center">
                                                                    <span className="text-[.6rem] sm:text-[.7rem] lg:text-[.75rem] pr-1">
                                                                        {
                                                                            app.review
                                                                        }
                                                                    </span>
                                                                    <UserIcon className="w-[.65rem] sm:w-[.75rem] lg:w-[.85rem]" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </Link>
                                                </div>
                                            </div>
                                        )
                                )}
                            </div>

                            <h3 className="font-bold text-[#ABAEBA] text-[.9rem] md:text-[1rem] lg:text-lg mb-4 mt-10">
                                Popular Apps
                            </h3>
                            <div className="grid grid-cols-[_repeat(2,_minmax(80px,_1fr))] sm:grid-cols-[_repeat(3,_minmax(80px,_1fr))] md:grid-cols-[_repeat(3,_minmax(120px,_1fr))] lg:grid-cols-[_repeat(4,_minmax(120px,_1fr))] gap-3 sm:gap-5 auto-rows-[_1fr]">
                                {apps.map(
                                    (app, idx) =>
                                        app.rate > 3 && (
                                            <div key={idx}>
                                                <div
                                                    className={
                                                        "flex flex-col rounded-xl hover:bg-[#E2EEFF] duration-200 border border-slate-200 relative"
                                                    }
                                                >
                                                    <div className="bg-white w-full rounded-t-xl aspect-square mb-[1rem]">
                                                        <img
                                                            className="object-contain w-full rounded-t-lg h-[130px]"
                                                            layout="fill"
                                                            src={app.icon}
                                                        />
                                                    </div>

                                                    <div
                                                        className={
                                                            `cursor-pointer love w-fit rounded-full p-[.25rem] lg:p-[.35rem] hover:bg-red-500 duration-200 absolute top-0 right-0 mt-[.35rem] mr-[.35rem] lg:mt-[.5rem] lg:mr-[.5rem] ` +
                                                            (app.is_workspace ==
                                                            1
                                                                ? "bg-red-500"
                                                                : "bg-white")
                                                        }
                                                        onClick={() =>
                                                            handleWorkspace(
                                                                app.id
                                                            )
                                                        }
                                                    >
                                                        {app.is_workspace ==
                                                            1 && (
                                                            <HeartIcon className="w-3 lg:w-4 text-white" />
                                                        )}
                                                        {app.is_workspace ==
                                                            0 && (
                                                            <HeartIconOutline className="love__icon w-3 lg:w-4 text-slate-400 duration-200" />
                                                        )}
                                                    </div>
                                                    <Link
                                                        href={`/apps/${app.slug}`}
                                                    >
                                                        <div className="px-4 mb-[1rem] flex flex-col gap-1 cursor-pointer">
                                                            <p className="text-[.7rem] lg:text-[.8rem] font-bold truncate">
                                                                {app.name}
                                                            </p>
                                                            <div className="flex flex-row gap-1 text-[.6rem] sm:text-[.75rem] items-center text-[#818598]">
                                                                {app.category
                                                                    .name ==
                                                                    "Web Apps" && (
                                                                    <DesktopComputerIcon className="w-[.65rem] sm:w-[.75rem] lg:w-[.85rem]" />
                                                                )}

                                                                {app.category
                                                                    .name !=
                                                                    "Web Apps" && (
                                                                    <DeviceMobileIcon className="w-[.65rem] sm:w-[.75rem] lg:w-[.85rem]" />
                                                                )}

                                                                <span>
                                                                    {
                                                                        app
                                                                            .category
                                                                            .name
                                                                    }
                                                                </span>
                                                            </div>
                                                            <div className="flex flex-row gap-3 justify-between">
                                                                <div className="flex flex-row">
                                                                    {(function () {
                                                                        const view =
                                                                            [];
                                                                        for (
                                                                            let i = 1;
                                                                            i <=
                                                                            5;
                                                                            i++
                                                                        ) {
                                                                            if (
                                                                                i <=
                                                                                app.rate
                                                                            )
                                                                                view.push(
                                                                                    <StarIcon
                                                                                        key={
                                                                                            i
                                                                                        }
                                                                                        className="w-[.7rem] sm:w-[.8rem] lg:w-[1rem] text-[#FAA705]"
                                                                                    />
                                                                                );
                                                                            else
                                                                                view.push(
                                                                                    <StarIconOutline
                                                                                        key={
                                                                                            i
                                                                                        }
                                                                                        className="w-[.6rem] sm:w-[.7rem] lg:w-[.9rem] text-[#818598]"
                                                                                    />
                                                                                );
                                                                        }
                                                                        return view;
                                                                    })()}
                                                                </div>

                                                                <div className="flex flex-row text-[#818598] items-center">
                                                                    <span className="text-[.6rem] sm:text-[.7rem] lg:text-[.75rem] pr-1">
                                                                        {
                                                                            app.review
                                                                        }
                                                                    </span>
                                                                    <UserIcon className="w-[.65rem] sm:w-[.75rem] lg:w-[.85rem]" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </Link>
                                                </div>
                                            </div>
                                        )
                                )}
                            </div>

                            <h3 className="font-bold text-[#ABAEBA] md:text-[1rem] lg:text-lg mb-4 mt-10">
                                All Apps
                            </h3>
                            <div className="grid grid-cols-[_repeat(2,_minmax(80px,_1fr))] sm:grid-cols-[_repeat(3,_minmax(80px,_1fr))] md:grid-cols-[_repeat(3,_minmax(120px,_1fr))] lg:grid-cols-[_repeat(4,_minmax(120px,_1fr))] gap-3 sm:gap-5 auto-rows-[_1fr]">
                                {apps.map((app, idx) => (
                                    <div key={idx}>
                                        <div
                                            className={
                                                "flex flex-col rounded-xl hover:bg-[#E2EEFF] duration-200 border border-slate-200 relative"
                                            }
                                        >
                                            <div className="bg-white w-full h-full rounded-t-xl aspect-square mb-[1rem]">
                                                <img
                                                    className="object-contain w-full h-full rounded-t-lg max-h-[130px]"
                                                    layout="fill"
                                                    src={app.icon}
                                                />
                                            </div>

                                            <div
                                                className={
                                                    `cursor-pointer love w-fit rounded-full p-[.25rem] lg:p-[.35rem] hover:bg-red-500 duration-200 absolute top-0 right-0 mt-[.35rem] mr-[.35rem] lg:mt-[.5rem] lg:mr-[.5rem] ` +
                                                    (app.is_workspace == 1
                                                        ? "bg-red-500"
                                                        : "bg-white")
                                                }
                                                onClick={() =>
                                                    handleWorkspace(app.id)
                                                }
                                            >
                                                {app.is_workspace == 1 && (
                                                    <HeartIcon className="w-3 lg:w-4 text-white" />
                                                )}
                                                {app.is_workspace == 0 && (
                                                    <HeartIconOutline className="love__icon w-3 lg:w-4 text-slate-400 duration-200" />
                                                )}
                                            </div>

                                            <Link href={`/apps/${app.slug}`}>
                                                <div className="px-4 mb-[1rem] flex flex-col gap-1 cursor-pointer">
                                                    <p className="text-[.7rem] lg:text-[.8rem] font-bold truncate">
                                                        {app.name}
                                                    </p>
                                                    <div className="flex flex-row gap-1 text-[.6rem] sm:text-[.75rem] items-center text-[#818598]">
                                                        {app.category.name ==
                                                            "Web Apps" && (
                                                            <DesktopComputerIcon className="w-[.65rem] sm:w-[.75rem] lg:w-[.85rem]" />
                                                        )}

                                                        {app.category.name !=
                                                            "Web Apps" && (
                                                            <DeviceMobileIcon className="w-[.65rem] sm:w-[.75rem] lg:w-[.85rem]" />
                                                        )}

                                                        <span>
                                                            {app.category.name}
                                                        </span>
                                                    </div>
                                                    <div className="flex flex-row gap-3 justify-between">
                                                        <div className="flex flex-row">
                                                            {(function () {
                                                                const view = [];
                                                                for (
                                                                    let i = 1;
                                                                    i <= 5;
                                                                    i++
                                                                ) {
                                                                    if (
                                                                        i <=
                                                                        app.rate
                                                                    )
                                                                        view.push(
                                                                            <StarIcon
                                                                                key={
                                                                                    i
                                                                                }
                                                                                className="w-[.7rem] sm:w-[.8rem] lg:w-[1rem] text-[#FAA705]"
                                                                            />
                                                                        );
                                                                    else
                                                                        view.push(
                                                                            <StarIconOutline
                                                                                key={
                                                                                    i
                                                                                }
                                                                                className="w-[.6rem] sm:w-[.7rem] lg:w-[.9rem] text-[#818598]"
                                                                            />
                                                                        );
                                                                }
                                                                return view;
                                                            })()}
                                                        </div>

                                                        <div className="flex flex-row text-[#818598] items-center">
                                                            <span className="text-[.6rem] sm:text-[.7rem] lg:text-[.75rem] pr-1">
                                                                {app.review}
                                                            </span>
                                                            <UserIcon className="w-[.65rem] sm:w-[.75rem] lg:w-[.85rem]" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </Link>
                                        </div>
                                    </div>
                                ))}
                            </div>
                        </div>

                        {/* {idx != 0 && (
                                <div className="grid grid-cols-[_repeat(2,_minmax(80px,_1fr))] sm:grid-cols-[_repeat(3,_minmax(80px,_1fr))] md:grid-cols-[_repeat(3,_minmax(120px,_1fr))] lg:grid-cols-[_repeat(4,_minmax(120px,_1fr))] gap-3 sm:gap-5 auto-rows-[_1fr]">
                                    {posts.map((post) => {
                                        return (
                                            <Link href={`/apps/detail`}>
                                                <div
                                                    className={
                                                        "flex flex-col rounded-xl hover:bg-[#E2EEFF] hover:scale-105 duration-200 border border-slate-200 cursor-pointer relative"
                                                    }
                                                >
                                                    <div className="bg-white w-full rounded-t-xl aspect-square mb-[1rem]">
                                                        <img
                                                            className="object-cover w-full h-full rounded-t-lg"
                                                            layout="fill"
                                                            src={
                                                                `/images/apps/` +
                                                                post.image
                                                            }
                                                        />
                                                    </div>

                                                    <div
                                                        className={
                                                            `love w-fit rounded-full p-[.25rem] lg:p-[.35rem] hover:bg-red-500 duration-200 absolute top-0 right-0 mt-[.35rem] mr-[.35rem] lg:mt-[.5rem] lg:mr-[.5rem] ` +
                                                            (post.isLike == 1
                                                                ? "bg-red-500"
                                                                : "bg-white")
                                                        }
                                                    >
                                                        {post.isLike == 1 && (
                                                            <HeartIcon className="w-3 lg:w-4 text-white" />
                                                        )}
                                                        {post.isLike == 0 && (
                                                            <HeartIconOutline className="love__icon w-3 lg:w-4 text-slate-400 duration-200" />
                                                        )}
                                                    </div>

                                                    <div className="px-4 mb-[1rem] flex flex-col gap-1">
                                                        <p className="text-[.7rem] lg:text-[.8rem] font-bold truncate">
                                                            {post.title}
                                                        </p>
                                                        <div className="flex flex-row gap-1 text-[.6rem] sm:text-[.75rem] items-center text-[#818598]">
                                                            {post.category ==
                                                                "Web App" && (
                                                                <DesktopComputerIcon className="w-[.65rem] sm:w-[.75rem] lg:w-[.85rem]" />
                                                            )}

                                                            {post.category !=
                                                                "Web App" && (
                                                                <DeviceMobileIcon className="w-[.65rem] sm:w-[.75rem] lg:w-[.85rem]" />
                                                            )}

                                                            <span>
                                                                {post.category}
                                                            </span>
                                                        </div>
                                                        <div className="flex flex-row gap-3 justify-between">
                                                            <div className="flex flex-row">
                                                                {(function () {
                                                                    const view =
                                                                        [];
                                                                    for (
                                                                        let i = 1;
                                                                        i <= 5;
                                                                        i++
                                                                    ) {
                                                                        if (
                                                                            i <=
                                                                            post.rate
                                                                        )
                                                                            view.push(
                                                                                <StarIcon className="w-[.7rem] sm:w-[.8rem] lg:w-[1rem] text-[#FAA705]" />
                                                                            );
                                                                        else
                                                                            view.push(
                                                                                <StarIconOutline className="w-[.6rem] sm:w-[.7rem] lg:w-[.9rem] text-[#818598]" />
                                                                            );
                                                                    }
                                                                    return view;
                                                                })()}
                                                            </div>

                                                            <div className="flex flex-row text-[#818598] items-center">
                                                                <span className="text-[.6rem] sm:text-[.7rem] lg:text-[.75rem] pr-1">
                                                                    {
                                                                        post.comment
                                                                    }
                                                                </span>
                                                                <UserIcon className="w-[.65rem] sm:w-[.75rem] lg:w-[.85rem]" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </Link>
                                        );
                                    })}
                                </div>
                            )}

                            {idx != 0 && posts.length < 1 && (
                                <div className="w-full flex justify-center mt-4">
                                    <img
                                        layout="fill"
                                        src="/images/apps/apps-notfound.png"
                                        className="w-72"
                                    />
                                </div>
                            )} */}
                    </div>
                </Tab.Panel>
                <Tab.Panel>
                    <div className="grid grid-cols-[_repeat(2,_minmax(80px,_1fr))] sm:grid-cols-[_repeat(3,_minmax(80px,_1fr))] md:grid-cols-[_repeat(3,_minmax(120px,_1fr))] lg:grid-cols-[_repeat(4,_minmax(120px,_1fr))] gap-3 sm:gap-5 auto-rows-[_1fr]">
                        {apps.length > 0 ? (
                            apps.map(
                                (app) =>
                                    app.category.name == "Web Apps" && (
                                        <Link
                                            href={`/apps/${app.slug}`}
                                            key={app.id}
                                        >
                                            <div
                                                className={
                                                    "flex bg-white flex-col rounded-xl hover:bg-[#E2EEFF] hover:scale-105 duration-200 border border-slate-200 cursor-pointer relative"
                                                }
                                            >
                                                <div className="bg-white w-full rounded-t-xl aspect-square mb-[1rem]">
                                                    <img
                                                        className="object-contain w-full h-full rounded-t-lg"
                                                        layout="fill"
                                                        src={app.icon}
                                                    />
                                                </div>

                                                <div
                                                    className={
                                                        `love w-fit rounded-full p-[.25rem] lg:p-[.35rem] hover:bg-red-500 duration-200 absolute top-0 right-0 mt-[.35rem] mr-[.35rem] lg:mt-[.5rem] lg:mr-[.5rem] ` +
                                                        (app.is_workspace ==
                                                        true
                                                            ? "bg-red-500"
                                                            : "bg-white")
                                                    }
                                                >
                                                    {app.is_workspace == 1 && (
                                                        <HeartIcon className="w-3 lg:w-4 text-white" />
                                                    )}
                                                    {app.is_workspace == 0 && (
                                                        <HeartIconOutline className="love__icon w-3 lg:w-4 text-slate-400 duration-200" />
                                                    )}
                                                </div>

                                                <div className="px-4 mb-[1rem] flex flex-col gap-1">
                                                    <p className="text-[.7rem] lg:text-[.8rem] font-bold truncate">
                                                        {app.name}
                                                    </p>
                                                    <div className="flex flex-row gap-1 text-[.6rem] sm:text-[.75rem] items-center text-[#818598]">
                                                        {app.category.name ==
                                                            "Web Apps" && (
                                                            <DesktopComputerIcon className="w-[.65rem] sm:w-[.75rem] lg:w-[.85rem]" />
                                                        )}

                                                        {app.category.name !=
                                                            "Web Apps" && (
                                                            <DeviceMobileIcon className="w-[.65rem] sm:w-[.75rem] lg:w-[.85rem]" />
                                                        )}

                                                        <span>
                                                            {app.category.name}
                                                        </span>
                                                    </div>
                                                    <div className="flex flex-row gap-3 justify-between">
                                                        <div className="flex flex-row">
                                                            {(function () {
                                                                const view = [];
                                                                for (
                                                                    let i = 1;
                                                                    i <= 5;
                                                                    i++
                                                                ) {
                                                                    if (
                                                                        i <=
                                                                        app.rate
                                                                    )
                                                                        view.push(
                                                                            <StarIcon
                                                                                key={
                                                                                    i
                                                                                }
                                                                                className="w-[.7rem] sm:w-[.8rem] lg:w-[1rem] text-[#FAA705]"
                                                                            />
                                                                        );
                                                                    else
                                                                        view.push(
                                                                            <StarIconOutline
                                                                                key={
                                                                                    i
                                                                                }
                                                                                className="w-[.6rem] sm:w-[.7rem] lg:w-[.9rem] text-[#818598]"
                                                                            />
                                                                        );
                                                                }
                                                                return view;
                                                            })()}
                                                        </div>

                                                        <div className="flex flex-row text-[#818598] items-center">
                                                            <span className="text-[.6rem] sm:text-[.7rem] lg:text-[.75rem] pr-1">
                                                                {app.review}
                                                            </span>
                                                            <UserIcon className="w-[.65rem] sm:w-[.75rem] lg:w-[.85rem]" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </Link>
                                    )
                            )
                        ) : (
                            <span className="text-slate-400">No Data</span>
                        )}
                    </div>
                </Tab.Panel>
                <Tab.Panel>
                    <div className="grid grid-cols-[_repeat(2,_minmax(80px,_1fr))] sm:grid-cols-[_repeat(3,_minmax(80px,_1fr))] md:grid-cols-[_repeat(3,_minmax(120px,_1fr))] lg:grid-cols-[_repeat(4,_minmax(120px,_1fr))] gap-3 sm:gap-5 auto-rows-[_1fr]">
                        {apps.length > 0 ? (
                            apps.map(
                                (app) =>
                                    app.category.name == "Android" && (
                                        <Link
                                            href={`/apps/${app.slug}`}
                                            key={app.id}
                                        >
                                            <div
                                                className={
                                                    "flex bg-white flex-col rounded-xl hover:bg-[#E2EEFF] hover:scale-105 duration-200 border border-slate-200 cursor-pointer relative"
                                                }
                                            >
                                                <div className="bg-white w-full rounded-t-xl aspect-square mb-[1rem]">
                                                    <img
                                                        className="object-contain w-full h-full rounded-t-lg"
                                                        layout="fill"
                                                        src={app.icon}
                                                    />
                                                </div>

                                                <div
                                                    className={
                                                        `love w-fit rounded-full p-[.25rem] lg:p-[.35rem] hover:bg-red-500 duration-200 absolute top-0 right-0 mt-[.35rem] mr-[.35rem] lg:mt-[.5rem] lg:mr-[.5rem] ` +
                                                        (app.is_workspace ==
                                                        true
                                                            ? "bg-red-500"
                                                            : "bg-white")
                                                    }
                                                >
                                                    {app.is_workspace == 1 && (
                                                        <HeartIcon className="w-3 lg:w-4 text-white" />
                                                    )}
                                                    {app.is_workspace == 0 && (
                                                        <HeartIconOutline className="love__icon w-3 lg:w-4 text-slate-400 duration-200" />
                                                    )}
                                                </div>

                                                <div className="px-4 mb-[1rem] flex flex-col gap-1">
                                                    <p className="text-[.7rem] lg:text-[.8rem] font-bold truncate">
                                                        {app.name}
                                                    </p>
                                                    <div className="flex flex-row gap-1 text-[.6rem] sm:text-[.75rem] items-center text-[#818598]">
                                                        {app.category.name ==
                                                            "Web Apps" && (
                                                            <DesktopComputerIcon className="w-[.65rem] sm:w-[.75rem] lg:w-[.85rem]" />
                                                        )}

                                                        {app.category.name !=
                                                            "Web Apps" && (
                                                            <DeviceMobileIcon className="w-[.65rem] sm:w-[.75rem] lg:w-[.85rem]" />
                                                        )}

                                                        <span>
                                                            {app.category.name}
                                                        </span>
                                                    </div>
                                                    <div className="flex flex-row gap-3 justify-between">
                                                        <div className="flex flex-row">
                                                            {(function () {
                                                                const view = [];
                                                                for (
                                                                    let i = 1;
                                                                    i <= 5;
                                                                    i++
                                                                ) {
                                                                    if (
                                                                        i <=
                                                                        app.rate
                                                                    )
                                                                        view.push(
                                                                            <StarIcon
                                                                                key={
                                                                                    i
                                                                                }
                                                                                className="w-[.7rem] sm:w-[.8rem] lg:w-[1rem] text-[#FAA705]"
                                                                            />
                                                                        );
                                                                    else
                                                                        view.push(
                                                                            <StarIconOutline
                                                                                key={
                                                                                    i
                                                                                }
                                                                                className="w-[.6rem] sm:w-[.7rem] lg:w-[.9rem] text-[#818598]"
                                                                            />
                                                                        );
                                                                }
                                                                return view;
                                                            })()}
                                                        </div>

                                                        <div className="flex flex-row text-[#818598] items-center">
                                                            <span className="text-[.6rem] sm:text-[.7rem] lg:text-[.75rem] pr-1">
                                                                {app.review}
                                                            </span>
                                                            <UserIcon className="w-[.65rem] sm:w-[.75rem] lg:w-[.85rem]" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </Link>
                                    )
                            )
                        ) : (
                            <span className="text-slate-400">No Data</span>
                        )}
                    </div>
                </Tab.Panel>
            </Tab.Panels>
        </Tab.Group>
    );
}
