import { Tab, Dialog, Transition } from "@headlessui/react";
import {
    StarIcon,
    ChatIcon,
    FilterIcon,
    DesktopComputerIcon,
    ChevronRightIcon,
    UserIcon,
    HeartIcon,
    XIcon,
    InformationCircleIcon,
} from "@heroicons/react/solid";
import { StarIcon as StarIconOutline } from "@heroicons/react/outline";

import { Mousewheel } from "swiper";
import { Swiper, SwiperSlide } from "swiper/react";

import { useState, Fragment } from "react";
import "swiper/css";
import Rating from "./Rating";
import Image from "next/image";
import axios from "axios";
import request from "../../utils/request";
import { parseCookies } from "nookies";
import { toast } from "react-toastify";
import { useRouter } from "next/router";

function classNames(...classes) {
    return classes.filter(Boolean).join(" ");
}

export default function AppsDetailComponent({ data }) {
    const router = useRouter();
    let [isOpen, setIsOpen] = useState(false);
    const [nama, setNama] = useState("");
    const [review, setReview] = useState("");
    const cookies = parseCookies();
    const jwt = { cookies };

    function closeModal() {
        setIsOpen(false);
    }

    function openModal() {
        setIsOpen(true);
    }

    let [categories] = useState({
        Preview: [
            {
                image: "app-detail-1.png",
            },
            {
                image: "app-detail-2.png",
            },
            {
                image: "app-detail-3.png",
            },
            {
                image: "app-detail-4.png",
            },
            {
                image: "app-detail-2.png",
            },
        ],
        Detail_Info: [
            {
                title: "Current Version",
                data: "5.5.15",
            },
            {
                title: "Last Accessed",
                data: "9 Juni 2022",
            },
            {
                title: "Accessed",
                data: "20.000 Times",
            },
        ],
        Ulasan: [
            {
                username: "Username One",
                photo: "username-one.png",
                date: "Kamis, 23 September 2021 pukul 11.32",
                comment:
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris odio cras tempus vulputate lacus, eget arcu. Nunc platea venenatis dui lectus tristique. Egestas turpis feugiat vitae quis. Integer sit tincidunt aenean convallis lectus ut sit.",
                rate: 5,
                position: "SVP Engineering",
            },
            {
                username: "Username Two",
                photo: "username-two.png",
                date: "Rabu, 22 September 2021 pukul 20.56",
                comment:
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris odio cras tempus vulputate lacus, eget arcu. Nunc platea venenatis dui lectus tristique. Egestas turpis feugiat vitae quis. Integer sit tincidunt aenean convallis lectus ut sit.",
                rate: 5,
                position: "SVP Engineering",
            },
            {
                username: "Username Three",
                photo: "username-three.png",
                date: "Rabu, 22 September 2021 pukul 08.30",
                comment:
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris odio cras tempus vulputate lacus, eget arcu. Nunc platea venenatis dui lectus tristique. Egestas turpis feugiat vitae quis. Integer sit tincidunt aenean convallis lectus ut sit.",
                rate: 4,
                position: "Senior Product Design",
            },
        ],
    });

    const [ratingValue, setRatingValue] = useState(undefined);

    const handleRatingAction = (value) => {
        setRatingValue(value);
    };

    const handlePostReview = async (id) => {
        const payload = {
            review: review,
            rate: ratingValue,
        };
        if(ratingValue > 0){
            await axios
                .post(request.postReview + id, payload, {
                    headers: {
                        Authorization: `Bearer ${jwt.cookies.token}`,
                    },
                })
                .then((res) => console.log("RES", res.data.data))
                .catch((err) =>
                    toast.error(err.response.data.message, {
                        pauseOnHover: false,
                        position: "bottom-right",
                        autoClose: 3000,
                        theme: "colored",
                        options: "5000 slide 1 0",
                    })
                )
                .finally(() => {
                    setIsOpen(false)
                    setNama('')
                    setReview('')
                    setRatingValue(0)
                });
        } else {
            toast.warning('Rating tidak boleh kosong')
        }
    };

    const handleWorkspace = async (id) => {
        await axios({
            method: "POST",
            url: request.postWorkspace+id,
            headers: {
                Authorization: `Bearer ${jwt.cookies.token}`,
            },
        })
        router.replace(router.asPath)
    }

    return (
        <>
            <div className="flex flex-col rounded-xl shadow-sm mb-4 border bg-white">
                <div
                    className={`relative p-5 rounded-t-xl h-36 lg:h-48 bg-cover flex flex-col sm:flex-row items-end justify-between`}
                >
                    <Image
                        src={data.app_background}
                        layout="fill"
                        objectFit="cover"
                        priority
                    />
                    <div className="bg-slate-50 w-14 lg:w-20 aspect-square rounded-lg lg:rounded-xl p-0.5 lg:p-1 absolute border-2 border-white outlined">
                        <img
                            src={data.app_icon}
                            className="object-cover sm:w-14 lg:w-20 aspect-square"
                        />
                    </div>
                    <button onClick={() => handleWorkspace(data.id)} className="absolute right-4 bg-white text-red-500 text-[.72rem] lg:text-xs font-bold hover:bg-red-50 duration-200 rounded-lg lg:rounded-xl px-4 flex flex-row items-center gap-x-2 py-2 lg:py-3">
                        <HeartIcon className="w-[.9rem] lg:w-[1rem]" />
                        {data.is_workspace ? 'Added to my Workspace' : 'Add to my Workspace'}
                        
                    </button>
                </div>

                <div className="flex flex-col p-5 gap-5">
                    <div className="flex flex-col gap-4 sm:gap-2 sm:flex-row justify-between items-start">
                        <div className="flex flex-col gap-2 lg:gap-3">
                            <div className="flex flex-col sm:flex-row gap-1 md:gap-2 lg:gap-3 md:items-center">
                                <h3 className="text-xs lg:text-sm font-medium">
                                    {data.name}
                                </h3>
                                <div className="flex flex-row gap-1 text-[.65rem] lg:text-[.75rem] items-center text-[#818598]">
                                    <DesktopComputerIcon className="w-[.75rem] lg:w-[.85rem]" />
                                    <span>{data.category.name}</span>
                                </div>
                            </div>
                            <div className="flex flex-row gap-3">
                                <div className="flex flex-row">
                                    {Array.from({ length: 5 }).map((el, idx) => (
                                        idx < data.rate ?  <StarIcon key={idx} className="w-[.8rem] lg:w-[1rem] text-[#FAA705]"/> :
                                        <StarIconOutline key={idx} className="w-[.7rem] lg:w-[.9rem] text-[#818598]"/>
                                    ))}
                                    {/* {(function () {
                                        const view = [];
                                        for (let i = 1; i <= 5; i++) {
                                            if (i <= data.rate)
                                                view.push(
                                                    <StarIcon
                                                        key={i}
                                                        className="w-[.8rem] lg:w-[1rem] text-[#FAA705]"
                                                    />
                                                );
                                            else
                                                view.push(
                                                    <StarIconOutline
                                                        key={i}
                                                        className="w-[.7rem] lg:w-[.9rem] text-[#818598]"
                                                    />
                                                );
                                        }
                                        return view;
                                    })()} */}
                                </div>

                                <div className="flex flex-row text-[#818598] items-center">
                                    <span className="text-[.65rem] lg:text-[.75rem] pr-1">
                                        {data.rate}
                                    </span>
                                    <UserIcon className="w-[.75rem] lg:w-[.85rem]" />
                                </div>
                            </div>
                        </div>

                        <div className="w-full sm:max-w-max">
                            <button
                                onClick={() => router.replace(data.app_url)}
                                className="bg-[#2E55A3] text-white text-[.72rem] lg:text-xs font-bold hover:bg-[#3280BB] duration-200 rounded-lg lg:rounded-xl px-4 flex flex-row items-center gap-x-2 py-2 lg:py-3 w-full justify-center"
                            >
                                Buka Aplikasi
                                <ChevronRightIcon className="w-[.9rem] lg:w-[1rem]" />
                            </button>
                        </div>
                    </div>

                    <div>
                        <p className="text-[#818598] text-[.72rem] leading-3 md:leading-4 lg:text-xs">
                            {data.description}
                        </p>
                    </div>
                </div>
            </div>

            <div as={`div`} className="min-h-[90vh]">
                <div className="flex gap-3 items-center justify-between border border-[#E9E9ED] shadow-md rounded-xl px-5 bg-white">
                    <div className="text-sm text-slate-400 py-4 flex gap-3 overflow-x-auto sm:overflow-x-hidden">
                        <div className="flex gap-2 items-center">
                            <a href="#preview" className="cursor-pointer">
                                Preview
                            </a>
                        </div>
                        <div className="flex gap-2 items-center">
                            <a href="#detail" className="cursor-pointer">
                                Detail Info
                            </a>
                        </div>
                        <div className="flex gap-2 items-center">
                            <a href="#ulasan" className="cursor-pointer">
                                Ulasan
                            </a>
                            <a
                                href="#ulasan"
                                className="cursor-pointer text-[.5rem] lg:text-[.6rem] font-medium text-white bg-[#FAA705] rounded-full leading-3 px-1 py-0 md:py-0.5"
                            >
                                {data.reviews.total}
                            </a>
                        </div>
                    </div>
                </div>
                <div className="mt-6">
                    <div className={classNames("focus:outline-none")}>
                        <div>
                            <div className="grid grid-cols-1">
                                <h3 id="preview" className="font-bold text-[#ABAEBA] text-[1rem] lg:text-lg mb-3">
                                    Preview
                                </h3>
                                <Swiper
                                    grabCursor={true}
                                    slidesPerView={4}
                                    spaceBetween={15}
                                    mousewheel={true}
                                    direction={"horizontal"}
                                    modules={[Mousewheel]}
                                    breakpoints={{
                                        "@0.00": {
                                            slidesPerView: 2,
                                            spaceBetween: 10,
                                        },
                                        "@0.75": {
                                            slidesPerView: 3,
                                            spaceBetween: 15,
                                        },
                                        "@1.00": {
                                            slidesPerView: 4,
                                        },
                                    }}
                                >
                                    {data.previews.preview.map((img, idx) => (
                                        <SwiperSlide className="mt-1" key={idx}>
                                            <img
                                                src={img.image}
                                                className="object-cover rounded-3xl cursor-pointer border duration-200 hover:-translate-y-1 hover:border-[#3280BB]"
                                            />
                                        </SwiperSlide>
                                    ))}
                                </Swiper>
                            </div>

                            <div id="detail">
                                <h3 className="font-bold text-[#ABAEBA] text-[1rem] lg:text-lg my-4">
                                    Detail Info
                                </h3>
                                <div className="grid grid-cols-2 sm:grid-cols-3 gap-4 lg:gap-6">
                                    <div className="border rounded-xl px-4 py-5 bg-white shadow-md hover:bg-slate-50 hover:shadow-sm duration-200">
                                        <h4 className="text-[#393C46] font-medium text-[.7rem] lg:text-[.8rem] mb-1">
                                            Current Version
                                        </h4>
                                        <h2 className="font-bold text-sm lg:text-lg">
                                            {data.detail_info.current_version}
                                        </h2>
                                    </div>
                                    <div className="border rounded-xl px-4 py-5 bg-white shadow-md hover:bg-slate-50 hover:shadow-sm duration-200">
                                        <h4 className="text-[#393C46] font-medium text-[.7rem] lg:text-[.8rem] mb-1">
                                            Last Accessed
                                        </h4>
                                        <h2 className="font-bold text-sm lg:text-lg">
                                            {data.detail_info.last_accessed}
                                        </h2>
                                    </div>
                                    <div className="border rounded-xl px-4 py-5 bg-white shadow-md hover:bg-slate-50 hover:shadow-sm duration-200">
                                        <h4 className="text-[#393C46] font-medium text-[.7rem] lg:text-[.8rem] mb-1">
                                            Accessed
                                        </h4>
                                        <h2 className="font-bold text-sm lg:text-lg">
                                            {data.detail_info.accessed}
                                        </h2>
                                    </div>
                                </div>
                            </div>

                            <div>
                                <h3
                                    id="ulasan"
                                    className="font-bold text-[#ABAEBA] text-[1rem] lg:text-lg my-4"
                                >
                                    Ulasan
                                </h3>
                                <div className="flex gap-4 justify-between mb-4">
                                    <button
                                        type="button"
                                        onClick={openModal}
                                        className="bg-[#2E55A3] text-white text-[.7rem] lg:text-sm font-bold hover:bg-[#3280BB] duration-200 rounded-lg lg:rounded-xl px-4 flex flex-row items-center gap-x-2 py-2 lg:py-3"
                                    >
                                        <ChatIcon className="w-[.9rem] lg:w-[1rem]" />
                                        Beri Ulasan
                                    </button>

                                    <button className="bg-[#E2EEFF] text-[#2E55A3] text-[.7rem] lg:text-sm font-bold hover:text-white hover:bg-[#3280BB] duration-200 rounded-lg lg:rounded-xl px-4 flex flex-row items-center gap-x-2 py-2 lg:py-3">
                                        <FilterIcon className="w-[.9rem] lg:w-[1rem]" />
                                        Sort By
                                    </button>
                                </div>

                                <div className="flex flex-col gap-4">
                                    {data.reviews.latest.map((review, idx) => (
                                        <div
                                            key={idx}
                                            className="rounded-xl hover:bg-[#E2EEFF] py-5 px-4 duration-200 border border-slate-200 shadow-sm bg-white"
                                        >
                                            <div className="grid sm:flex sm:flex-row gap-4 md:gap-6 sm:auto-rows-[_1fr] items-start cursor-pointer">
                                                <div className="flex flex-row sm:basis-[25%] md:basis-[35%] gap-4 items-center">
                                                    <img
                                                        className="dropdown__menu-profile-img w-[32px] h-[32px] lg:w-[40px] lg:h-[40px]"
                                                        layout="fill"
                                                        src={
                                                            review.user
                                                                .photo_profile
                                                        }
                                                    />

                                                    <div className="flex flex-col">
                                                        <span className="text-[.72rem] lg:text-[.82rem] font-bold">
                                                            {review.user.name}
                                                        </span>
                                                        <span className="text-[.6rem] lg:text-[.65rem] font-normal text-slate-500">
                                                            {
                                                                review.user
                                                                    .position
                                                            }
                                                        </span>
                                                    </div>
                                                </div>

                                                <div className="sm:basis-[75%] md:basis-[65%]">
                                                    <div className="flex flex-row justify-between items-start sm:items-center gap-3">
                                                        <div className="flex flex-row">
                                                        {Array.from({ length: 5 }).map((el, idx) => (
                                                            idx < data.rate ?  <StarIcon key={idx} className="w-[.8rem] lg:w-[1rem] text-[#FAA705]"/> :
                                                            <StarIconOutline key={idx} className="w-[.7rem] lg:w-[.9rem] text-[#818598]"/>
                                                        ))}
                                                        </div>
                                                        <span className="text-[#818598] text-[.6rem] lg:text-xs text-end">
                                                            {review.created_at}
                                                        </span>
                                                    </div>

                                                    <p className="text-[.65rem] text-xs">
                                                        {review.review}
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    ))}
                                </div>

                                <Transition appear show={isOpen} as={Fragment}>
                                    <Dialog
                                        as="div"
                                        className="relative z-10"
                                        onClose={closeModal}
                                    >
                                        <Transition.Child
                                            as={Fragment}
                                            enter="ease-out duration-300"
                                            enterFrom="opacity-0"
                                            enterTo="opacity-100"
                                            leave="ease-in duration-200"
                                            leaveFrom="opacity-100"
                                            leaveTo="opacity-0"
                                        >
                                            <div className="fixed inset-0 bg-black bg-opacity-40" />
                                        </Transition.Child>

                                        <div className="modal__add-review fixed inset-0 overflow-y-auto">
                                            <div className="flex min-h-full items-center justify-center p-4 text-center">
                                                <Transition.Child
                                                    as={Fragment}
                                                    enter="ease-out duration-300"
                                                    enterFrom="opacity-0 scale-95"
                                                    enterTo="opacity-100 scale-100"
                                                    leave="ease-in duration-200"
                                                    leaveFrom="opacity-100 scale-100"
                                                    leaveTo="opacity-0 scale-95"
                                                >
                                                    <Dialog.Panel className="modal__add-review lg:w-full max-w-md transform overflow-hidden rounded-2xl bg-white p-5 text-left align-middle shadow-xl transition-all">
                                                        <Dialog.Title
                                                            as="div"
                                                            className="flex flex-row justify-between text-[#818598] items-center"
                                                        >
                                                            <h3 className="text-[.72rem] lg:text-sm font-medium">
                                                                BERIKAN ULASAN
                                                            </h3>
                                                            <button
                                                                onClick={
                                                                    closeModal
                                                                }
                                                                type="button"
                                                            >
                                                                <XIcon className="w-3 lg:w-4 hover:text-slate-700 duration-150"></XIcon>
                                                            </button>
                                                        </Dialog.Title>
                                                        <div className="mt-5 flex flex-col gap-5">
                                                            <div className="bg-[#E2EEFF] text-[#2E55A3] text-[.72rem] lg:text-sm rounded-xl px-3 lg:px-4 flex flex-row items-center gap-x-2 py-3 lg:py-4 justify-center">
                                                                <InformationCircleIcon className="w-3 lg:w-4" />
                                                                <h3>
                                                                    Ulasan Anda
                                                                    akan dilihat
                                                                    oleh seluruh
                                                                    pengguna
                                                                    lain.
                                                                </h3>
                                                            </div>
                                                            <div className="font-inter">
                                                                <form action="">
                                                                    <div className="input__flex mb-[1rem]">
                                                                        <label
                                                                            className="input__groups-label"
                                                                        >
                                                                            Nama
                                                                        </label>
                                                                        <div className="input__groups">
                                                                            <input
                                                                                type="text"
                                                                                id="username"
                                                                                className="input__group px-3"
                                                                                placeholder="Masukkan nama Anda"
                                                                                value={
                                                                                    nama
                                                                                }
                                                                                onChange={(
                                                                                    e
                                                                                ) =>
                                                                                    setNama(
                                                                                        e
                                                                                            .target
                                                                                            .value
                                                                                    )
                                                                                }
                                                                            />
                                                                        </div>
                                                                    </div>

                                                                    <div className="input__flex mb-[1rem]">
                                                                        <label
                                                                            className="input__groups-label"
                                                                        >
                                                                            Review
                                                                        </label>
                                                                        <div className="input__groups">
                                                                            <textarea
                                                                                rows={5}
                                                                                type="text"
                                                                                id="username"
                                                                                className="input__group px-3 resize-none"
                                                                                placeholder="Bagikan pendapat Anda tentang aplikasi ini"
                                                                                value={review}
                                                                                onChange={(e) =>setReview(e.target.value)}
                                                                                required
                                                                            />
                                                                        </div>
                                                                    </div>

                                                                    <div className="input__flex mb-[1rem]">
                                                                        <label
                                                                            className="input__groups-label"
                                                                        >
                                                                            Beri Rating
                                                                        </label>
                                                                        {/* <div className="flex flex-row">
                                                                                    <StarIcon className="w-[1.5rem] lg:w-[2rem] text-[#FAA705]" />
                                                                                    <StarIcon className="w-[1.5rem] lg:w-[2rem] text-[#FAA705]" />
                                                                                    <StarIcon className="w-[1.5rem] lg:w-[2rem] text-[#FAA705]" />
                                                                                    <StarIconOutline className="w-[1.2rem] lg:w-[1.8rem] text-[#818598]" />
                                                                                    <StarIconOutline className="w-[1.2rem] lg:w-[1.8rem] text-[#818598]" />
                                                                                </div> */}
                                                                        <div className="flex items-center gap-x-2">
                                                                            <Rating
                                                                                iconSize="xl"
                                                                                showOutOf={true}
                                                                                enableUserInteraction={true}
                                                                                onClick={handleRatingAction}
                                                                            />
                                                                            {/* <label className="text-lg text-slate-500">{ratingValue}.0</label> */}
                                                                        </div>
                                                                    </div>

                                                                    <div className="flex flex-row justify-end items-center gap-2">
                                                                        <button
                                                                            type="button"
                                                                            onClick={
                                                                                closeModal
                                                                            }
                                                                            className="bg-[#E2EEFF] text-[#2E55A3] text-[.65rem] lg:text-[.75rem] font-bold hover:text-white hover:bg-[#3280BB] duration-200 rounded-xl px-4 py-2"
                                                                        >
                                                                            Cancel
                                                                        </button>
                                                                        <button
                                                                            type="button"
                                                                            onClick={() => handlePostReview(data.id)}
                                                                            className="bg-[#2E55A3] text-white text-[.65rem] lg:text-[.75rem] font-bold hover:bg-[#3280BB] duration-200 rounded-xl px-4 py-2"
                                                                        >
                                                                            Submit
                                                                        </button>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </Dialog.Panel>
                                                </Transition.Child>
                                            </div>
                                        </div>
                                    </Dialog>
                                </Transition>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
}
