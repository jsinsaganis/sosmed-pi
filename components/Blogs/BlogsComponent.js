import axios from "axios";
import Link from "next/link";
import React from "react";

function BlogsComponent({ blogs }) {

    return (
        <>
            <div className="min-h-[90vh]">
                <div className="grid grid-cols-[_repeat(2,_minmax(120px,_1fr))] sm:grid-cols-[_repeat(3,_minmax(120px,_1fr))] md:grid-cols-[_repeat(2,_minmax(120px,_1fr))] lg:grid-cols-[_repeat(3,_minmax(120px,_1fr))] gap-3 md:gap-5 auto-rows-[_1fr]">
                    {blogs.map((blog) => (
                        <Link href={`blogs/${blog.slug}`} key={blog.id}>
                            <div
                                className={
                                    "flex flex-col rounded-xl hover:bg-[#E2EEFF] hover:scale-105 duration-200 border border-slate-200 cursor-pointer"
                                }
                            >
                                <div className="bg-slate-50 w-full rounded-t-xl h-32 sm:h-28 md:h-32 lg:h-36 mb-[1rem]">
                                    <img
                                        className="object-cover w-full h-full rounded-t-lg"
                                        layout="fill"
                                        src={blog.cover.cover_url}
                                    />
                                </div>

                                <div className="px-4 mb-[1rem] flex flex-col gap-1">
                                    <p className="text-[.6rem] leading-3 lg:text-[.7rem] line-clamp-3 text-[#393C46]">
                                        {blog.created_at}
                                    </p>
                                    <h3 className="text-[.7rem] leading-4 lg:leading-5 lg:text-[.8rem] font-bold line-clamp-2">
                                        {blog.title}
                                    </h3>
                                    <p className="text-[.6rem] leading-3 lg:leading-4 lg:text-[.7rem] line-clamp-3 text-[#818598]">
                                        {blog.body}
                                    </p>
                                </div>
                            </div>
                        </Link>
                    ))}
                </div>
            </div>
        </>
    );
}

export default BlogsComponent;
