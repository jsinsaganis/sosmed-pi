import Link from "next/link";
import LatestBlog from "./LatestBlog"

export default function BlogsDetailComponent({ blog, trending_blogs }) {
    return (
        <div className="flex flex-col sm:flex-row gap-4 md:gap-7">
            <div className="basis-2/3">
                <div className="border border-slate-200 rounded-xl p-5 bg-white shadow-sm">
                    <img
                        src={blog.cover.cover_url}
                        className="w-full h-36 sm:h-48 lg:h-64 object-cover rounded-xl"
                    />
                    <div className="flex flex-col mt-6">
                        <p className="text-[#393C46] text-[.65rem] lg:text-xs mb-1">
                            {blog.created_at}
                        </p>
                        <h3 className="text-sm lg:text-lg font-bold">
                            {blog.title}
                        </h3>
                        <p className="text-[#393C46] text-[.65rem] lg:text-xs mt-4">
                            {blog.body}
                        </p>
                    </div>
                </div>
            </div>


            <div className="basis-1/3">
                <div className="w-full md:top-[6rem] lg:top-[7.2rem] flex flex-col gap-3 md:gap-5">
                    <LatestBlog/>
                    {/* <div className="border border-slate-200 rounded-xl bg-white">
                        <h3 className="text-xs font-medium text-[#818598] p-3 lg:p-4">
                            LATEST BLOG
                        </h3>
                        <div className="flex flex-col pb-2">
                            {latest_blogs.slice(0, 3).map((blog) => {
                                return (
                                    <Link href={`/blogs/${blog.slug}`} key={blog.id}>
                                        <div className="flex flex-row items-center gap-2 cursor-pointer hover:bg-[#E2EEFF] px-3 lg:px-4 py-1 lg:py-2 duration-200">
                                            <img
                                                src={blog.cover.cover_url}
                                                className="w-8 h-8 lg:w-12 lg:h-12 rounded-md object-cover"
                                            />
                                            <div className="flex flex-col gap-0">
                                                <p className="text-[.55rem] lg:text-[.65rem] text-[#393C46]">
                                                    {blog.created_at}
                                                </p>
                                                <p className="text-[.62rem] leading-3 lg:leading-4 lg:text-[.75rem] font-bold line-clamp-2">
                                                    {blog.title}
                                                </p>
                                            </div>
                                        </div>
                                    </Link>
                                );
                            })}
                        </div>
                    </div> */}
                    <div className="border border-slate-200 rounded-xl bg-white">
                        <h3 className="text-xs font-medium text-[#818598] p-3 lg:p-4">
                            TRENDING
                        </h3>
                        <div className="flex flex-col pb-2">
                            {trending_blogs.slice(0, 3).map((blog) => {
                                return (
                                    <Link href={`/blogs/${blog.slug}`} key={blog.id}>
                                        <div className="flex flex-row items-center gap-2 cursor-pointer hover:bg-[#E2EEFF] px-3 lg:px-4 py-1 lg:py-2 duration-200">
                                            <img
                                                src={blog.cover.cover_url}
                                                className="w-8 h-8 lg:w-12 lg:h-12 rounded-md object-cover"
                                            />
                                            <div className="flex flex-col gap-0">
                                                <p className="text-[.55rem] lg:text-[.65rem] text-[#393C46]">
                                                    {blog.created_at}
                                                </p>
                                                <p className="text-[.62rem] leading-3 lg:leading-4 lg:text-[.75rem] font-bold line-clamp-2">
                                                    {blog.title}
                                                </p>
                                            </div>
                                        </div>
                                    </Link>
                                );
                            })}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}
