import axios from "axios";
import Link from "next/link";
import { parseCookies } from "nookies";
import React, { useEffect, useState } from "react";
import useSWR from "swr";
import request from "../../utils/request";

function LatestBlog() {
    const cookies = parseCookies();
    const jwt = { cookies };
    const [latestBlog, setLatestBlog] = useState([])

    const fetcher = (url) =>
        axios
            .get(url, {
                headers: {
                    Authorization: `Bearer ${jwt.cookies.token}`,
                },
            })
            .then((res) => res.data.data);

    const { data, isValidating } = useSWR(request.getAllBlog, fetcher)

    useEffect(() => {
        if(data){
            setLatestBlog(data.blogs)
        }
    },[data])

    return (
        <div className="border border-slate-200 rounded-xl bg-white">
            <h3 className="text-xs font-medium text-[#818598] p-3 lg:p-4">
                LATEST BLOG
            </h3>
            <div className="flex flex-col pb-2">
                {latestBlog && latestBlog.slice(0, 3).map((blog) => (
                        <Link href={`/blogs/${blog.slug}`} key={blog.id}>
                            <div className="flex flex-row items-center gap-2 cursor-pointer hover:bg-[#E2EEFF] px-3 lg:px-4 py-1 lg:py-2 duration-200">
                                <img
                                    src={blog.cover.cover_url}
                                    className="w-8 h-8 lg:w-12 lg:h-12 rounded-md object-cover"
                                />
                                <div className="flex flex-col gap-0">
                                    <p className="text-[.55rem] lg:text-[.65rem] text-[#393C46]">
                                        {blog.created_at}
                                    </p>
                                    <p className="text-[.62rem] leading-3 lg:leading-4 lg:text-[.75rem] font-bold line-clamp-2">
                                        {blog.title}
                                    </p>
                                </div>
                            </div>
                        </Link>
                    ))}
            </div>
        </div>
    );
}

export default LatestBlog;
