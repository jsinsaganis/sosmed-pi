import {
    DotsHorizontalIcon,
    HeartIcon,
    ThumbUpIcon,
} from "@heroicons/react/solid";
import { FaRegLaughSquint } from "react-icons/fa";
import Image from "next/image";
import { useState } from "react";
import Dropdown from "./Dropdown";
import { BsFillPinAngleFill } from "react-icons/bs";
import { BellIcon } from "@heroicons/react/solid";
import { TrashIcon } from "@heroicons/react/solid";

function Comments({ comments }) {
    const [replyInputFlag, setReplyInputFlag] = useState(false);
    return (
        <section className="space-y-2.5 w-full">
            {comments.map((comment) => (
                <div
                    key={comment.id}
                    className="flex items-start space-x-1.5"
                >
                    <div className="relative flex space-x-3 w-10 h-10 rounded-xl overflow-hidden">
                        <Image src={comment.user.photo_profile} width={40} height={40} objectFit="cover" />
                    </div>
                    <div className="space-y-2.5 w-full">
                        <div className="relative bg-slate-100 py-2 px-3 rounded-lg flex-auto space-y-2">
                            <div className="flex justify-between h-fit">
                                <p className={`text-slate-500 flex items-center space-x-2 ${comment.user.name.length+comment.user.position.length > 40 && '!flex-col !space-x-0 !items-start'}`}>
                                    <span className="">{comment.user.name}</span>
                                    <span className={`inline-block w-[4px] h-[4px] rounded-full bg-slate-400 ${comment.user.name.length+comment.user.position.length > 40 && 'hidden'}`}></span>
                                    <span className="">{comment.user.position}</span>
                                    
                                </p>
                                <DotsHorizontalIcon className="w-6 h-6 text-slate-400 cursor-pointer" />
                            </div>
                            <div>
                                <p className="inline-block max-w-md text-slate-700">
                                    {comment.comment}
                                </p>
                            </div>
                            {/* <div className="flex justify-center items-center">
                                <ThumbUpIcon className="absolute right-16 text-white w-6 h-6 bg-blue-700 rounded-full px-1" />
                                <HeartIcon className="absolute right-11 text-white w-6 h-6 bg-pink-500 rounded-full px-1" />
                                <FaRegLaughSquint className="absolute right-6 text-black w-6 h-6 bg-gradient-to-b from-[#FCCA69] to-[#FAA705] rounded-full" />
                                <span className="absolute right-0 rounded-full">
                                    belum ada love
                                </span>
                            </div> */}
                        </div>
                        {/* <div className="flex text-sm space-x-2.5 px-3">
                            <span className="text-[#F53D49]">Love</span>
                            <span
                                onClick={() =>
                                    setReplyInputFlag(!replyInputFlag)
                                }
                                className="text-slate-600 cursor-pointer"
                            >
                                Reply
                            </span>
                            <span className="text-slate-600">Share</span>
                        </div> */}
                        {/* {comment.reply.length > 0 ? (
                            comment.reply.map((reply) => (
                                <div key={reply.username} className="flex items-start space-x-1.5">
                                  <div className="relative flex space-x-3 w-10 h-10 rounded-xl overflow-hidden">
                                      <Image
                                          src={reply.avatar}
                                          width={40}
                                          height={40}
                                          layout="fixed"
                                          objectFit="cover"
                                      />
                                  </div>
                                    <div className="space-y-2.5 w-full">
                                        <div className="relative bg-slate-100 py-2 px-3 rounded-lg flex-auto space-y-2">
                                            <div className="flex justify-between h-fit">
                                                <p className="text-slate-500 flex items-center gap-x-2">
                                                    {reply.username}{" "}
                                                    <span className="inline-block w-1.5 h-1.5 rounded-full bg-slate-400"></span>{" "}
                                                    {reply.jabatan}
                                                </p>
                                                <DotsHorizontalIcon className="w-6 h-6 text-slate-400 cursor-pointer" />
                                            </div>
                                            <div>
                                                <p className="inline-block max-w-md text-slate-700">
                                                    {reply.quote}
                                                </p>
                                            </div>
                                        </div>
                                        <div className="flex text-sm space-x-2.5 px-3">
                                            <span className="text-slate-600">
                                                Like
                                            </span>
                                            <span
                                                onClick={() =>
                                                    setReplyInputFlag(
                                                        !replyInputFlag
                                                    )
                                                }
                                                className="text-slate-600 cursor-pointer"
                                            >
                                                Reply
                                            </span>
                                            <span className="text-slate-600">
                                                Share
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            ))
                        ) : (
                            <></>
                        )} */}
                    </div>
                </div>
            ))}
        </section>
    );
}

export default Comments;
