import { useEffect, useRef, useState } from "react";
import { CameraIcon, VideoCameraIcon, XIcon } from "@heroicons/react/solid";
import { useRecoilState, useSetRecoilState } from "recoil";
import request from "../../utils/request";
import { userData } from "../../atoms/atomState";
import axios from "axios";
import { parseCookies } from "nookies";
import { loading } from "../../atoms/atomState";
import { useRouter } from "next/router";
import { useSWRConfig, mutate } from "swr";

function CustomTextArea({ minRows, placeholder, bordered, bgColor }) {
    const router = useRouter()
    const [rows, setRows] = useState(minRows);
    const [value, setValue] = useState("");
    const filePickerRef = useRef(null);
    const [selectedFile, setSelectedFile] = useState([]);
    const [userProfile] = useRecoilState(userData);
    const [isLoading, setIsLoading] = useRecoilState(loading);
    const [fileUpload, setFileUpload] = useState([]);

    const removeImage = (file) => {
        const filteredFile = selectedFile.filter((item) => item !== file);

        const idx = selectedFile.indexOf(file);
        const filteredImg = [...fileUpload]
        if (idx > -1) {
            filteredImg.splice(idx, 1);
        }

        setSelectedFile(filteredFile);
        setFileUpload(filteredImg);
    };

    const addImageToInput = (e) => {
        for (const file of e.target.files) {
            const reader = new FileReader();
            if (file) {
                reader.readAsDataURL(file);
            }
            reader.onload = () => {
                setSelectedFile((imgs) => [...imgs, reader.result]);
            };
            reader.onerror = () => {
                console.log(reader.error);
            };
        }

        const images = [...fileUpload];
        images.push(e.target.files[0]);

        setFileUpload(images);
    };

    useEffect(() => {
        const rowlen = value.split("\n");

        if (rowlen.length > minRows) {
            setRows(rowlen.length);
        }
    }, [value]);

    const handlePost = async () => {
        setIsLoading(true);
        const cookies = parseCookies();
        const jwt = { cookies };
        
        const formData = new FormData();
        formData.append("title", value);
        formData.append("desc", value);
        if (fileUpload) {
            for (let i = 0; i < fileUpload.length; i++) {
                formData.append(`image[${i}]`, fileUpload[i]);
            }
        }
        formData.append("url", "www.google.com");
        formData.append("hastag", "#Whelp");
        formData.append("nik", jwt.cookies.nik);

        await axios({
            method: "POST",
            url: request.postFeed,
            data: formData,
            headers: {
                "Content-Type": `multipart/form-data; boundary=${formData._boundary}`,
                Authorization: `Bearer ${jwt.cookies.token}`,
            },
        })
            .then((res) => console.log("res",res))
            .catch((err) => console.log("Err", err));

        setValue("");
        mutate(request.getFeedLocal+1);
        setIsLoading(false);
        setSelectedFile([]);
        setFileUpload([]);
    };

    return (
        <>
            <div className="bg-slate-400"></div>
            <textarea
                placeholder={placeholder}
                className={`w-full resize-none ${
                    bordered ? "border-[#E9E9ED] border-b-2" : ""
                }  focus:outline-none overflow-hidden bg-${bgColor}-100 p-2 text-slate-600 text-md`}
                rows={rows}
                value={value}
                onChange={(text) => setValue(text.target.value)}
            />
            {selectedFile && !isLoading && (
                <div className="flex flex-wrap gap-2">
                    {selectedFile.map((file, idx) => (
                        <div className="relative" key={idx}>
                            <div
                                className="absolute w-8 h-8 bg-white hover:bg-slate-400 bg-opacity-75 rounded-full flex items-center justify-center top-1 left-1 cursor-pointer"
                                onClick={() => removeImage(file)}
                            >
                                <XIcon className="text-red-700 h-5" />
                            </div>
                            <img
                                src={file}
                                alt=""
                                className="rounded-2xl max-h-80 object-contain"
                            />
                        </div>
                    ))}
                </div>
            )}

            <div className="flex items-center justify-between mt-2 space-x-2">
                <div className="flex items-center space-x-2">
                    <button
                        onClick={() => filePickerRef.current.click()}
                        className="flex bg-slate-100 px-2 py-3 rounded-xl w-[80px] md:w-[100px] text-xs md:text-sm"
                    >
                        <CameraIcon className="md:w-6 md:h-6 w-4 h-4 mr-2" />
                        Photo
                    </button>
                    <input
                        type="file"
                        ref={filePickerRef}
                        hidden
                        onChange={addImageToInput}
                    />
                    <button className="flex bg-slate-100 px-2 py-3 rounded-xl w-[80px] md:w-[100px] text-xs md:text-sm">
                        <VideoCameraIcon className="md:w-6 md:h-6 w-4 h-4 mr-2" />
                        Video
                    </button>
                </div>

                <button
                    onClick={handlePost}
                    className="bg-[#2E55A3] text-white font-semibold px-4 py-3 rounded-xl w-[100px] text-xs md:text-sm"
                >
                    Post
                </button>
            </div>
        </>
    );
}

export default CustomTextArea;
