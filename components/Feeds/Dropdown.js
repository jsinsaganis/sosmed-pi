import { useEffect, useRef, useState } from "react";

function Dropdown({ list, Icon }) {
  const [opened, setOpened] = useState(false);
  const componentRef = useRef();

  useEffect(() => {
    document.addEventListener("click", handleClick);
    return () => document.removeEventListener("click", handleClick);
    function handleClick(e) {
      if (componentRef && componentRef.current) {
        const ref = componentRef.current;
        if (!ref.contains(e.target)) {
          setOpened(false);
        }
      }
    }
  }, [componentRef]);

  return (
    <div className="relative " ref={componentRef}>
      <Icon
        onClick={() => setOpened(!opened)}
        className="w-8 h-8 cursor-pointer rounded-full p-1 hover:bg-slate-200"
      />
      {opened && (
        <div className="absolute w-fit transform -translate-x-[80%] bg-white rounded-xl p-2 h-fit z-10 shadow-md ">
          {list[0].map(({ Icon, text }) => (
            <a
              href="#"
              key={text}
              className="flex items-center space-x-4 p-2 rounded-lg hover:bg-slate-200 hover:text-[#2E55A3] last:text-red-500 last:hover:text-red-500"
              onClick={() => setOpened(false)}
            >
              <Icon className="w-4 h-4" />
              <span>{text}</span>
            </a>
          ))}
        </div>
      )}
    </div>
  );
}

export default Dropdown;
