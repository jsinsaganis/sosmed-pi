import CustomTextArea from "./CustomTextArea";
import { PlusCircleIcon } from "@heroicons/react/solid";
import Reels from "../Feeds/Reels";
import Workspace from "../Workspace";
import Image from "next/image";
import HeadComponent from "../Layouts/HeadComponent";
import { useRecoilState } from "recoil";
import { userData } from "../../atoms/atomState";
import axios from "axios";
import { parseCookies } from "nookies";
import useSWRInfinite from "swr/infinite";
import PostCard from "./PostCard";
import request from "../../utils/request";
import useSWR from "swr";
import { useRef, useState } from "react";
import { Mousewheel, Scrollbar } from "swiper";
import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/css/scrollbar";
import "swiper/css";
import Loader from "../Loader";
import LatestBlog from "../Blogs/LatestBlog";

export default function Feed() {
    const [profile] = useRecoilState(userData);
    const filePicker = useRef(null);
    const [loading, setLoading] = useState(false);

    const cookies = parseCookies();
    const jwt = { cookies };

    {
        /* Using default SWR */
    }
    const fetcher = (url) =>
        axios
            .get(url, {
                headers: {
                    Authorization: `Bearer ${jwt.cookies.token}`,
                },
            })
            .then((res) => res.data.data);

    const {
        data: content,
        mutate: magic,
        isValidating: isLoading,
    } = useSWR(request.getFeedLocal + 1, fetcher, { revalidateOnFocus: false });

    {
        /* Using useSWRInfinite */
    }
    // const getKey = (pageIndex, previousPageData) => {
    //     const page = pageIndex + 1;
    //     if (previousPageData && !previousPageData.length) return null; // reached the end
    //     return request.getFeedLocal + page; // SWR key
    // };

    // const {
    //     data,
    //     mutate: magic,
    //     size,
    //     setSize,
    // } = useSWRInfinite(getKey, fetcher, {
    //     initialSize: 1,
    //     revalidateFirstPage: true,
    // });
    // const content = data ? [].concat(...data) : [];

    {
        /* fetching Reels */
    }
    const { data: reels, isValidating } = useSWR(request.getReels, fetcher, {
        revalidateOnFocus: false,
        revalidateIfStale: false,
    });

    const addReels = async (e) => {
        setLoading(true);
        const formData = new FormData();
        const file = e.target.files;
        if (file) {
            for (let i = 0; i < file.length; i++) {
                formData.append(`image[${i}]`, file[i]);
            }
        }
        await axios
            .post(request.storeReels, formData, {
                headers: {
                    Authorization: `Bearer ${jwt.cookies.token}`,
                },
            })
            .then((res) => console.log(res.data.data))
            .catch((err) => console.log(err))
            .finally(() => setLoading(false));
    };

    return (
        <>
            <HeadComponent title="Feed" />
            <div className="lg:flex gap-x-7 min-h-[90vh]">
                <div className="space-y-[24px] h-full rounded-xl overflow-hidden basis-2/3">
                    <div className="">
                        <div className="grid items-center md:space-x-2.5 rounded-xl flex-nowrap relative pb-2">
                            <Swiper
                                className={`reels_swiper ${
                                    isValidating && "hidden"
                                }`}
                                grabCursor={true}
                                slidesPerView={"auto"}
                                spaceBetween={10}
                                mousewheel={true}
                                direction={"horizontal"}
                                modules={[Mousewheel, Scrollbar]}
                                scrollbar={true}
                            >
                                <SwiperSlide className="mt-1 max-w-[100px]">
                                    <div
                                        className={`text-center md:bg-white rounded-xl relative h-28 min-w-[97px] max-w-[100px] cursor-pointer transition duration-200 ease-out md:h-36 md:min-w-[97px] group ${
                                            loading && "animate-pulse"
                                        }`}
                                    >
                                        <div className="aspect-square relative rounded-full flex items-center justify-center md:h-[40px] md:w-[40px] md:absolute top-1 left-1 border-2 border-white md:rounded-xl outlined h-[85px] w-[85px] overflow-hidden">
                                            {profile && (
                                                <Image
                                                    layout="fill"
                                                    src={profile.photo_profile}
                                                    objectFit="cover"
                                                />
                                            )}
                                        </div>
                                        <div className="bg-white rounded-full absolute bottom-5 right-3 md:top-14 md:right-8">
                                            <PlusCircleIcon
                                                onClick={() =>
                                                    filePicker.current.click()
                                                }
                                                className="text-blue-700 w-7 h-7 "
                                            />
                                        </div>
                                        <span className="text-sm absolute md:text-shadow-md text-slate-700 text-shadow-sm bottom-0 md:bottom-1 left-0 right-0 w-[80px] mx-auto">
                                            Add Reels
                                        </span>
                                        <input
                                            ref={filePicker}
                                            className="hidden"
                                            type="file"
                                            onChange={addReels}
                                            multiple
                                        />
                                    </div>
                                </SwiperSlide>
                                {reels &&
                                    reels.map((reel) => (
                                        <SwiperSlide
                                            key={reel.user.id}
                                            className="mt-1 max-w-[100px]"
                                        >
                                            <Reels data={reel} />
                                        </SwiperSlide>
                                    ))}
                            </Swiper>
                            <div
                                className={`hidden md:flex space-x-2 ${
                                    !isValidating && "md:hidden"
                                }`}
                            >
                                {Array.from({ length: 5 }).map((el, idx) => (
                                    <div
                                        key={idx}
                                        className="mt-1 max-w-[100px]"
                                    >
                                        <div
                                            className={`text-center outlined md:bg-slate-400 rounded-xl relative h-28 min-w-[97px] max-w-[100px] md:h-36 md:min-w-[97px] group animate-pulse`}
                                        ></div>
                                    </div>
                                ))}
                            </div>
                        </div>
                    </div>
                    <div className="rounded-xl h-fit bg-white p-4">
                        <CustomTextArea
                            placeholder="What's on your mind, Username?"
                            minRows={2}
                            bordered={true}
                            mutate={magic}
                        />
                    </div>
                    {/* <PostList/> */}
                    {isLoading &&
                        <div className="relative">
                            <div className="flex justify-center absolute -top-5 right-[50%]"><Loader className=""/></div> 
                        </div>
                    }
                    {content &&
                        content.map((post) => (
                            <PostCard key={post.id} data={post} />
                        ))}
                    {/* <button
                        onClick={() => {
                            setSize(size + 1);
                        }}
                    >
                        Load more
                    </button> */}
                </div>
                <div className="space-y-6 md:basis-1/3">
                    <Workspace />
                    <LatestBlog />
                </div>
            </div>
        </>
    );
}
