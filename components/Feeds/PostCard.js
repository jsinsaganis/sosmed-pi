import {
    BellIcon,
    ChatIcon,
    DotsHorizontalIcon,
    HeartIcon,
    PlusIcon,
    ShareIcon,
    ThumbUpIcon,
    TrashIcon,
} from "@heroicons/react/solid";
import axios from "axios";
import Image from "next/image";
import { parseCookies } from "nookies";
import React, { useRef, useState } from "react";
import { BsFillPinAngleFill } from "react-icons/bs";
import { FaRegLaughSquint } from "react-icons/fa";
import { useSWRConfig } from "swr";
import request from "../../utils/request";
import Comments from "./Comments";
import Dropdown from "./Dropdown";
import {mutate} from 'swr'
import { useRecoilState } from "recoil";
import { userData } from "../../atoms/atomState";

function PostCard({ data }) {
    // const {mutate} = useSWRConfig()
    const [profile] = useRecoilState(userData)
    const commentRef = useRef();
    const [value, setValue] = useState("");
    const dropdownList = useState([
        {
            Icon: BsFillPinAngleFill,
            text: "Pin",
        },
        {
            Icon: BellIcon,
            text: "Notification",
        },
        {
            Icon: TrashIcon,
            text: "Delete",
        },
    ]);

    const handleLike = async (id, nik) => {
        const payload = {
            id_feeds: parseInt(id),
            nik: nik,
        };
        

        const cookies = parseCookies();
        const jwt = { cookies };

        await axios.post(request.postLike, payload, {
            headers: {
                Authorization: `Bearer ${jwt.cookies.token}`,
            },
        })
        mutate(request.getFeedLocal+1)
    };

    const handleComment = async (value, id, nik) => {
        const payload = {
            id_feeds: id,
            nik: nik,
            comment: value,
        };

        const cookies = parseCookies();
        const jwt = { cookies };
        await axios
            .post(request.postComment, payload, {
                headers: {
                    Authorization: `Bearer ${jwt.cookies.token}`,
                },
            })
            .then(() => setValue(""))
            .catch((err) => {
                console.log(err);
            })
            .finally(()=> setValue(""))
        mutate(request.getFeedLocal+1);
    };

    return (
        <article className="bg-white rounded-xl p-4 space-y-5 text-sm">
            <div className="flex items-center justify-between">
                <div className="flex space-x-2.5">
                    <div className="relative flex space-x-3 w-14 h-14 rounded-xl overflow-hidden">
                        <Image
                            src={data.user.photo_profile}
                            layout="fill"
                            objectFit="cover"
                        />
                    </div>
                    <div className="">
                        <p className="flex text-md font-bold text-slate-700 items-center space-x-2">
                            <span>{data.user.name}</span> 
                            <span className="inline-block w-[4px] h-[4px] rounded-full bg-slate-400"></span>
                            <span>{data.user.position}</span> 
                        </p>
                        <span className="text-slate-500 text-xs">
                            {data.created_at}
                        </span>
                    </div>
                </div>
                <Dropdown list={dropdownList} Icon={DotsHorizontalIcon} />
            </div>
            <div>
                <p className="">{data.desc}</p>
            </div>
            {data.photo && data.photos.length > 0 ? (
                <section className="relative">
                    <div
                        className={`${
                            data.photos.length < 4 ? "hidden" : ""
                        } absolute bottom-[10%] z-10 right-3 flex items-center text-slate-600`}
                    >
                        <PlusIcon className="h-4 w-4 " />
                        {data.photos.length - 3} more
                    </div>
                    <div className="grid grid-cols-2 grid-rows-2 gap-2 h-64">
                        {data.photos.photos.slice(0, 3).map((photo) => (
                            <div
                                key={photo.id}
                                className={`relative ${
                                    data.photos.total > 2
                                        ? "first:row-span-2"
                                        : data.photos.total == 2
                                        ? "row-span-2"
                                        : "row-span-2 col-span-2"
                                }`}
                            >
                                <Image
                                    layout="fill"
                                    src={photo.image}
                                    objectFit="cover"
                                />
                            </div>
                        ))}
                    </div>
                </section>
            ) : (
                <></>
            )}
            <section className="pb-1 border-b-2">
                <div className="flex justify-between">
                    <div className="flex gap-x-12 items-center">
                        <div className="flex relative">
                            <ThumbUpIcon className="absolute -bottom-2 text-white w-4 h-4 bg-blue-700 rounded-full px-1" />
                            <HeartIcon className="absolute -bottom-2 left-3 text-white w-4 h-4 bg-pink-500 rounded-full px-1" />
                            <FaRegLaughSquint className="absolute -bottom-2 left-6 text-black w-4 h-4 bg-gradient-to-b from-[#FCCA69] to-[#FAA705] rounded-full" />
                        </div>
                        <span className="text-slate-600 text-xs">
                            {data.likes.total > 0 && data.likes.total}
                        </span>
                    </div>
                    <div className="flex gap-x-2 items-center">
                        <ChatIcon className="w-6 h-6 text-slate-400" />
                        <div className="flex items-center gap-x-2  text-slate-700">
                            <p
                                className="cursor-pointer"
                                onClick={() => {
                                    handleShowComment(data.id);
                                }}
                            >
                                {data.comments.total} Comment
                                {data.comments.total > 1 ? "s" : ""}
                            </p>
                            <span className="inline-block w-1.5 h-1.5 rounded-full bg-slate-400"></span>
                            <p className="cursor-pointer">
                                {data.shares} Share
                                {data.shares > 1 ? "s" : ""}{" "}
                            </p>
                        </div>
                    </div>
                </div>
            </section>
            <section className="flex justify-between !mt-3">
                <a
                    className="flex gap-x-1.5 text-blue-700 items-center"
                    onClick={() => {
                        handleLike(data.id, data.user.nik);
                    }}
                >
                    {data.likes.is_like && (
                        <ThumbUpIcon className="text-white w-6 h-6 bg-blue-700 rounded-full px-1" />
                    )}
                    <span className="font-semibold cursor-pointer text-xs">
                        Like{data.likes.is_like && "d"}
                    </span>
                </a>
                <a
                    href="#"
                    className="flex gap-x-1.5 text-slate-500 items-center"
                >
                    <ShareIcon className="w-4 h-4" />
                    <span className="text-xs">Share</span>
                </a>
            </section>
            <section>
                <div className="flex space-x-2 items-start relative">
                    <div className="relative flex space-x-3 w-10 h-10 rounded-xl overflow-hidden">
                        <Image
                            // src={data.user.photo_profile}
                            src={profile.photo_profile}
                            width={40}
                            height={40}
                            objectFit="cover"
                        />
                    </div>
                    <form className="flex flex-col justify-end items-end relative w-full space-y-2">
                        <textarea
                            ref={commentRef}
                            id={data.id}
                            type="text"
                            placeholder="Write a comment..."
                            className={`w-full block pr-10 resize-none bg-slate-100 border-[#E9E9ED] focus:outline-none overflow-hidden rounded-lg p-2 text-slate-600 text-md`}
                            rows="2"
                            value={value}
                            onChange={(e) => {
                                setValue(e.target.value);
                            }}
                        />
                        <button
                            className={`p-2 text-white bg-[#2E55A3] rounded-xl text-xs w-16 ${
                                !value && "hidden"
                            }`}
                            type="submit"
                            onClick={(e) => {
                                e.preventDefault(),
                                    handleComment(
                                        value,
                                        data.id,
                                        data.user.nik
                                    );
                            }}
                        >
                            Submit
                        </button>
                    </form>
                </div>
            </section>
            {/* {post.id == comments.feedId && ( */}

            <Comments comments={data.comments.latest} />
            {/* )} */}
        </article>
    );
}

export default PostCard;
