import {
    ChatIcon,
    HeartIcon,
    PhotographIcon,
    PlusIcon,
    ShareIcon,
    ThumbUpIcon,
    TrashIcon,
} from "@heroicons/react/solid";
import { DotsHorizontalIcon } from "@heroicons/react/solid";
import { FaRegLaughSquint } from "react-icons/fa";
import Image from "next/image";
import React, { useEffect, useRef, useState } from "react";
import Comments from "./Comments";
import Dropdown from "./Dropdown";
import { BsFillPinAngleFill } from "react-icons/bs";
import { BellIcon } from "@heroicons/react/solid";
import { useRecoilState } from "recoil";
import { parseCookies } from "nookies";
import request from "../../utils/request";
import axios from "axios";
import useSWR, { mutate } from "swr";
import useSWRInfinite from "swr/infinite";
import { loading } from "../../atoms/atomState";

function PostList() {
    const cookies = parseCookies();
    const jwt = { cookies };

    const [page, setPage] = useState(1);
    // const [content, setContent] = useState([])

    const fetcher = (url) =>
        axios
            .post(
                url,
                { nik: jwt.cookies.nik},
                {
                    headers: {
                        Authorization: `Bearer ${jwt.cookies.token}`,
                    },
                }
            )
            .then((res) => res.data.data);

    // const { data: content } = useSWR(request.getFeedLocal + page, fetcher, {
    //     revalidateOnFocus: false,
    //     revalidateIfStale: false,
    // });

    const {data: content} = useSWR(request.fetchContentFeeds, fetcher)
    

    // const getKey = (pageIndex, previousPageData) => {
    //     if (previousPageData && !previousPageData.length) return null; // reached the end
    //     return `http://127.0.0.1:8000/api/mobile/getFeeds?page=${
    //         pageIndex + 1
    //     }`; // SWR key
    // };

    // const { data, size, setSize, mutate } = useSWRInfinite(getKey, fetcher, {
    //     initialSize: 1,
    //     revalidateFirstPage: false,
    // });
    // const content = data ? [].concat(...data) : [];

    // useEffect(() => {
    //     console.log("first", data)
    //     setContent(data)
        
    // }, [page])


    const length = content && content.length;
    const [value, setValue] = useState(
        Array.from({ length: length }).map((el) => "")
    );
    const [comments, setComments] = useState([]);
    const commentRef = useRef();
    const dropdownList = useState([
        {
            Icon: BsFillPinAngleFill,
            text: "Pin",
        },
        {
            Icon: BellIcon,
            text: "Notification",
        },
        {
            Icon: TrashIcon,
            text: "Delete",
        },
    ]);

    const loadMoreData = () => {
        // setSize(size + 1);

        setPage((page) => page + 1);
    };

    const handleLike = async (id, nik) => {
        const payload = {
            id_feeds: parseInt(id),
            nik: nik,
        };

        const cookies = parseCookies();
        const jwt = { cookies };

        await axios.post(request.postLike, payload, {
            headers: {
                Authorization: `Bearer ${jwt.cookies.token}`,
            },
        }).finally(() => mutate(request.fetchContentFeeds))


    };

    const handleShowComment = async (id) => {
        const cookies = parseCookies();
        const jwt = { cookies };

        const res = await axios.get(request.showComment + id, {
            headers: {
                Authorization: `Bearer ${jwt.cookies.token}`,
            },
        });

        if (res.data.data.length) {
            const text = {
                feedId: id,
                comments: res.data.data,
            };
            setComments(text);
        }
    };

    const handleComment = async (index, id, nik) => {
        const payload = {
            id_feeds: id,
            nik: nik,
            comment: value[index],
        };

        const cookies = parseCookies();
        const jwt = { cookies };
        await axios
            .post(request.postComment, payload, {
                headers: {
                    Authorization: `Bearer ${jwt.cookies.token}`,
                },
            })
            .then((res) => console.log(res.data.data))
            .catch(() => {
                const newArr = [...value];
                newArr[index] = "";
                setValue(newArr);
                // mutate(request.showComment + id);
            });
        mutate(request.fetchContentFeeds);

    };

    return (
        <section className="space-y-5">
            {content &&
                content.map((post, index) => (
                    <article
                        key={index}
                        className="bg-white rounded-xl p-6 space-y-5 text-sm"
                    >
                        <div className="flex items-center justify-between">
                            <div className="flex space-x-2.5">
                                <div className="relative flex space-x-3 w-14 h-14 rounded-xl overflow-hidden">
                                    <Image
                                        src={post.data_profile.photo_profile}
                                        layout="fill"
                                        objectFit="cover"
                                    />
                                </div>
                                <div className="flex flex-col justify-center">
                                    <p className="text-md font-bold text-slate-700">
                                        {post.data_profile.nama} 
                                    </p>
                                    <span className="text-slate-500 text-xs">
                                        {post.data_profile.diff_time}
                                    </span>
                                </div>
                            </div>
                            <Dropdown
                                list={dropdownList}
                                Icon={DotsHorizontalIcon}
                            />
                        </div>
                        <div>
                            <p className="">{post.desc}</p>
                        </div>
                        {post.photos.length > 0 ? (
                            <section className="relative">
                                <div
                                    className={`${
                                        post.photos.length < 4 ? "hidden" : ""
                                    } absolute bottom-[10%] z-10 right-3 flex items-center text-slate-600`}
                                >
                                    <PlusIcon className="h-4 w-4 " />
                                    {post.photos.length - 3} more
                                </div>
                                <div className="grid grid-cols-2 grid-rows-2 gap-2 h-64">
                                    {post.photos.slice(0, 3).map((photo) => (
                                        <div
                                            key={photo.id}
                                            className={`relative ${
                                                post.photos.length > 2
                                                    ? "first:row-span-2"
                                                    : post.photos.length == 2
                                                    ? "row-span-2"
                                                    : "row-span-2 col-span-2"
                                            }`}
                                        >
                                            <Image
                                                layout="fill"
                                                src={photo.image_url}
                                                objectFit="cover"
                                            />
                                        </div>
                                    ))}
                                </div>
                            </section>
                        ) : (
                            <></>
                        )}
                        <section className="pb-4 border-b-2">
                            <div className="sm:flex justify-between items-center">
                                <div className="flex gap-x-20">
                                    <div className="md:flex relative">
                                        <ThumbUpIcon className="absolute text-white w-6 h-6 bg-blue-700 rounded-full px-1" />
                                        <HeartIcon className="absolute left-5 text-white w-6 h-6 bg-pink-500 rounded-full px-1" />
                                        <FaRegLaughSquint className="absolute left-10 text-black w-6 h-6 bg-gradient-to-b from-[#FCCA69] to-[#FAA705] rounded-full" />
                                    </div>
                                    <span className="text-slate-600 -ml-3 mt-1">
                                        {post.is_like &&
                                        post.jumlah_like > 1 ? (
                                            <>You and </>
                                        ) : (
                                            <>
                                                {post.jumlah_like
                                                    ? post.jumlah_like
                                                    : ""}
                                            </>
                                        )}
                                    </span>
                                </div>
                                <div className="flex gap-x-2 items-center mt-2">
                                    <ChatIcon className="w-6 h-6 text-slate-400" />
                                    <div className="flex items-center gap-x-2  text-slate-700">
                                        <p
                                            className="cursor-pointer"
                                            onClick={() => {
                                                handleShowComment(post.id);
                                            }}
                                        >
                                            {post.jumlah_comment} Comment
                                            {post.jumlah_comment > 1 ? "s" : ""}
                                        </p>
                                        <span className="inline-block w-1.5 h-1.5 rounded-full bg-slate-400"></span>
                                        <p className="cursor-pointer">
                                            {post.shares} Share
                                            {post.shares > 1 ? "s" : ""}{" "}
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <section className="flex justify-between">
                            <a
                                className="flex gap-x-1.5 text-blue-700 items-center"
                                onClick={() => {
                                    handleLike(post.id, post.data_profile.nik);
                                }}
                            >
                                {post.is_like && (
                                    <ThumbUpIcon className="text-white w-6 h-6 bg-blue-700 rounded-full px-1" />
                                )}
                                <span className="font-semibold cursor-pointer">
                                    Like{post.is_like && "d"}
                                </span>
                            </a>
                            <a
                                href="#"
                                className="flex gap-x-1.5 text-slate-500 items-center"
                            >
                                <ShareIcon className="w-5 h-5" />
                                <span>Share</span>
                            </a>
                        </section>
                        <section>
                            <div className="flex space-x-2 items-start relative">
                                <div className="relative flex space-x-3 w-10 h-10 rounded-xl overflow-hidden">
                                    <Image
                                        src={post.data_profile.photo_profile}
                                        width={40}
                                        height={40}
                                        objectFit="cover"
                                    />
                                </div>
                                <form className="flex flex-col justify-end items-end relative w-full space-y-2">
                                    <textarea
                                        ref={commentRef}
                                        id={content.id}
                                        type="text"
                                        placeholder="Write a comment..."
                                        className={`w-full block pr-10 resize-none bg-slate-100 border-[#E9E9ED] focus:outline-none overflow-hidden rounded-lg p-2 text-slate-600 text-md`}
                                        rows="2"
                                        value={value[index]}
                                        onChange={(e) => {
                                            const newInput = [...value];
                                            newInput[index] = e.target.value;
                                            setValue(newInput);
                                        }}
                                    />
                                    <button
                                        className={`p-2 text-white bg-[#2E55A3] rounded-xl text-xs w-16 ${!value[index] && 'hidden'}`}
                                        type="submit"
                                        onClick={(e) => {
                                            e.preventDefault(),
                                                handleComment(
                                                    index,
                                                    post.id,
                                                    post.data_profile.nik
                                                );
                                        }}
                                    >
                                        Submit
                                    </button>
                                </form>
                                {/* <PhotographIcon className="w-5 h-5 text-slate-400 absolute right-3 top-6 hover:text-slate-600 cursor-pointer" /> */}
                            </div>
                        </section>
                        {post.id == comments.feedId && (
                            <Comments comments={comments.comments} />
                        )}
                    </article>
                ))}
            {/* <button
                onClick={() => {
                    loadMoreData(page);
                }}
            >
                Load more
            </button> */}
        </section>
    );
}
export default PostList;
