import { Dialog, Transition } from "@headlessui/react";
import {
    ChevronLeftIcon,
    ChevronRightIcon,
    XIcon,
} from "@heroicons/react/solid";
import axios from "axios";
import Image from "next/image";
import { parseCookies } from "nookies";
import { Fragment, useEffect, useRef, useState } from "react";
import { useCountdown } from "usehooks-ts";
import request from "../../utils/request";

function Reels({ data }) {
    let [isOpen, setIsOpen] = useState(false);
    const [current, setCurrent] = useState(0);
    const [intervalValue, setIntervalValue] = useState(1000);
    const imageElement = useRef()
    const [loaded, setLoaded] = useState(false)
    const [loading, setIsLoading] = useState(false)

    const [count, { startCountdown, stopCountdown, resetCountdown }] =
        useCountdown({
            countStart: 5,
            intervalMs: intervalValue,
        });

    const [imageList, setImageList] = useState([
        // {
        //     url: "/sample-1.png",
        //     id: 1123,
        //     text: "sample-1",
        // },
        // {
        //     url: "/sample-2.png",
        //     id: 212312,
        //     text: "sample-2",
        // },
        // {
        //     url: "/sample-3.png",
        //     id: 212312,
        //     text: "sample-3",
        // },
    ]);

    // let imageList = [
    //     {
    //         url: "/sample-1.png",
    //         id: 1123,
    //         text: 'sample-1'
    //     },
    //     {
    //         url: "/sample-2.png",
    //         id: 212312,
    //         text: 'sample-2'
    //     },
    //     {
    //       url: "/sample-3.png",
    //       id: 212312,
    //       text: 'sample-3'
    //   },
    // ];

    useEffect(() => {
        if (count == 0) {
            nextSlide();
            resetCountdown();
            startCountdown();
            if (imageList.length - 1 === current) {
                // setImageList([
                //     {
                //         url: "/sample-4.png",
                //         id: 1,
                //         text: "sample-4",
                //     },
                // ]);
                resetCountdown();
                setCurrent(0);
                startCountdown();
            }
        }

        // if(!loaded){
        //     stopCountdown()
        // } else {
        //     startCountdown();
        // }
    }, [count]);

    function closeModal() {
        setIsOpen(false);
        resetCountdown();
        stopCountdown();
    }

    function openModal(id) {
        setIsLoading(true)
        const cookies = parseCookies();
        const jwt = { cookies };
        axios.get(request.getReelsById + id, {
            headers: {
                Authorization: `Bearer ${jwt.cookies.token}`,
            },
        }).then(res => {
            setImageList(res.data.data.reels)
            startCountdown();
            setIsOpen(true);
        }).finally(()=> setIsLoading(false))
    }

    const nextSlide = () => {
        setCurrent(current === length - 1 ? 0 : current + 1);
    };

    const prevSlide = () => {
        setCurrent(current === 0 ? length - 1 : current - 1);
    };

    const nextUser = () => {
        console.log("next user");
    };

    const prevUser = () => {
        console.log("prev user");
    };

    const goToImage = (i) => {
        setCurrent(i);
        resetCountdown();
        startCountdown();
    };

    return (
        <>
            <div
                onClick={() => openModal(data.user.id)}
                className={`bg-none md:bg-slate-400 rounded-xl bg-no-repeat relative h-28 min-w-[97px] max-w-[100px] cursor-pointer md:h-36 md:min-w-[97px] md:hover:outlined transition duration-200 ease-out overflow-hidden bg-cover bg-center ${loading && 'animate-pulse'}`}
            >
                <img
                    src={data.reels.thumbnail}
                    className="hidden md:flex object-cover"
                />
                <div className="bg-white relative rounded-full flex items-center justify-center md:h-[40px] md:w-[40px] md:absolute top-1 left-1 border-2 border-white md:rounded-xl  h-[85px] w-[85px] overflow-hidden aspect-1">
                    <Image
                        layout="fill"
                        src={
                            data.user.photo_profile
                                ? data.user.photo_profile
                                : "/avatar.png"
                        }
                        objectFit="cover"
                    />
                </div>
                <p className="truncate text-sm absolute md:text-shadow-md md:text-white text-slate-700 text-shadow-sm bottom-0 md:bottom-1 left-0 right-0 w-[80px] mx-auto">
                    {data.user.name}
                </p>
            </div>

            <Transition appear show={isOpen} as={Fragment}>
                <Dialog as="div" className="relative z-10" onClose={closeModal}>
                    <Transition.Child
                        as={Fragment}
                        enter="ease-out duration-300"
                        enterFrom="opacity-0"
                        enterTo="opacity-100"
                        leave="ease-in duration-200"
                        leaveFrom="opacity-100"
                        leaveTo="opacity-0"
                    >
                        <div className="fixed inset-0 bg-black bg-opacity-25" />
                    </Transition.Child>

                    <div className="fixed inset-0 overflow-y-auto">
                        <div className="flex min-h-full items-center justify-center text-center">
                            <Transition.Child
                                as={Fragment}
                                enter="ease-out duration-300"
                                enterFrom="opacity-0 scale-95"
                                enterTo="opacity-100 scale-100"
                                leave="ease-in duration-200"
                                leaveFrom="opacity-100 scale-100"
                                leaveTo="opacity-0 scale-95"
                            >
                                <Dialog.Panel className="max-w-md transform overflow-hidden rounded-2xl bg-white p-0 text-left align-middle shadow-xl transition-all">
                                    {imageList.map((image, id) => {
                                        return (
                                            <div
                                                className="relative w-full h-[50%]"
                                                key={id}
                                            >
                                                {id === current && (
                                                    <div className="space-y-2 relative">
                                                        <button
                                                            onClick={closeModal}
                                                        >
                                                            <XIcon className="text-slate-500 h-4 w-4 absolute right-1 top-1 hover:text-black transform duration-200 ease-out" />
                                                        </button>
                                                        <p
                                                            className={`text-white opacity-75 font-thin text-xs absolute right-2 bottom-4 transition duration-200 ease-out  text-shadow-xl${
                                                                count < 2
                                                                    ? "hidden"
                                                                    : ""
                                                            }`}
                                                        >
                                                            00:
                                                            {count > 9 ? "" : 0}
                                                            {count}
                                                        </p>
                                                        <img
                                                            alt=""
                                                            className="object-cover mx-auto my-auto  max-h-[80vh]"
                                                            src={image.image}
                                                            ref={imageElement}
                                                            onLoad={() => setLoaded(true)}
                                                        />
                                                        <div className="flex space-x-1 items-center justify-center pb-1">
                                                            {[
                                                                ...Array(
                                                                    imageList.length
                                                                ),
                                                            ].map((e, i) => (
                                                                <div
                                                                    onClick={() =>
                                                                        goToImage(
                                                                            i
                                                                        )
                                                                    }
                                                                    key={i}
                                                                    className={`h-[4px] bg-slate-400 w-3 cursor-pointer hover:bg-black ${
                                                                        id ===
                                                                            i &&
                                                                        "!bg-black !h-2"
                                                                    }`}
                                                                ></div>
                                                            ))}
                                                        </div>
                                                        <ChevronLeftIcon
                                                            onClick={nextUser}
                                                            className="text-slate-500 h-6 w-6 absolute top-[50%] -left-4 cursor-pointer hover:scale-150 hover:text-black transform duration-200 ease-out"
                                                        />
                                                        <ChevronRightIcon
                                                            onClick={prevUser}
                                                            className={`text-slate-500 h-6 w-6 absolute top-[50%] -right-4 cursor-pointer hover:scale-150 hover:text-black transform duration-200 ease-out`}
                                                        />
                                                    </div>
                                                )}
                                            </div>
                                        );
                                    })}
                                </Dialog.Panel>
                            </Transition.Child>
                        </div>
                    </div>
                </Dialog>
            </Transition>
        </>
    );
}

export default Reels;
