import { BellIcon, ChevronDownIcon, SearchIcon } from "@heroicons/react/solid";
import Notification from "./Notification";
import ProfileDropdown from "./ProfileDropdown";

function Header() {
    return (
        <header className="z-10 shadow-lg fixed top-0 right-0 left-0 sm:flex h-20 items-center justify-between bg-white px-[20px] xl:px-[120px] gap-x-6">
            <div className="flex justify-between items-center py-2">
                <img src="/logo-dark.png" alt="logo-pt-pupuk" />
                <div className="flex sm:hidden items-center space-x-2">
                    <SearchIcon className="h-5 w-5" />
                    <svg
                        xmlns="http://www.w3.org/2000/svg"
                        className="h-6 w-6"
                        fill="none"
                        viewBox="0 0 24 24"
                        stroke="currentColor"
                        strokeWidth={3}
                    >
                        <path
                            strokeLinecap="round"
                            strokeLinejoin="round"
                            d="M4 6h16M4 12h16M4 18h16"
                        />
                    </svg>
                </div>
            </div>
            <div className="hidden md:flex items-center">
                <label className="hidden md:block">
                    <input
                        className="min-w-[520px] outline outline-2 outline-[#E9E9ED] rounded-md px-4 py-3"
                        type="text"
                        name="search"
                        placeholder="Search here..."
                    />
                </label>
            </div>
            <div className="flex items-center justify-between">
                <Notification />
                <ProfileDropdown
                    username="username"
                    email="username@gmail.com"
                    Icon={ChevronDownIcon}
                />
            </div>
        </header>
    );
}

export default Header;
