import NavbarComponent from "./Layouts/NavbarComponent";
import SidebarComponent from "./Layouts/SidebarComponent";
import { useRouter } from "next/router";
import { loading } from "../atoms/atomState";
import { useRecoilState } from "recoil";

const Layout = ({ children }) => {
    const router = useRouter();
    const [isLoading] = useRecoilState(loading)
    return (
        <>
            {router.pathname == "/auth" ? (
                <div>{children}</div>
            ) : (
                <div className="container font-inter relative">
                        <div className={`${isLoading && 'bg-slate-400/90 opacity-40 w-full h-full absolute z-50'}`}></div>
                    <NavbarComponent />

                    <div className="content__container ">
                        <div className="hidden md:flex flex-row gap-7">
                            <div className="basis-1/4">
                                <SidebarComponent />
                            </div>

                            <div className="basis-3/4 md:mt-[3rem] lg:mt-[4.2rem]">
                                {children}
                            </div>
                        </div>

                        <div className="md:hidden">
                            <div>
                                <SidebarComponent />
                            </div>

                            <div className="mt-[4.2rem]">{children}</div>
                        </div>
                    </div>
                </div>
            )}
        </>
    );
};

export default Layout;
