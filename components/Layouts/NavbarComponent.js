import { React, useState } from "react";
import { SearchIcon, XIcon } from "@heroicons/react/solid";
import ProfileDropdownComponent from "../Profiles/ProfileDropdownComponent";
import NotificationsDropdownComponent from "../Notifications/NotificationsDropdownComponent";
import useSWR from "swr";
import { parseCookies } from "nookies";
import axios from "axios";
import request from "../../utils/request";

export default function NavbarComponent({ userData }) {
    const [isOpen, setOpen] = useState(false);
    const toggleMenu = () => setOpen(!isOpen);


    const variants = {
        open: { opacity: 1, x: 0 },
        closed: { opacity: 0, x: "-100%" },
    };

    const cookies = parseCookies();
    const jwt = { cookies };
    const nik = jwt.cookies.nik

    const fetcher = (url) =>
        axios
            .get(url, {
                headers: {
                    Authorization: `Bearer ${jwt.cookies.token}`,
                },
            })

    const { data } = useSWR(`https://api-pismart-dev.pupuk-indonesia.com/oauth_api/api/web/v1/users/${nik}`, fetcher, {
        revalidateOnFocus: false,
        revalidateIfStale: false
    });

    const { data: notificationData} = useSWR(data ? request.getNotification+data.data.data.id : null, fetcher, {
        revalidateOnFocus: false,
        revalidateIfStale: false
    })

    return (
        <nav className="nav__container fixed left-0 right-0 top-0 w-full z-[10]">
            <div className="nav__section">
                <a
                    href="https://pupuk-indonesia.com"
                    className={`hidden md:flex items-center`}
                >
                    <img
                        src="https://www.pupuk-indonesia.com/public/bundles/img/logo.png"
                        className="nav__icon"
                        alt="PT Pupuk Indonesia Logo"
                    />
                </a>

                <div className="hidden md:flex input__flex nav__search">
                    <div className="input__groups">
                        <div className="input__groups-icon">
                            <SearchIcon className="input__icon" />
                        </div>
                        <input
                            type="text"
                            id="search"
                            className="input__group"
                            placeholder="Seacrh here ..."
                        />
                    </div>
                </div>

                {/* Mobile Version */}
                <a
                    href="https://pupuk-indonesia.com"
                    className={
                        isOpen ? "hidden" : "flex" + ` md:hidden items-center`
                    }
                >
                    <img
                        src="https://www.pupuk-indonesia.com/public/bundles/img/logo.png"
                        className="nav__icon"
                        alt="PT Pupuk Indonesia Logo"
                    />
                </a>

                {isOpen && (
                    <div
                        className={`flex md:hidden input__flex nav__search mr-2`}
                    >
                        <div className="input__groups">
                            <input
                                type="text"
                                id="search"
                                className="input__group pl-4"
                                placeholder="Seacrh here ..."
                                autoFocus
                            />
                        </div>
                    </div>
                )}
                {/* Mobile Version */}

                <div className="nav__data">
                    {/* Mobile Version */}
                    <button
                        onClick={toggleMenu}
                        className="flex md:hidden justify-center rounded-md bg-transparent relative items-center px-[.5rem] bg-[#F9FAFA] hover:bg-slate-100 outline-none duration-300"
                    >
                        {isOpen && <XIcon className="w-[.9rem] text-black" />}
                        {!isOpen && (
                            <SearchIcon className="w-[.9rem] text-black" />
                        )}
                    </button>
                    {/* Mobile Version */}

                    <NotificationsDropdownComponent data={notificationData} />

                    <div className="hidden md:inline-block">
                        <ProfileDropdownComponent data={data}/>
                    </div>
                </div>
            </div>
        </nav>
    );
}

