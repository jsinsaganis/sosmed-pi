import React, { Component } from "react";
import {
    LogoutIcon,
    StatusOnlineIcon,
    TemplateIcon,
    VideoCameraIcon,
    ViewGridIcon,
} from "@heroicons/react/solid";
import Link from "next/link";
import ProfileDropdownComponent from "../Profiles/ProfileDropdownComponent";
import { useRouter } from "next/router";
import { useAuth } from "../../hooks/auth";
import { useRecoilState } from "recoil";
import { userData } from "../../atoms/atomState";
import Image from "next/image";
import axios from "axios";
import { parseCookies } from "nookies";
import useSWR from "swr";

export default function SidebarComponent() {
    const router = useRouter();
    const { logout } = useAuth()

    // const [profile]= useRecoilState(userData)

    const cookies = parseCookies();
    const jwt = { cookies };
    const nik = jwt.cookies.nik

    const fetcher = async (url) =>
        await axios
            .get(url, {
                headers: {
                    Authorization: `Bearer ${jwt.cookies.token}`,
                },
            }).then((res) => res.data.data)

    const { data } = useSWR(`https://api-pismart-dev.pupuk-indonesia.com/oauth_api/api/web/v1/users/${nik}`, fetcher, {
        revalidateOnFocus: false,
        revalidateIfStale: false
    });
    const profile = data && data.data.data

    return (
        <>
            <div className="bg-white hidden md:block h-max sticky w-full md:top-[6rem] lg:top-[7.2rem] border border-[#E9E9ED] shadow-sm rounded-xl py-5">
                <div className="flex flex-col gap-5 justify-between h-[100%]">
                    <div className="flex p-5 mx-5 mb-2 rounded-xl  justify-center bg-[url('/images/sidebar/morning.png')] md:h-28 lg:h-40  bg-cover">
                        <div className="flex gap-2 flex-col self-end justify-center items-center">
                            <div className="w-12 h-12 lg:w-20 lg:h-20 relative border-2 border-white outlined rounded-full">
                                {profile && <Image src={profile?.photo_profile} alt="profile-picture" layout="fill" objectFit="cover" className="rounded-full overflow-hidden" />}
                            </div>
                            {/* <span className="md:text-[1.3rem] lg:text-[2.5rem]">
                                &#128075;
                            </span> */}
                            <h3 className="text-white font-bold md:text-[.8rem] md:leading-4 md:max-w-[120px] lg:text-lg lg:leading-5 lg:max-w-[180px] text-shadow-sm">
                                Hello, <br />
                                {profile && profile?.name}
                            </h3>
                        </div>
                    </div>

                    <div className="flex-auto md:mb-[6rem] lg:mb-[1.8rem]">
                        <ul>
                            <li>
                                <Link href={`/`}>
                                    <a
                                        className={
                                            `text-[#ABAEBA] flex flex-row items-center md:gap-3 lg:gap-4 w-full hover:bg-[#E2EEFF] hover:text-[#2E55A3] py-3 duration-200 px-5 ` +
                                            (router.pathname == "/"
                                                ? "active"
                                                : "")
                                        }
                                    >
                                        <TemplateIcon className="md:w-[1rem] lg:w-[1.3rem]" />
                                        <span className="font-medium md:text-xs lg:text-sm">
                                            News Feed
                                        </span>
                                    </a>
                                </Link>
                            </li>

                            <li>
                                <Link href={`/apps`}>
                                    <a
                                        className={
                                            `text-[#ABAEBA] flex flex-row items-center md:gap-3 lg:gap-4 w-full hover:bg-[#E2EEFF] hover:text-[#2E55A3] py-3 duration-200 px-5 ` +
                                            (router.pathname == "/apps"
                                                ? "active"
                                                : "")
                                        }
                                    >
                                        <ViewGridIcon className="md:w-[1rem] lg:w-[1.3rem]" />
                                        <span className="font-medium md:text-xs lg:text-sm">
                                            Apps
                                        </span>
                                    </a>
                                </Link>
                            </li>

                            <li>
                                <Link href={`/photos`}>
                                    <a
                                        className={
                                            `text-[#ABAEBA] flex flex-row items-center md:gap-3 lg:gap-4 w-full hover:bg-[#E2EEFF] hover:text-[#2E55A3] py-3 duration-200 px-5 ` +
                                            (router.pathname == "/photos"
                                                ? "active"
                                                : "")
                                        }
                                    >
                                        <VideoCameraIcon className="md:w-[1rem] lg:w-[1.3rem]" />
                                        <span className="font-medium md:text-xs lg:text-sm">
                                            Photos
                                        </span>
                                    </a>
                                </Link>
                            </li>

                            <li>
                                <Link href={`/blogs`}>
                                    <a
                                        className={
                                            `text-[#ABAEBA] flex flex-row items-center md:gap-3 lg:gap-4 w-full hover:bg-[#E2EEFF] hover:text-[#2E55A3] py-3 duration-200 px-5 ` +
                                            (router.pathname == "/blogs"
                                                ? "active"
                                                : "")
                                        }
                                    >
                                        <StatusOnlineIcon className="md:w-[1rem] lg:w-[1.3rem]" />
                                        <span className="font-medium md:text-xs lg:text-sm">
                                            Blogs
                                        </span>
                                    </a>
                                </Link>
                            </li>
                        </ul>
                    </div>

                    <div>
                        <a onClick={logout} className="w-full cursor-pointer flex flex-row md:gap-3 lg:gap-4 items-center text-red-600 hover:bg-red-100 py-3 duration-200 px-5">
                            <LogoutIcon className="md:w-[1rem] lg:w-[1.3rem]" />
                            <span className="font-medium md:text-xs lg:text-sm">
                                Logout
                            </span>
                        </a>
                    </div>
                </div>
            </div>

            {/* Mobile Version */}
            <div className="flex md:hidden max-h-max fixed left-0 bottom-0 z-[10] w-full">
                <div className="border border-slate-200 shadow-xl bg-white w-full px-4 sm:max-w-md sm:mx-auto sm:rounded-lg sm:mb-4">
                    <ul className="flex flex-row gap-2 items-center justify-between">
                        <li className="w-full">
                            <Link href={`/`}>
                                <a
                                    className={
                                        `text-[#ABAEBA] flex flex-col items-center gap-1 hover:bg-white hover:text-[#2E55A3] duration-200 py-2 ` +
                                        (router.pathname == "/" ? "active" : "")
                                    }
                                >
                                    <TemplateIcon className="w-[1.2rem]" />
                                    <span className="font-medium text-[.65rem]">
                                        News
                                    </span>
                                </a>
                            </Link>
                        </li>

                        <li className="w-full">
                            <Link href={`/apps`}>
                                <a
                                    className={
                                        `text-[#ABAEBA] flex flex-col items-center gap-1 hover:bg-white hover:text-[#2E55A3] duration-200 py-2 ` +
                                        (router.pathname == "/apps"
                                            ? "active"
                                            : "")
                                    }
                                >
                                    <ViewGridIcon className="w-[1.2rem]" />
                                    <span className="font-medium text-[.65rem]">
                                        Apps
                                    </span>
                                </a>
                            </Link>
                        </li>

                        <li className="w-full">
                            <Link href={`/photos`}>
                                <a
                                    className={
                                        `text-[#ABAEBA] flex flex-col items-center gap-1 hover:bg-white hover:text-[#2E55A3] duration-200 py-2 ` +
                                        (router.pathname == "/photos"
                                            ? "active"
                                            : "")
                                    }
                                >
                                    <VideoCameraIcon className="w-[1.2rem]" />
                                    <span className="font-medium text-[.65rem]">
                                        Photos
                                    </span>
                                </a>
                            </Link>
                        </li>

                        <li className="w-full">
                            <Link href={`/blogs`}>
                                <a
                                    className={
                                        `text-[#ABAEBA] flex flex-col items-center gap-1 hover:bg-white hover:text-[#2E55A3] duration-200 py-2 ` +
                                        (router.pathname == "/blogs"
                                            ? "active"
                                            : "")
                                    }
                                >
                                    <StatusOnlineIcon className="w-[1.2rem]" />
                                    <span className="font-medium text-[.65rem]">
                                        Blogs
                                    </span>
                                </a>
                            </Link>
                        </li>

                        <li className="block w-full text-center">
                            <ProfileDropdownComponent />
                        </li>
                    </ul>
                </div>
            </div>
            {/* Mobile Version */}
        </>
    );
}
