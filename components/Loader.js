import React from 'react'
import { BiLoader } from 'react-icons/bi'

function Loader() {
  return (
    <div> <BiLoader className='w-5 h-5 animate-spin'/> </div>
  )
}

export default Loader