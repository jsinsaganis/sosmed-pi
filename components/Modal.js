import { Dialog, Transition } from "@headlessui/react";
import { PencilIcon, XIcon } from "@heroicons/react/solid";
import Image from "next/image";
import { Fragment, useState } from "react";

function Modal() {
    let [isOpen, setIsOpen] = useState(false);
    const [addNewSkillFlag, setAddNewSkillFlag] = useState(false);

    const [categories, setCategories] = useState({
        About: {
            name: "Kaido The Kid",
            position: "Captain",
            bio: "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Voluptas distinctio enim ipsa? Facilis debitis eligendi quibusdam ipsa eos distinctio unde?",
            skills: [
                {
                    id: 1,
                    skillName: "Ruby on Rails",
                    skilIcon: "avatar.png",
                    exp: "10 Years Experience",
                    disabled: true,
                },
                {
                    id: 2,
                    skillName: "C++",
                    skilIcon: "avatar.png",
                    exp: "10 Years Experience",
                    disabled: true,
                },
                {
                    id: 3,
                    skillName: "C#",
                    skilIcon: "avatar.png",
                    exp: "15 Years Experience",
                    disabled: true,
                },
            ],
        },
    });

    function closeModal() {
        setIsOpen(false);
    }

    function openModal() {
        setIsOpen(true);
    }

    const handleEditFlag = (id) => {
        let idx = categories.About.skills.findIndex((obj) => obj.id == id);
        let newCategories = { ...categories };
        newCategories.About.skills[idx].disabled = false;
        setCategories(newCategories);
    };

    const handleCancel = (id) => {
        let idx = categories.About.skills.findIndex((obj) => obj.id == id);
        let newCategories = { ...categories };
        newCategories.About.skills[idx].disabled = true;
        setCategories(newCategories);
    };

    const handleSave = (id) => {
        let idx = categories.About.skills.findIndex((obj) => obj.id == id);
        let newCategories = { ...categories };
        newCategories.About.skills[idx].disabled = true;
        setCategories(newCategories);
    };

    return (
        <>
            <button
                onClick={openModal}
                className="w-full block text-white bg-[#2E55A3] py-2 rounded-xl text-sm"
            >
                Edit Skills
            </button>

            <Transition appear show={isOpen} as={Fragment}>
                <Dialog as="div" className="relative z-10" onClose={closeModal}>
                    <Transition.Child
                        as={Fragment}
                        enter="ease-out duration-300"
                        enterFrom="opacity-0"
                        enterTo="opacity-100"
                        leave="ease-in duration-200"
                        leaveFrom="opacity-100"
                        leaveTo="opacity-0"
                    >
                        <div className="fixed inset-0 bg-black bg-opacity-25" />
                    </Transition.Child>

                    <div className="fixed inset-0 overflow-y-auto">
                        <div className="flex min-h-full items-center justify-center p-4 text-center">
                            <Transition.Child
                                as={Fragment}
                                enter="ease-out duration-300"
                                enterFrom="opacity-0 scale-95"
                                enterTo="opacity-100 scale-100"
                                leave="ease-in duration-200"
                                leaveFrom="opacity-100 scale-100"
                                leaveTo="opacity-0 scale-95"
                            >
                                <Dialog.Panel className="w-full max-w-md transform overflow-hidden rounded-2xl bg-white p-6 text-left align-middle shadow-xl transition-all">
                                    <Dialog.Title
                                        as="h3"
                                        className="flex justify-between items-center  text-lg font-medium leading-6 text-gray-900"
                                    >
                                        <span className="uppercase">
                                            edit skills
                                        </span>
                                        <XIcon
                                            onClick={closeModal}
                                            className="w-5 h-5 cursor-pointer"
                                        />
                                    </Dialog.Title>

                                    <div className="bg-white rounded-xl mt-5">
                                        {categories.About.skills.map(
                                            (skill) => (
                                                <div
                                                    key={skill.id}
                                                    className="flex items-center w-full py-2 space-x-2"
                                                >
                                                    <div
                                                        className={`w-10 h-10 relative ${
                                                            !skill.disabled &&
                                                            "hidden"
                                                        }`}
                                                    >
                                                        <Image
                                                            src="/avatar.png"
                                                            layout="fill"
                                                        />
                                                    </div>
                                                    <div
                                                        className={`!mt-0 flex items-center justify-between w-full ${
                                                            !skill.disabled &&
                                                            "!ml-0 flex-col"
                                                        }`}
                                                    >
                                                        <div
                                                            className={`space-y-2 w-full`}
                                                        >
                                                            <label
                                                                className={`capitalize text-sm font-bold ${
                                                                    skill.disabled &&
                                                                    "hidden"
                                                                }`}
                                                            >
                                                                Name
                                                            </label>
                                                            <input
                                                                disabled={
                                                                    skill.disabled
                                                                }
                                                                className={`focus:outline-none w-full disabled:bg-white text-sm font-semibold text-slate-600 !mt-0 ${
                                                                    !skill.disabled &&
                                                                    "p-2 border-[1px] border-gray-600 rounded-lg !mt-2 !text-md "
                                                                }`}
                                                                spellCheck="false"
                                                                defaultValue={
                                                                    skill.skillName
                                                                }
                                                            />
                                                            <label
                                                                className={`capitalize text-sm font-bold ${
                                                                    skill.disabled &&
                                                                    "hidden"
                                                                }`}
                                                            >
                                                                Years of
                                                                Experience
                                                            </label>
                                                            <input
                                                                disabled={
                                                                    skill.disabled
                                                                }
                                                                className={`focus:outline-none w-full disabled:bg-white text-xs text-slate-600 !mt-0 ${
                                                                    !skill.disabled &&
                                                                    "p-2 border-[1px] border-gray-600 rounded-lg !mt-2 !text-md "
                                                                }`}
                                                                spellCheck="false"
                                                                defaultValue={
                                                                    skill.exp
                                                                }
                                                            />
                                                            {/* <span
                          className={`text-xs text-slate-400 !mt-0 ${
                            !skill.disabled && "hidden"
                          }`}
                        >
                          {skill.exp}
                        </span> */}
                                                        </div>
                                                        <PencilIcon
                                                            onClick={() =>
                                                                handleEditFlag(
                                                                    skill.id
                                                                )
                                                            }
                                                            className={`w-4 h-4 text-gray-700 hover:text-gray-900 cursor-pointer border-b-2 border-gray-700 ${
                                                                !skill.disabled &&
                                                                "hidden"
                                                            }`}
                                                        />
                                                        {!skill.disabled && (
                                                            <div className="w-full text-right mt-3 space-x-2">
                                                                <button
                                                                    onClick={() =>
                                                                        handleCancel(
                                                                            skill.id
                                                                        )
                                                                    }
                                                                    className="text-[#2E55A3] bg-[#E2EEFF] py-2 px-4 rounded-xl"
                                                                >
                                                                    Cancel
                                                                </button>
                                                                <button
                                                                    onClick={() =>
                                                                        handleSave(
                                                                            skill.id
                                                                        )
                                                                    }
                                                                    className="text-white bg-[#2E55A3] py-2 px-4 rounded-xl"
                                                                >
                                                                    Save
                                                                </button>
                                                            </div>
                                                        )}
                                                    </div>
                                                </div>
                                            )
                                        )}
                                        {addNewSkillFlag && (
                                            <div className="my-5">
                                                <label
                                                    className={`capitalize text-sm font-bold`}
                                                >
                                                    Name
                                                </label>
                                                <input
                                                    className={`focus:outline-none w-full disabled:bg-white text-sm font-semibold text-slate-600 p-2 border-[1px] rounded-lg border-gray-600 rounded-lg"
                          }`}
                                                    spellCheck="false"
                                                />
                                                <label
                                                    className={`capitalize text-sm font-bold`}
                                                >
                                                    Years of Experience
                                                </label>
                                                <input
                                                    className={`focus:outline-none w-full disabled:bg-white text-sm font-semibold text-slate-600 p-2 border-[1px] rounded-lg border-gray-600 rounded-lg"
                          }`}
                                                    spellCheck="false"
                                                />
                                                <div className="w-full text-right mt-3 space-x-2">
                                                    <button
                                                        onClick={() =>
                                                            setAddNewSkillFlag(
                                                                false
                                                            )
                                                        }
                                                        className="text-[#2E55A3] bg-[#E2EEFF] py-2 px-4 rounded-xl"
                                                    >
                                                        Cancel
                                                    </button>
                                                    <button
                                                        onClick={() =>
                                                            setAddNewSkillFlag(
                                                                false
                                                            )
                                                        }
                                                        className="text-white bg-[#2E55A3] py-2 px-4 rounded-xl"
                                                    >
                                                        Save
                                                    </button>
                                                </div>
                                            </div>
                                        )}
                                        <button
                                            onClick={() =>
                                                setAddNewSkillFlag(true)
                                            }
                                            className="w-full bg-[#E2EEFF] py-2 text-[#2E55A3] font-bold rounded-md"
                                        >
                                            Add More Skills
                                        </button>
                                    </div>
                                </Dialog.Panel>
                            </Transition.Child>
                        </div>
                    </div>
                </Dialog>
            </Transition>
        </>
    );
}

export default Modal;
