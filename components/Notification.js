import { BellIcon } from "@heroicons/react/solid";
import { useEffect, useRef, useState } from "react";
import { Tab } from "@headlessui/react";
import { Fragment } from "react";
import Image from "next/image";

function classNames(...classes) {
    return classes.filter(Boolean).join(" ");
}

function Notification() {
    const [showNoti, setShowNoti] = useState(false);
    const componentRef = useRef();

    useEffect(() => {
        document.addEventListener("click", handleClick);
        return () => document.removeEventListener("click", handleClick);
        function handleClick(e) {
            if (componentRef && componentRef.current) {
                const ref = componentRef.current;
                if (!ref.contains(e.target)) {
                    setShowNoti(false);
                }
            }
        }
    }, [componentRef]);

    return (
        <div className="relative inline-block text-left " ref={componentRef}>
            <div>
                <button
                    className="hidden sm:inline-flex justify-center rounded-md px-4 py-2 text-sm font-medium hover:text-black/75"
                    onClick={() => setShowNoti(!showNoti)}
                >
                    <BellIcon className="h-3 w-3 md:h-5 md:w-5 cursor-pointer" />
                </button>
            </div>

            {showNoti && (
                <div
                    className="origin-top-right absolute right-0 mt-2 w-72 space-y-5 rounded-md shadow-lg bg-white ring-1 ring-black ring-opacity-5 focus:outline-none z-10 px-5 py-6"
                    role="menu"
                >
                    <div className="flex justify-between items-center">
                        <p className="font-bold">Notification</p>
                        <span className="text-xs font-bold text-blue-500 cursor-pointer">
                            Mark all as read
                        </span>
                    </div>
                    <Tab.Group>
                        <Tab.List className="cursor-pointer font-semibold flex text-sm space-x-1 justify-around rounded-xl bg-slate-200 px-6 py-3">
                            <Tab as={Fragment}>
                                {({ selected }) => (
                                    <a
                                        className={
                                            selected
                                                ? "text-blue-500 border-b-blue-400 border"
                                                : ""
                                        }
                                    >
                                        All
                                    </a>
                                )}
                            </Tab>
                            <Tab className="relative">
                                {({ selected }) => (
                                    <a
                                        className={
                                            selected
                                                ? "text-blue-500 border-b-blue-400 border"
                                                : ""
                                        }
                                    >
                                        Unread
                                        <span className="px-1 absolute -top-2 -right-8 h-fit w-fit rounded-full bg-red-600 text-white flex justify-center items-center items">
                                            <span className="text-xs">
                                                999+
                                            </span>
                                        </span>
                                    </a>
                                )}
                            </Tab>
                        </Tab.List>
                        <Tab.Panels>
                            <Tab.Panel>
                                <div className="flex items-center space-x-2">
                                    <Image
                                        src="/avatar.png"
                                        alt="user-pic"
                                        width={40}
                                        height={40}
                                    />
                                    <div className="flex flex-col justify-center text-sm">
                                        <p>Username</p>
                                        <p>Liked your Post - 2 hour ago</p>
                                    </div>
                                </div>
                            </Tab.Panel>
                            <Tab.Panel>
                                <div className="flex items-center space-x-2">
                                    <Image
                                        src="/avatar.png"
                                        alt="user-pic"
                                        width={40}
                                        height={40}
                                    />
                                    <div className="flex flex-col justify-center text-sm">
                                        <p>Username</p>
                                        <p>Liked your Post - 2 hour ago</p>
                                    </div>
                                </div>
                            </Tab.Panel>
                        </Tab.Panels>
                    </Tab.Group>
                    <button className="inline-block w-full bg-blue-200 font-bold text-blue-700 py-2 rounded-xl text-sm">
                        View All
                    </button>
                </div>
            )}
        </div>
    );
}

export default Notification;
