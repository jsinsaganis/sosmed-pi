import { Tab } from "@headlessui/react";
import { BellIcon } from "@heroicons/react/solid";
import { useState } from "react";
import NotificationsDropdownDetailComponent from "./NotificationsDropdownDetailComponent";
import axios from "axios";
import useSWR from "swr";
import { parseCookies } from "nookies";
import request from "../../utils/request";
import Link from "next/link";

function classNames(...classes) {
    return classes.filter(Boolean).join(" ");
}

export default function NotificationsAllComponent() {
    const cookies = parseCookies();
    const jwt = { cookies };
    const nik = jwt.cookies.nik;

    const fetcher = (url) =>
        axios.get(url, {
            headers: {
                Authorization: `Bearer ${jwt.cookies.token}`,
            },
        });

    const { data } = useSWR(
        `https://api-pismart-dev.pupuk-indonesia.com/oauth_api/api/web/v1/users/${nik}`,
        fetcher,
        {
            revalidateOnFocus: false,
            revalidateIfStale: false,
        }
    );

    const { data :notifData, isValidating } = useSWR(
        data ? request.getNotification + data.data.data.id : null,
        fetcher,
        {
            revalidateOnFocus: false,
            revalidateIfStale: false,
        }
    );
    const notificationData = !isValidating && notifData && notifData.data.data
    let [categories] = useState({
        All: [
            {
                username: "Username One",
                photo: "username-one.png",
                date: "2h ago",
                description: "Like your ",
                status: "unread",
                position: "SVP Production",
            },
            {
                username: "Username Two",
                photo: "username-two.png",
                date: "16h ago",
                description: "Like your ",
                status: "unread",
                position: "Head of Engineering",
            },
            {
                username: "Username Three",
                photo: "username-three.png",
                date: "20 June 2022",
                description: "Mentioned you in ",
                status: "read",
                position: "SVP Production",
            },
            {
                username: "Username Four",
                photo: "username-four.png",
                date: "19 June 2022",
                description: "Loved your ",
                status: "read",
                position: "SVP Production",
            },
            {
                username: "Username Five",
                photo: "username-five.png",
                date: "18 June 2022",
                description: "Commented on your ",
                status: "read",
                position: "SVP Production",
            },
        ],
        Unread: [
            {
                username: "Username One",
                photo: "username-one.png",
                date: "2h ago",
                description: "Like your ",
                status: "unread",
                position: "SVP Production",
            },
            {
                username: "Username Two",
                photo: "username-two.png",
                date: "16h ago",
                description: "Like your ",
                status: "unread",
                position: "Head of Engineering",
            },
        ],
    });

    return (
        <Tab.Group as={`div`} className="">
            <Tab.List className="flex gap-3 border border-[#E9E9ED] shadow-md bg-white rounded-xl px-5">
                {Object.keys(categories).map((category) => (
                    <Tab
                        key={category}
                        className={({ selected }) =>
                            classNames(
                                "py-[.8rem] px-2 text-xs lg:text-sm font-medium duration-300 ease-linear outline-none",
                                "hover:text-[#002DF5]",
                                selected
                                    ? "border-b-2 border-[#002DF5] text-[#002DF5]"
                                    : "text-[#818598] border-b-2 border-transparent"
                            )
                        }
                    >
                        {category == "Unread" && (
                            <div className="flex gap-2 items-center">
                                <span>{category}</span>
                                <span className="text-[.5rem] lg:text-[.6rem] font-medium text-white bg-red-500 rounded-full leading-3 px-1 py-0 md:py-0.5">
                                    {notificationData?.unread}
                                </span>
                            </div>
                        )}
                        {category == "All" && <span>{category}</span>}
                    </Tab>
                ))}
            </Tab.List>
            <Tab.Panels className="mt-6 min-h-[90vh]">
                    <Tab.Panel className={classNames("outline-none")}>
                        <div className="space-y-3">
                            {notificationData?.notifications?.map((notif) => (
                                <div
                                    key={notif.id}
                                    className={
                                        "flex justify-between items-start sm:items-center rounded-xl hover:bg-[#E2EEFF] p-2 md:p-3 lg:p-4 duration-200 border border-slate-200 " +
                                        (!notif.is_read ? "bg-[#E2EEFF]" : "bg-white")
                                    }
                                >
                                    <div className="dropdown__menu-profile cursor-pointer items-start sm:items-center">
                                        <div className="relative">
                                            {!notif.is_read && (
                                                <div className="sm:hidden absolute bg-red-500 rounded-full aspect-square h-2 -top-0.5 -left-0.5 border border-[#E2EEFF]" />
                                            )}

                                            <img
                                                className="dropdown__menu-profile-img w-[32px] h-[32px] lg:w-[40px] lg:h-[40px]"
                                                layout="fill"
                                                src={notif.user.photo_profile}
                                            />
                                        </div>

                                        <div className="flex flex-col items-start gap-2 sm:gap-0.5 lg:gap-1">
                                            <div className="flex flex-col gap-0 sm:flex-row sm:gap-1 sm:items-center">
                                                <span className="text-[.72rem] lg:text-[.82rem] font-bold">
                                                    {notif.user.name}
                                                </span>
                                                <span className="hidden sm:block ml-0.5 text-slate-500 text-[1.5rem] lg:text-[2rem] leading-[0] pb-[1px] lg:pb-[3px]">
                                                    &middot;
                                                </span>
                                                <span className="ml-0 sm:ml-0.5 text-[.6rem] lg:text-[.65rem] font-normal text-slate-500">
                                                    {notif.user.position}
                                                </span>

                                                {!notif.is_read && (
                                                    <span className="hidden sm:block ml-0.5 text-[.5rem] lg:text-[.6rem] font-medium text-white bg-red-500 rounded-full leading-3 px-1 lg:px-2 py-0 lg:py-0.5">
                                                        Unread
                                                    </span>
                                                )}
                                            </div>
                                            <div className="text-[.6rem] lg:text-[.65rem] font-normal flex flex-col gap-0 sm:flex-row sm:gap-1 sm:items-center">
                                                <span className="text-slate-900">
                                                    {notif.message}{" "}
                                                    <Link href="/notifications">
                                                        <a
                                                            className="text-blue-700 hover:underline duration-100"
                                                        >
                                                            Post
                                                        </a>
                                                    </Link>
                                                </span>
                                                <span className="hidden sm:block ml-0.5 text-slate-500 text-[1.5rem] lg:text-[2rem] leading-[0] pb-[1px] lg:pb-[3px]">
                                                    &middot;
                                                </span>
                                                <span className="ml-0 sm:ml-1 text-slate-500">
                                                    {notif.created_at}
                                                </span>
                                            </div>
                                        </div>
                                    </div>

                                    <div>
                                        <NotificationsDropdownDetailComponent />
                                    </div>
                                </div>
                            ))}
                        </div>
                    </Tab.Panel>
                    <Tab.Panel className={classNames("outline-none")}>
                        <div className="space-y-3">
                            {notificationData?.notifications?.map((notif) => !notif.is_read && ( 
                                <div
                                    key={notif.id}
                                    className={
                                        "flex justify-between items-start sm:items-center rounded-xl hover:bg-[#E2EEFF] p-2 md:p-3 lg:p-4 duration-200 border border-slate-200 " +
                                        (!notif.is_read ? "bg-[#E2EEFF]" : "bg-white")
                                    }
                                >
                                    <div className="dropdown__menu-profile cursor-pointer items-start sm:items-center">
                                        <div className="relative">
                                            {!notif.is_read && (
                                                <div className="sm:hidden absolute bg-red-500 rounded-full aspect-square h-2 -top-0.5 -left-0.5 border border-[#E2EEFF]" />
                                            )}

                                            <img
                                                className="dropdown__menu-profile-img w-[32px] h-[32px] lg:w-[40px] lg:h-[40px]"
                                                layout="fill"
                                                src={notif.user.photo_profile}
                                            />
                                        </div>

                                        <div className="flex flex-col items-start gap-2 sm:gap-0.5 lg:gap-1">
                                            <div className="flex flex-col gap-0 sm:flex-row sm:gap-1 sm:items-center">
                                                <span className="text-[.72rem] lg:text-[.82rem] font-bold">
                                                    {notif.user.name}
                                                </span>
                                                <span className="hidden sm:block ml-0.5 text-slate-500 text-[1.5rem] lg:text-[2rem] leading-[0] pb-[1px] lg:pb-[3px]">
                                                    &middot;
                                                </span>
                                                <span className="ml-0 sm:ml-0.5 text-[.6rem] lg:text-[.65rem] font-normal text-slate-500">
                                                    {notif.user.position}
                                                </span>

                                                {!notif.is_read && (
                                                    <span className="hidden sm:block ml-0.5 text-[.5rem] lg:text-[.6rem] font-medium text-white bg-red-500 rounded-full leading-3 px-1 lg:px-2 py-0 lg:py-0.5">
                                                        Unread
                                                    </span>
                                                )}
                                            </div>
                                            <div className="text-[.6rem] lg:text-[.65rem] font-normal flex flex-col gap-0 sm:flex-row sm:gap-1 sm:items-center">
                                                <span className="text-slate-900">
                                                    {notif.message}{" "}
                                                    <Link href="/notifications">
                                                    <a className="text-blue-700 hover:underline duration-100">
                                                        Post
                                                    </a>
                                                    </Link>
                                                </span>
                                                <span className="hidden sm:block ml-0.5 text-slate-500 text-[1.5rem] lg:text-[2rem] leading-[0] pb-[1px] lg:pb-[3px]">
                                                    &middot;
                                                </span>
                                                <span className="ml-0 sm:ml-1 text-slate-500">
                                                    {notif.created_at}
                                                </span>
                                            </div>
                                        </div>
                                    </div>

                                    <div>
                                        <NotificationsDropdownDetailComponent />
                                    </div>
                                </div>
                            ))}
                        </div>
                    </Tab.Panel>
                    
            </Tab.Panels>
        </Tab.Group>
    );
}
