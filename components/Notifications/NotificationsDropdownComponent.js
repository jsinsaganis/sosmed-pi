import { Menu, Transition, Tab } from "@headlessui/react";
import { useState, Fragment, forwardRef, useEffect } from "react";
import { BellIcon } from "@heroicons/react/solid";
import Link from "next/link";

function classNames(...classes) {
    return classes.filter(Boolean).join(" ");
}

const MyLink = forwardRef((props, ref) => {
    let { href, children, ...rest } = props;
    return (
        <Link href={href}>
            <a ref={ref} {...rest}>
                {children}
            </a>
        </Link>
    );
});

MyLink.displayName = "MyLink"

export default function NotificationsDropdownComponent({data}) {
    const [notifData, setNotifData] = useState()
    let [categories] = useState({
        All: [
            {
                username: "Username One",
                photo: "username-one.png",
                date: "2h ago",
                description: "Like your ",
                status: "unread",
            },
            {
                username: "Username Two",
                photo: "username-two.png",
                date: "16h ago",
                description: "Like your ",
                status: "unread",
            },
            {
                username: "Username Three",
                photo: "username-three.png",
                date: "20 June 2022",
                description: "Mentioned you in ",
                status: "read",
            },
            {
                username: "Username Four",
                photo: "username-four.png",
                date: "19 June 2022",
                description: "Loved your ",
                status: "read",
            },
            {
                username: "Username Five",
                photo: "username-five.png",
                date: "18 June 2022",
                description: "Commented on your ",
                status: "read",
            },
        ],
        Unread: [
            {
                username: "Username One",
                photo: "username-one.png",
                date: "2h ago",
                description: "Like your ",
                status: "unread",
            },
            {
                username: "Username Two",
                photo: "username-two.png",
                date: "16h ago",
                description: "Like your ",
                status: "unread",
            },
        ],
    });

    useEffect(() => {
        if(data){
            setNotifData(data.data.data)
        }
    },[data])



    return (
        <Menu as="div" className="dropdown__menu self-center">
            {({ open }) => (
                <>
                    <Menu.Button className="dropdown__menu-btn nav__btn-notif">
                        <BellIcon className="notif__icon" />
                        <div className="nav__notif-count">{notifData?.unread}</div>
                    </Menu.Button>
                    <Transition
                        as={Fragment}
                        show={open}
                        enter="transition ease-out duration-100"
                        enterFrom="transform opacity-0 scale-95"
                        enterTo="transform opacity-100 scale-100"
                        leave="transition ease-in duration-75"
                        leaveFrom="transform opacity-100 scale-100"
                        leaveTo="transform opacity-0 scale-95"
                    >
                        <Menu.Items
                            className="dropdown__menu-items w-72 md:w-[360px] right-0 mt-2 shadow-lg"
                            unmount
                        >
                            <div className="dropdown__menu-items-container px-0 py-4">
                                <Menu.Item>
                                    <div className="flex gap-3 items-center justify-between pl-6 pr-5 mb-4">
                                        <h3 className="font-bold text-sm lg:text-md text-[#393C46]">
                                            Notification
                                        </h3>
                                        <button className="font-bold text-[.72rem] lg:text-xs text-[#001A8F] px-2 py-1 rounded-md hover:bg-[#E2EEFF] duration-150">
                                            Mark all as read
                                        </button>
                                    </div>
                                </Menu.Item>

                                <Tab.Group as="div">
                                    <Tab.List className="flex gap-2 rounded-xl bg-[#F9FAFA] border border-[#E9E9ED] px-4 mx-5">
                                        {Object.keys(categories).map(
                                            (category) => (
                                                <Tab
                                                    key={category}
                                                    className={({ selected }) =>
                                                        classNames(
                                                            "py-[.6rem] px-2 text-xs lg:text-sm font-medium duration-150",
                                                            "hover:text-[#002DF5]",
                                                            selected
                                                                ? "border-b-2 border-[#002DF5] text-[#002DF5]"
                                                                : "text-[#818598] border-b-2 border-transparent"
                                                        )
                                                    }
                                                >
                                                    {category == "Unread" && (
                                                        <div className="flex gap-2 items-center">
                                                            <span>
                                                                {category}
                                                            </span>
                                                            <span className="text-[.5rem] lg:text-[.6rem] font-medium text-white bg-red-500 rounded-full leading-3 px-1 py-0 md:py-0.5">
                                                            {notifData?.unread}
                                                            </span>
                                                        </div>
                                                    )}
                                                    {category == "All" && (
                                                        <span>{category}</span>
                                                    )}
                                                </Tab>
                                            )
                                        )}
                                    </Tab.List>
                                    <Tab.Panels className="notif__tab mt-4 max-h-[240px] lg:max-h-[320px] overflow-y-auto">
                                        <Tab.Panel
                                            className={classNames(
                                                'bg-white',
                                                'outline-none'
                                            )}
                                        >
                                            <ul>
                                                {notifData?.notifications.map((notif) => (
                                                    <li key={notif.id} className={"relative hover:bg-[#E2EEFF] py-2 px-4 duration-100 " + (!notif.is_read ? 'bg-[#E2EEFF]' : 'bg-white')}>
                                                        <div className="dropdown__menu-profile cursor-pointer w-full">
                                                            <img className="dropdown__menu-profile-img w-[32px] h-[32px] lg:w-[36px] lg:h-[36px] object-cover"
                                                                layout="fill"
                                                                src={notif.user.photo_profile}
                                                            />
                                                            <div className="dropdown__menu-profile-data">
                                                                <div className="flex gap-1 items-center">
                                                                    <span className='text-[.72rem] lg:text-[.82rem]'>{notif.user.name}</span>

                                                                    {!notif.is_read &&
                                                                        <span className="text-[.5rem] lg:text-[.6rem] font-medium text-white bg-red-500 rounded-full leading-3 px-1 lg:px-2 py-0 lg:py-0.5">Unread</span>
                                                                    }
                                                                </div>
                                                                <div className="text-[.6rem] lg:text-[.65rem] font-normal flex items-center">
                                                                    <span className="text-slate-900">{notif.message} 
                                                                    <Link href="/notifications">
                                                                        <a  className="text-blue-700 hover:underline duration-100">Post</a>
                                                                    </Link>
                                                                    
                                                                    </span>
                                                                    <span className="ml-1 text-slate-500 text-[2rem] leading-[0] pb-[3px]">&middot;</span>
                                                                    <span className="ml-1 text-slate-500">{notif.created_at}</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </li>
                                                ))}
                                            </ul>
                                        </Tab.Panel>
                                        <Tab.Panel
                                            className={classNames(
                                                'bg-white',
                                                'outline-none'
                                            )}
                                        >
                                            <ul>
                                                {notifData?.notifications.map((notif) => !notif.is_read && (
                                                    <li key={notif.id} className={"relative hover:bg-[#E2EEFF] py-2 px-4 duration-100 " + (!notif.is_read ? 'bg-[#E2EEFF]' : 'bg-white')}>
                                                        <div className="dropdown__menu-profile cursor-pointer w-full">
                                                            <img className="dropdown__menu-profile-img w-[32px] h-[32px] lg:w-[36px] lg:h-[36px] object-cover"
                                                                layout="fill"
                                                                src={notif.user.photo_profile}
                                                            />
                                                            <div className="dropdown__menu-profile-data">
                                                                <div className="flex gap-1 items-center">
                                                                    <span className='text-[.72rem] lg:text-[.82rem]'>{notif.user.username}</span>

                                                                    {!notif.is_read &&
                                                                        <span className="text-[.5rem] lg:text-[.6rem] font-medium text-white bg-red-500 rounded-full leading-3 px-1 lg:px-2 py-0 lg:py-0.5">Unread</span>
                                                                    }
                                                                </div>
                                                                <div className="text-[.6rem] lg:text-[.65rem] font-normal flex items-center">
                                                                    <span className="text-slate-900">{notif.message} 
                                                                        <Link href="/notifications">
                                                                            <a className="text-blue-700 hover:underline duration-100">Post</a>
                                                                        </Link>
                                                                    </span>
                                                                    <span className="ml-1 text-slate-500 text-[2rem] leading-[0] pb-[3px]">&middot;</span>
                                                                    <span className="ml-1 text-slate-500">{notif.created_at}</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </li>
                                                ))}
                                            </ul>
                                        </Tab.Panel>
                                    </Tab.Panels>
                                </Tab.Group>
                                
                                <Menu.Item>
                                    <MyLink href={`/notifications`}>
                                        <div className="mt-4 mx-4">
                                            <span className="inline-block text-center button dropdown__btn-notification">
                                                View All
                                            </span>
                                        </div>
                                    </MyLink>
                                </Menu.Item>
                            </div>
                        </Menu.Items>
                    </Transition>
                </>
            )}
        </Menu>
    );
}
