import { Menu, Transition } from "@headlessui/react";
import { Fragment, useEffect, useRef, useState } from "react";
import {
    DotsHorizontalIcon,
    CheckCircleIcon,
    TrashIcon,
} from "@heroicons/react/solid";
import axios from "axios";
import useSWR from "swr";
import { parseCookies } from "nookies";
import request from "../../utils/request";

export default function NotificationsDropdownDetailComponent() {

    return (
        <Menu as="div" className="dropdown__menu mr-1">
            <div>
                <Menu.Button className="">
                    <DotsHorizontalIcon className="w-3 lg:w-5 text-[#818598] hover:text-black duration-200 pt-1" />
                </Menu.Button>
            </div>
            <Transition
                as={Fragment}
                enter="transition ease-out duration-100"
                enterFrom="transform opacity-0 scale-95"
                enterTo="transform opacity-100 scale-100"
                leave="transition ease-in duration-75"
                leaveFrom="transform opacity-100 scale-100"
                leaveTo="transform opacity-0 scale-95"
            >
                <Menu.Items className="dropdown__menu-items z-[100] w-40 mt-0 right-0">
                    <div className="dropdown__menu-items-container">
                        <Menu.Item>
                            {({ active }) => (
                                <button className="group dropdown__menu-item hover:bg-slate-100 text-[.65rem] xl:text-xs">
                                    <CheckCircleIcon
                                        className="dropdown__menu-item-icon"
                                        aria-hidden="true"
                                    />
                                    Mark as read
                                </button>
                            )}
                        </Menu.Item>

                        <Menu.Item>
                            {({ active }) => (
                                <button className="group dropdown__menu-item text-red-600 hover:bg-red-50 md:text-[.65rem] xl:text-xs">
                                    <TrashIcon
                                        className="dropdown__menu-item-icon"
                                        aria-hidden="true"
                                    />
                                    Delete Post
                                </button>
                            )}
                        </Menu.Item>
                    </div>
                </Menu.Items>
            </Transition>
        </Menu>
    );
}
