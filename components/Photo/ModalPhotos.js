import { Dialog, Transition } from "@headlessui/react";
import {
    ChatIcon,
    DotsHorizontalIcon,
    HeartIcon,
    PhotographIcon,
    ShareIcon,
    ThumbUpIcon,
} from "@heroicons/react/solid";
import { FaRegLaughSquint } from "react-icons/fa";
import { AiOutlineLoading } from "react-icons/ai";
import Image from "next/image";
import { Fragment, useEffect, useState } from "react";
import axios from "axios";
import request from "../../utils/request";
import { parseCookies } from "nookies";
import useSWR, { mutate } from "swr";
import { useRecoilState } from "recoil";
import { userData } from "../../atoms/atomState";

function cn(...classes) {
    return classes.filter(Boolean).join(" ");
}

export default function ModalPhotos({ list }) {
    let [isOpen, setIsOpen] = useState(false);
    const [comment, setComment] = useState("");
    const [isLoading, setLoading] = useState(true);
    const [loadingComment, setLoadingComment] = useState(false);
    const [loadData, setLoadData] = useState(true);
    const [slug, setSlug] = useState();
    const [slugArr, setSlugArr] = useState([]);
    const cookies = parseCookies();
    const jwt = { cookies };

    const [profile] = useRecoilState(userData);

    useEffect(() => {
        const slugs = [];
        for (let i = 0; i < list.length; i++) {
            slugs.push(list[i].slug);
        }
        setSlugArr(slugs);
    }, []);

    function closeModal() {
        setIsOpen(false);
        setSlug(null);
    }

    function openModal() {
        setIsOpen(true);
    }

    const handleShowPhoto = async (slug) => {
        setSlug(slug);
        setIsOpen(true);
        setLoadData(true);
    };

    const fetcher = (url) =>
        axios
            .get(url, {
                headers: {
                    Authorization: `Bearer ${jwt.cookies.token}`,
                },
            })
            .then((res) => res.data.photos);

    const { data: modalData, isValidating } = useSWR(
        slug ? request.getPhotoById + slug : null,
        fetcher,
        { revalidateOnFocus: false }
    );

    useEffect(() => {
        if (modalData) {
            setLoadData(false);
        }
    }, [modalData]);

    const nextPhoto = (slug) => {
        setLoadData(true);
        setSlug(null);
        const idx = slugArr.indexOf(slug);
        const index = idx + 1 === slugArr.length ? 0 : idx + 1;
        setSlug(slugArr[index]);
    };

    const prevPhoto = (slug) => {
        setLoadData(true);
        setSlug(null);
        const idx = slugArr.indexOf(slug);
        const index = idx === 0 ? slugArr.length - 1 : idx - 1;
        setSlug(slugArr[index]);
    };

    const postComment = async (comment, id) => {
        setLoadingComment(true);
        const payload = {
            comment: comment,
            photo_id: id,
        };
        await axios.post(request.postPhotoComment, payload, {
            headers: {
                Authorization: `Bearer ${jwt.cookies.token}`,
            },
        });
        setComment("");
        mutate(request.getPhotoById + slug);
        setLoadingComment(false);
    };

    return (
        <>
            {list.map((photo) => (
                <li
                    key={photo.id}
                    className="relative bg-gray-200 rounded-lg overflow-hidden h-[250px] group cursor-pointer"
                >
                    <Image
                        alt=""
                        src={photo.photo}
                        layout="fill"
                        objectFit="cover"
                        className={cn(
                            "group-hover:opacity-75 duration-700 ease-in-out",
                            isLoading
                                ? "grayscale blur-2xl scale-110"
                                : "grayscale-0 blur-0 scale-100"
                        )}
                        onLoadingComplete={() => setLoading(false)}
                        onClick={() => handleShowPhoto(photo.slug)}
                    />
                </li>
            ))}

            <Transition appear show={isOpen} as={Fragment}>
                <Dialog as="div" className="relative z-10" onClose={closeModal}>
                    <Transition.Child
                        as={Fragment}
                        enter="ease-out duration-300"
                        enterFrom="opacity-0"
                        enterTo="opacity-100"
                        leave="ease-in duration-200"
                        leaveFrom="opacity-100"
                        leaveTo="opacity-0"
                    >
                        <div className="fixed inset-0 bg-black bg-opacity-25" />
                    </Transition.Child>

                    <div className="font-inter fixed inset-0 overflow-y-auto z-50 scrollbar-hide">
                        <div className="flex min-h-full items-center justify-center p-4 text-center">
                            <Transition.Child
                                as={Fragment}
                                enter="ease-out duration-300"
                                enterFrom="opacity-0 scale-95"
                                enterTo="opacity-100 scale-100"
                                leave="ease-in duration-200"
                                leaveFrom="opacity-100 scale-100"
                                leaveTo="opacity-0 scale-95"
                            >
                                <Dialog.Panel className="md:flex w-full space-y-0 md:space-y-0 max-w-max md:space-x-4 transform overflow-hidden rounded-xl bg-white p-5 text-left shadow-xl transition-all">
                                    {loadData && (
                                        <div
                                            className={`basis-1/2 bg-slate-400 w-[450px] rounded-xl animate-pulse`}
                                        ></div>
                                    )}
                                    <div
                                        className={`relative basis-2/3 hidden md:inline-block`}
                                    >
                                        <img
                                            className="rounded-xl object-cover max-h-[90vh] w-full"
                                            src={modalData?.photo}
                                        />
                                        <svg
                                            xmlns="http://www.w3.org/2000/svg"
                                            className="h-6 w-6 absolute bg-white rounded-full top-[50%] md:top-[50%] left-4 cursor-pointer p-1 shadow-md shadow-black hover:bg-slate-400 hover:text-white transform duration-200 ease-out"
                                            fill="none"
                                            viewBox="0 0 24 24"
                                            stroke="currentColor"
                                            strokeWidth={2}
                                            onClick={() =>
                                                prevPhoto(modalData.slug)
                                            }
                                        >
                                            <path
                                                strokeLinecap="round"
                                                strokeLinejoin="round"
                                                d="M15 19l-7-7 7-7"
                                            />
                                        </svg>
                                        <svg
                                            xmlns="http://www.w3.org/2000/svg"
                                            className="h-6 w-6 absolute bg-white rounded-full top-[50%] md:top-[50%] right-4 cursor-pointer p-1 shadow-md shadow-black hover:bg-slate-400 hover:text-white transform duration-200 ease-out"
                                            fill="none"
                                            viewBox="0 0 24 24"
                                            stroke="currentColor"
                                            strokeWidth={2}
                                            onClick={() =>
                                                nextPhoto(modalData.slug)
                                            }
                                        >
                                            <path
                                                strokeLinecap="round"
                                                strokeLinejoin="round"
                                                d="M9 5l7 7-7 7"
                                            />
                                        </svg>
                                    </div>
                                    <div className="overflow-y-auto max-h-[450px] scrollbar-hide basis-1/3">
                                        <Dialog.Description
                                            as="h3"
                                            className="text-lg leading-6 text-gray-900 space-y-4"
                                        >
                                            <div className="flex items-center justify-between">
                                                <div className="flex space-x-3">
                                                    <div className="relative w-12 h-12">
                                                        <Image
                                                            src={
                                                                modalData
                                                                    ?.created_by
                                                                    ?.photo_profile
                                                            }
                                                            layout="fill"
                                                            objectFit="cover"
                                                            className="rounded-xl"
                                                        />
                                                    </div>
                                                    <div className="flex justify-between items-center">
                                                        <div>
                                                            <h3 className="text-sm font-normal">
                                                                {
                                                                    modalData
                                                                        ?.created_by
                                                                        ?.name
                                                                }
                                                            </h3>
                                                            <span className="text-xs text-slate-400">
                                                                {
                                                                    modalData?.created_at
                                                                }
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <DotsHorizontalIcon className="w-6 h-6 text-slate-400 hover:text-slate-700 cursor-pointer" />
                                            </div>

                                            <div>
                                                <p className="inline-block max-w-md text-slate-700 text-sm">
                                                    {modalData?.desc}
                                                </p>
                                            </div>
                                            <div className="relative md:hidden">
                                                <img
                                                    className="rounded-xl object-cover"
                                                    src={modalData?.photo}
                                                />
                                                <svg
                                                    xmlns="http://www.w3.org/2000/svg"
                                                    className="h-6 w-6 absolute bg-white rounded-full top-[50%] md:top-[50%] left-4 cursor-pointer p-1 shadow-md shadow-black hover:bg-slate-400 hover:text-white transform duration-200 ease-out"
                                                    fill="none"
                                                    viewBox="0 0 24 24"
                                                    stroke="currentColor"
                                                    strokeWidth={2}
                                                    onClick={() =>
                                                        prevPhoto(
                                                            modalData.slug
                                                        )
                                                    }
                                                >
                                                    <path
                                                        strokeLinecap="round"
                                                        strokeLinejoin="round"
                                                        d="M15 19l-7-7 7-7"
                                                    />
                                                </svg>
                                                <svg
                                                    xmlns="http://www.w3.org/2000/svg"
                                                    className="h-6 w-6 absolute bg-white rounded-full top-[50%] md:top-[50%] right-4 cursor-pointer p-1 shadow-md shadow-black hover:bg-slate-400 hover:text-white transform duration-200 ease-out"
                                                    fill="none"
                                                    viewBox="0 0 24 24"
                                                    stroke="currentColor"
                                                    strokeWidth={2}
                                                    onClick={() =>
                                                        nextPhoto(
                                                            modalData.slug
                                                        )
                                                    }
                                                >
                                                    <path
                                                        strokeLinecap="round"
                                                        strokeLinejoin="round"
                                                        d="M9 5l7 7-7 7"
                                                    />
                                                </svg>
                                            </div>
                                            <section className="pb-4 border-b-2 text-sm">
                                                <div className="flex justify-between items-center">
                                                    <div className="flex gap-x-12 items-start">
                                                        <div className="flex relative">
                                                            <ThumbUpIcon className="absolute text-white w-4 h-4 bg-blue-700 rounded-full px-1" />
                                                            <HeartIcon className="absolute left-3 text-white w-4 h-4 bg-pink-500 rounded-full px-1" />
                                                            <FaRegLaughSquint className="absolute left-6 text-black w-4 h-4 bg-gradient-to-b from-[#FCCA69] to-[#FAA705] rounded-full" />
                                                        </div>
                                                        <span className="text-xs text-slate-600">
                                                            {modalData?.likes
                                                                ?.total > 0 &&
                                                                modalData?.likes
                                                                    ?.total}
                                                        </span>
                                                    </div>
                                                    <div className="flex gap-x-2 items-center">
                                                        <ChatIcon className="w-6 h-6 text-slate-400" />
                                                        <p className="flex items-center gap-x-2 text-xs text-slate-700 cursor-pointer">
                                                            {modalData?.comments
                                                                ?.total > 0 &&
                                                                modalData
                                                                    ?.comments
                                                                    ?.total}{" "}
                                                            Comment
                                                            {modalData?.comments
                                                                ?.total > 1 && (
                                                                <>s</>
                                                            )}
                                                            <span className="inline-block w-1.5 h-1.5 rounded-full bg-slate-400"></span>{" "}
                                                            Share
                                                        </p>
                                                    </div>
                                                </div>
                                            </section>
                                            <section className="flex justify-between text-sm">
                                                <a
                                                    href="#"
                                                    className="flex gap-x-1.5 text-blue-700 items-center outline-none"
                                                >
                                                    <ThumbUpIcon className="text-white w-4 h-4 bg-blue-700 rounded-full px-1" />
                                                    <span className="font-semibold text-xs">
                                                        Like
                                                    </span>
                                                </a>
                                                <a
                                                    href="#"
                                                    className="flex gap-x-1.5 text-slate-500 items-center"
                                                >
                                                    <ShareIcon className="w-4 h-4" />
                                                    <span className="text-xs">
                                                        Share
                                                    </span>
                                                </a>
                                            </section>
                                            <section>
                                                <div className="flex space-x-1.5 items-start relative">
                                                    <Image
                                                        src={
                                                            profile.photo_profile
                                                        }
                                                        width={40}
                                                        height={40}
                                                        className="rounded-xl"
                                                    />
                                                    <textarea
                                                        placeholder="Write a comment..."
                                                        className={`w-full pr-10 resize-none bg-slate-100 border-[#E9E9ED] focus:outline-none overflow-hidden rounded-lg p-2 text-slate-600 text-sm`}
                                                        rows="2"
                                                        value={comment}
                                                        onChange={(e) =>
                                                            setComment(
                                                                e.target.value
                                                            )
                                                        }
                                                        disabled={
                                                            loadingComment
                                                        }
                                                    />
                                                    {loadingComment && (
                                                        <button>
                                                            <AiOutlineLoading className="absolute right-4 top-[30%] text-white w-4 h-4 bg-blue-500 rounded-full px-1 animate-spin" />
                                                        </button>
                                                    )}
                                                    {!loadingComment && (
                                                        <button
                                                            onClick={() =>
                                                                postComment(
                                                                    comment,
                                                                    modalData.id,
                                                                    modalData.slug
                                                                )
                                                            }
                                                            className="absolute right-2 top-[30%] text-xs text-blue-500 hover:scale-95"
                                                        >
                                                            Post
                                                        </button>
                                                    )}
                                                </div>
                                            </section>
                                            {modalData?.comments?.total > 0 && (
                                                <section>
                                                    {modalData?.comments?.latest.map(
                                                        (comment, index) => (
                                                            <div
                                                                key={index}
                                                                className="flex items-start space-x-1.5 relative"
                                                            >
                                                                <Image
                                                                    className="rounded-xl"
                                                                    src={
                                                                        comment
                                                                            .user
                                                                            .photo_profile
                                                                    }
                                                                    width={40}
                                                                    height={40}
                                                                    layout="fixed"
                                                                />
                                                                <div className="space-y-2.5 w-full text-sm">
                                                                    <div className="relative bg-slate-100 space-y-2 px-2 rounded-t-xl flex-auto">
                                                                        <div className="flex justify-between h-fit">
                                                                            <p className="text-slate-500 flex items-center gap-x-2 text-xs truncate">
                                                                                {
                                                                                    comment
                                                                                        .user
                                                                                        .name
                                                                                }
                                                                                <br className="" />
                                                                                <span className="inline-block w-1.5 h-1.5 rounded-full bg-slate-400 md:hidden lg:inline-block"></span>{" "}
                                                                                {
                                                                                    comment
                                                                                        .user
                                                                                        .position
                                                                                }
                                                                            </p>
                                                                            <DotsHorizontalIcon className="w-6 h-6 text-slate-400 cursor-pointer" />
                                                                        </div>
                                                                        <div>
                                                                            <p className="inline-block max-w-md text-slate-700">
                                                                                {
                                                                                    comment.comment
                                                                                }
                                                                            </p>
                                                                        </div>
                                                                        <div className="flex">
                                                                            <ThumbUpIcon className="absolute right-12 text-white w-4 h-4 bg-blue-700 rounded-full px-1" />
                                                                            <HeartIcon className="absolute right-9 text-white w-4 h-4 bg-pink-500 rounded-full px-1" />
                                                                            <FaRegLaughSquint className="absolute right-6 text-black w-4 h-4 bg-gradient-to-b from-[#FCCA69] to-[#FAA705] rounded-full" />
                                                                            <span className="absolute right-2 rounded-full text-xs">
                                                                                50
                                                                            </span>
                                                                        </div>
                                                                    </div>
                                                                    <div className="flex text-xs space-x-2.5 px-2 !mt-0 !mb-3 bg-slate-100 rounded-b-xl">
                                                                        <span className="">
                                                                            Love
                                                                        </span>
                                                                        <span
                                                                            onClick={() =>
                                                                                setReplyInputFlag(
                                                                                    !replyInputFlag
                                                                                )
                                                                            }
                                                                            className="text-slate-600 cursor-pointer"
                                                                        >
                                                                            Reply
                                                                        </span>
                                                                        <span className="text-slate-600">
                                                                            Share
                                                                        </span>
                                                                    </div>
                                                                    {/* <div className="flex items-start space-x-1.5">
                                                                    <Image
                                                                        src="/avatar.png"
                                                                        width={40}
                                                                        height={40}
                                                                        layout="fixed"
                                                                    />
                                                                    <div className="space-y-2.5 w-full">
                                                                        <div className="relative bg-slate-100 py-2 px-3 rounded-lg flex-auto space-y-2">
                                                                            <div className="flex justify-between h-fit">
                                                                                <p className="text-slate-500 flex items-center gap-x-2">
                                                                                    Username{" "}
                                                                                    <span className="inline-block w-1.5 h-1.5 rounded-full bg-slate-400"></span>{" "}
                                                                                    Vice
                                                                                    President
                                                                                </p>
                                                                                <DotsHorizontalIcon className="w-6 h-6 text-slate-400 cursor-pointer" />
                                                                            </div>
                                                                            <div>
                                                                                <p className="inline-block max-w-md text-slate-700">
                                                                                    Lorem
                                                                                    ipsum
                                                                                    dolor
                                                                                    sit
                                                                                    amet
                                                                                    consectetur
                                                                                    adipisicing
                                                                                    elit.
                                                                                    Vel
                                                                                    rerum
                                                                                    illo
                                                                                    magnam
                                                                                    ratione
                                                                                    quae.
                                                                                    At
                                                                                    vel
                                                                                    ea
                                                                                    totam
                                                                                    rerum
                                                                                    dignissimos
                                                                                    molestiae
                                                                                    reprehenderit
                                                                                    earum
                                                                                    unde
                                                                                    repellendus
                                                                                    consectetur
                                                                                    nulla
                                                                                    saepe
                                                                                    porro
                                                                                    facilis
                                                                                    nostrum
                                                                                    esse
                                                                                    fuga,
                                                                                    est
                                                                                    omnis
                                                                                    magni
                                                                                    explicabo
                                                                                    culpa!
                                                                                    Saepe,
                                                                                    dolores.
                                                                                </p>
                                                                            </div>
                                                                        </div>
                                                                        <div className="flex space-x-2.5 px-3 text-xs">
                                                                            <span className="text-slate-600">
                                                                                Like
                                                                            </span>
                                                                            <span
                                                                                onClick={() =>
                                                                                    setReplyInputFlag(
                                                                                        !replyInputFlag
                                                                                    )
                                                                                }
                                                                                className="text-slate-600 cursor-pointer"
                                                                            >
                                                                                Reply
                                                                            </span>
                                                                            <span className="text-slate-600">
                                                                                Share
                                                                            </span>
                                                                        </div>
                                                                    </div>
                                                                </div> */}
                                                                </div>
                                                            </div>
                                                        )
                                                    )}
                                                </section>
                                            )}
                                        </Dialog.Description>
                                    </div>
                                </Dialog.Panel>
                            </Transition.Child>
                        </div>
                    </div>
                </Dialog>
            </Transition>
        </>
    );
}
