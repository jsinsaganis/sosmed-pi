import ContentLoader from "react-content-loader"

const SkeletonModalPhotos = (props) => (
  <ContentLoader 
    speed={2}
    width={400}
    height={460}
    viewBox="0 0 400 460"
    backgroundColor="#f3f3f3"
    foregroundColor="#ecebeb"
    {...props}
  >
    <circle cx="215" cy="79" r="17" /> 
    <rect x="240" y="66" rx="2" ry="2" width="64" height="12" /> 
    <rect x="203" y="103" rx="2" ry="2" width="168" height="37" /> 
    <rect x="205" y="152" rx="2" ry="2" width="179" height="3" />
  </ContentLoader>
)

export default SkeletonModalPhotos