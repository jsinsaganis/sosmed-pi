import React from "react";
import Comments from "../Feeds/Comments";
import CustomTextArea from "../Feeds/CustomTextArea";
import PostList from "../Feeds/PostList";

function Activity() {
    return (
        <div className="space-y-6">
            <div className="bg-white p-8 rounded-xl">
                <CustomTextArea
                    bordered={true}
                    minRows="2"
                    placeholder="What's on your mind, Username ?"
                />
            </div>
            <PostList />
        </div>
    );
}

export default Activity;
