import Image from "next/image";
import React from "react";

function Photos() {
    return (
        <div className="bg-white gap-4 p-8 grid grid-cols-2">
            <a href="#">
                <div className="outlined rounded-xl overflow-hidden h-52 relative">
                    <Image layout="fill" src="/login-image.png" alt="" />
                </div>
            </a>
            <a href="#">
                <div className="grid grid-cols-2 grid-rows-2 gap-1 h-52 rounded-xl overflow-hidden ">
                    <div className="relative row-span-2">
                        <Image
                            objectFit="cover"
                            layout="fill"
                            src="/sample-1.png"
                        />
                    </div>
                    <div className="relative">
                        <Image
                            layout="fill"
                            src="/sample-2.png"
                            objectFit="cover"
                        />
                    </div>
                    <div className="relative">
                        <Image
                            layout="fill"
                            src="/sample-3.png"
                            objectFit="cover"
                        />
                    </div>
                </div>
            </a>
            <a href="#">
                <div className="grid grid-cols-2 grid-rows-2 gap-1 h-52 rounded-xl overflow-hidden">
                    <div className="relative row-span-2 h-52">
                        <Image
                            layout="fill"
                            src="/sample-4.png"
                            objectFit="cover"
                        />
                    </div>
                    <div className="relative">
                        <Image
                            layout="fill"
                            src="/sample-5.png"
                            objectFit="cover"
                        />
                    </div>
                    <div className="relative">
                        <Image
                            layout="fill"
                            src="/sample-6.png"
                            objectFit="cover"
                        />
                    </div>
                </div>
            </a>
            <a href="#">
                <div className="outlined rounded-xl overflow-hidden h-52 relative">
                    <Image layout="fill" src="/login-image.png" alt="" />
                </div>
            </a>
        </div>
    );
}

export default Photos;
