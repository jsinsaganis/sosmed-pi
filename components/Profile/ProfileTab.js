import { useEffect, useState } from "react";
import { Tab } from "@headlessui/react";
import {
    DesktopComputerIcon,
    DeviceMobileIcon,
    PencilAltIcon,
    PencilIcon,
} from "@heroicons/react/solid";
import Image from "next/image";
import Workspace from "../Workspace";
import Skills from "./Skills";
import Photos from "./Photos";
import Activity from "./Activity";
import axios from "axios";
import { parseCookies } from "nookies";
import useSWR from "swr";
import request from "../../utils/request";

function classNames(...classes) {
    return classes.filter(Boolean).join(" ");
}

export default function ProfileTab() {
    const [bioFlag, setBioFlag] = useState(true);
    const [skillFlag, setSkillFlag] = useState(1);

    const cookies = parseCookies();
    const jwt = { cookies };

    const fetcher =async (url) =>
       await axios
            .get(url, {
                headers: {
                    Authorization: `Bearer ${jwt.cookies.token}`,
                },
            })
            .then((res) => res.data.data);

    const { data } = useSWR(request.getSkill + jwt.cookies.nik, fetcher);

    const { data: about, mutate, isValidating } = useSWR(
        request.getAboutUs + jwt.cookies.nik,
        fetcher
    );
    const [bio, setBio] = useState(!isValidating ? about.about_us_desc: '');

    const handlePostBio = () => {
        axios
            .post(
                request.storeAboutUs,
                {
                    nik: jwt.cookies.nik,
                    desc: bio,
                    hobby: "",
                },
                {
                    headers: {
                        Authorization: `Bearer ${jwt.cookies.token}`,
                    },
                }
            )
            .then(() => {
                setBioFlag(true);
                setBio(bio);
        mutate(request.getAboutUs);

            })
            .catch((err) => console.log(err))
            .finally(() => {
                setBio("");
            });
    };

    const initializeBio = () => {
        setBioFlag(false);
        setBio(about.about_us_desc);
    };


    const [categories, setCategories] = useState({
        About: {
            name: "Kaido The Kid",
            position: "Captain",
            bio: "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Voluptas distinctio enim ipsa? Facilis debitis eligendi quibusdam ipsa eos distinctio unde?",
            skills: [
                {
                    id: 1,
                    skillName: "Ruby on Rails",
                    skilIcon: "avatar.png",
                    exp: "10 Years Experience",
                    disabled: true,
                },
                {
                    id: 2,
                    skillName: "C++",
                    skilIcon: "avatar.png",
                    exp: "10 Years Experience",
                    disabled: true,
                },
                {
                    id: 3,
                    skillName: "C#",
                    skilIcon: "avatar.png",
                    exp: "15 Years Experience",
                    disabled: true,
                },
            ],
        },
        //   {
        //     id: 1,
        //     title: 'Does drinking coffee make you smarter?',
        //     date: '5h ago',
        //     commentCount: 5,
        //     shareCount: 2,
        //   },
        //   {
        //     id: 2,
        //     title: "So you've bought coffee... now what?",
        //     date: '2h ago',
        //     commentCount: 3,
        //     shareCount: 2,
        //   },
        // ],
        Photos: [
            {
                id: 1,
                title: "Is tech making coffee better or worse?",
                date: "Jan 7",
                commentCount: 29,
                shareCount: 16,
            },
            {
                id: 2,
                title: "The most innovative things happening in coffee",
                date: "Mar 19",
                commentCount: 24,
                shareCount: 12,
            },
        ],
        Reels: [
            {
                id: 1,
                title: "Ask Me Anything: 10 answers to your questions about coffee",
                date: "2d ago",
                commentCount: 9,
                shareCount: 5,
            },
            {
                id: 2,
                title: "The worst advice we've ever heard about coffee",
                date: "4d ago",
                commentCount: 1,
                shareCount: 2,
            },
        ],
        Activity: [
            {
                id: 1,
                title: "Ask Me Anything: 10 answers to your questions about coffee",
                date: "2d ago",
                commentCount: 9,
                shareCount: 5,
            },
            {
                id: 2,
                title: "The worst advice we've ever heard about coffee",
                date: "4d ago",
                commentCount: 1,
                shareCount: 2,
            },
        ],
    });

    // const handleEditFlag = (id) => {
    //     let idx = categories.About.skills.findIndex((obj) => obj.id == id);
    //     let newCategories = { ...categories };
    //     newCategories.About.skills[idx].disabled = false;
    //     setCategories(newCategories);
    // };

    // const handleCancel = (id) => {
    //     let idx = categories.About.skills.findIndex((obj) => obj.id == id);
    //     let newCategories = { ...categories };
    //     newCategories.About.skills[idx].disabled = true;
    //     setCategories(newCategories);
    // };

    // const handleSave = (id) => {
    //     let idx = categories.About.skills.findIndex((obj) => obj.id == id);
    //     let newCategories = { ...categories };
    //     newCategories.About.skills[idx].disabled = true;
    //     setCategories(newCategories);
    // };

    return (
        <div className="w-full ">
            <Tab.Group>
                <Tab.List
                    as="div"
                    className="flex space-x-1 rounded-xl bg-white px-2 pt-1 outlined"
                >
                    {Object.keys(categories).map((category) => (
                        <Tab
                            key={category}
                            className={({ selected }) =>
                                classNames(
                                    "w-32 py-2.5 text-sm font-medium duration-300 ease-out ",
                                    "hover:text-black outline-none min-w-max ",
                                    selected
                                        ? "border-b-2 border-b-[#2E55A3] text-[#002DF5] hover:text-[#002DF5]"
                                        : "text-[#818598]"
                                )
                            }
                        >
                            {category}
                        </Tab>
                    ))}
                </Tab.List>
                <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 space-x-0 lg:space-x-6 md:mt-4 ">
                    <Tab.Panels className="mt-2 md:col-span-2 col-span-full">
                        <Tab.Panel className={"space-y-6"}>
                            <div className="bg-white rounded-xl p-7 space-y-3 outlined">
                                <h2 className="uppercase text-gray-400">
                                    general information
                                </h2>
                                <div>
                                    <span className="capitalize text-sm font-bold">
                                        Name
                                    </span>
                                    <p className="text-slate-600">
                                        {categories.About.name}
                                    </p>
                                </div>
                                <div>
                                    <span className="capitalize text-sm font-bold">
                                        position
                                    </span>
                                    <p className="text-slate-600">Captain</p>
                                </div>
                                <div className="w-full">
                                    <label className="capitalize text-sm font-bold">
                                        bio
                                    </label>
                                    <div className="flex items-end space-x-3 justify-between">
                                        {/* <div> */}
                                        <p
                                            className={`${
                                                !bioFlag && "hidden"
                                            }`}
                                        >
                                            {!isValidating && about.about_us_desc}
                                        </p>
                                        <textarea
                                            disabled={bioFlag}
                                            className={`p-4 border-[1px] border-gray-600 rounded-xl mt-3 w-full block ${
                                                bioFlag && "hidden"
                                            }`}
                                            spellCheck="false"
                                            cols="20"
                                            rows="3"
                                            value={bio}
                                            onChange={(e) =>
                                                setBio(e.target.value)
                                            }
                                        />
                                        {/* </div> */}
                                        <PencilIcon
                                            onClick={initializeBio}
                                            className={`w-4 h-4 text-gray-700 hover:text-gray-900 cursor-pointer border-b-2 border-gray-700 ${
                                                !bioFlag && "hidden"
                                            }`}
                                        />
                                    </div>
                                    {!bioFlag && (
                                        <div className="text-right space-x-2 mt-2">
                                            <button
                                                onClick={() => setBioFlag(true)}
                                                className="text-[#2E55A3] bg-[#E2EEFF] py-2 px-4 rounded-xl"
                                            >
                                                Cancel
                                            </button>
                                            <button
                                                onClick={handlePostBio}
                                                className="text-white bg-[#2E55A3] py-2 px-4 rounded-xl"
                                            >
                                                Save
                                            </button>
                                        </div>
                                    )}
                                </div>
                            </div>
                            <div className="bg-white rounded-xl p-8 space-y-0 w-full lg:inline-block outlined">
                                <h2 className="uppercase text-slate-600">
                                    skills
                                </h2>
                                {data &&
                                    data.map((skill) => (
                                        <div
                                            key={skill.id}
                                            className="flex items-center w-full py-4 space-x-2"
                                        >
                                            <div
                                                className={`w-10 h-10 relative`}
                                            >
                                                <Image
                                                    src="/avatar.png"
                                                    layout="fill"
                                                />
                                            </div>
                                            <div>
                                                <p>{skill.name}</p>
                                                {skill.skill.map((subSkill) => (
                                                    <span
                                                        className="text-slate-500 text-xs"
                                                        key={subSkill.id}
                                                    >
                                                        #{subSkill.name}{" "}
                                                    </span>
                                                ))}
                                            </div>
                                        </div>
                                    ))}
                                {/* {categories.About.skills.map((skill) => (
                                    <div
                                        key={skill.id}
                                        className="flex items-center w-full space-y-2 py-4 space-x-2"
                                    >
                                        <div
                                            className={`w-10 h-10 relative ${
                                                !skill.disabled && "hidden"
                                            }`}
                                        >
                                            <Image
                                                src="/avatar.png"
                                                layout="fill"
                                            />
                                        </div>
                                        <div
                                            className={`!mt-0 flex items-center justify-between w-full ${
                                                !skill.disabled &&
                                                "!ml-0 flex-col"
                                            }`}
                                        >
                                            <div className={`space-y-2 w-full`}>
                                                <label
                                                    className={`capitalize text-sm font-bold ${
                                                        skill.disabled &&
                                                        "hidden"
                                                    }`}
                                                >
                                                    Name
                                                </label>
                                                <input
                                                    disabled={skill.disabled}
                                                    className={`focus:outline-none w-full disabled:bg-white text-sm font-semibold text-slate-600 !mt-0 ${
                                                        !skill.disabled &&
                                                        "p-2 border-[1px] border-gray-600 rounded-lg !mt-2 !text-md "
                                                    }`}
                                                    spellCheck="false"
                                                    defaultValue={
                                                        skill.skillName
                                                    }
                                                />
                                                <label
                                                    className={`capitalize text-sm font-bold ${
                                                        skill.disabled &&
                                                        "hidden"
                                                    }`}
                                                >
                                                    Years of Experience
                                                </label>
                                                <input
                                                    disabled={skill.disabled}
                                                    className={`focus:outline-none w-full disabled:bg-white text-xs text-slate-600 !mt-0 ${
                                                        !skill.disabled &&
                                                        "p-2 border-[1px] border-gray-600 rounded-lg !mt-2 !text-md "
                                                    }`}
                                                    spellCheck="false"
                                                    defaultValue={skill.exp}
                                                />
                                            </div>
                                            <PencilIcon
                                                onClick={() =>
                                                    handleEditFlag(skill.id)
                                                }
                                                className={`w-4 h-4 text-gray-700 hover:text-gray-900 cursor-pointer border-b-2 border-gray-700 ${
                                                    !skill.disabled && "hidden"
                                                }`}
                                            />
                                            {!skill.disabled && (
                                                <div className="w-full text-right mt-3 space-x-2">
                                                    <button
                                                        onClick={() =>
                                                            handleCancel(
                                                                skill.id
                                                            )
                                                        }
                                                        className="text-[#2E55A3] bg-[#E2EEFF] py-2 px-4 rounded-xl"
                                                    >
                                                        Cancel
                                                    </button>
                                                    <button
                                                        onClick={() =>
                                                            handleSave(skill.id)
                                                        }
                                                        className="text-white bg-[#2E55A3] py-2 px-4 rounded-xl"
                                                    >
                                                        Save
                                                    </button>
                                                </div>
                                            )}
                                        </div>
                                    </div>
                                ))}
                                <button className="w-full bg-[#E2EEFF] py-2 text-[#2E55A3] font-bold rounded-md">
                                    Add More Skills
                                </button> */}
                            </div>
                        </Tab.Panel>
                        <Tab.Panel>
                            <Photos />
                        </Tab.Panel>
                        <Tab.Panel>
                            <Photos />
                        </Tab.Panel>
                        <Tab.Panel>
                            <Activity />
                        </Tab.Panel>
                    </Tab.Panels>
                    <div className="space-y-4 mt-2 ml-0">
                        <Workspace />
                        <Skills />
                    </div>
                </div>
            </Tab.Group>
        </div>
    );
}
