import { DesktopComputerIcon, DeviceMobileIcon } from "@heroicons/react/solid";
import Image from "next/image";
import React from "react";
import Modal from "../Modal";
import axios from "axios";
import { parseCookies } from "nookies";
import useSWR from "swr";
import request from "../../utils/request";

function Skills() {
    const cookies = parseCookies();
    const jwt = { cookies };

    const fetcher = (url) =>
        axios
            .get(url, {
                headers: {
                    Authorization: `Bearer ${jwt.cookies.token}`,
                },
            })
            .then((res) => res.data.data);

    const { data } = useSWR(request.getSkill + jwt.cookies.nik, fetcher);
    return (
        <div className="outlined p-4 h-fit rounded-xl text-[#ABAEBA] bg-white space-y-5">
            <h2 className="uppercase text-sm">your skills</h2>
            {data &&
                data.map((skill) => (
                    <div key={skill.id} className="flex items-center space-x-3">
                        <div className="w-12 h-12 relative flex-shrink-0">
                            <Image src="/avatar.png" layout="fill" />
                        </div>
                        <div className="flex flex-col gap-y-1">
                            <h2 className="text-gray-900 text-sm">
                                {skill.name}
                            </h2>
                            <div className="inline items-center space-x-1">
                                {/* <DesktopComputerIcon className="w-5 h-5" /> */}
                                { skill.skill && skill.skill.map(subskill => (
                                    <span key={subskill.id} className="capitalize text-xs">
                                       #{subskill.name}
                                    </span>
                                ))}
                            </div>
                        </div>
                    </div>
                ))}

            {/* <div className="flex items-center space-x-3">
                <div className="w-12 h-12 relative">
                    <Image src="/avatar.png" layout="fill" />
                </div>
                <div className="flex flex-col gap-y-1">
                    <h2 className="text-gray-900 text-sm">C#</h2>
                    <div className="flex items-center space-x-1">
                        <DeviceMobileIcon className="w-5 h-5" />
                        <span className="capitalize text-xs">android</span>
                    </div>
                </div>
            </div>

            <div className="flex items-center space-x-3">
                <div className="w-12 h-12 relative">
                    <Image src="/avatar.png" layout="fill" />
                </div>
                <div className="flex flex-col gap-y-1">
                    <h2 className="text-gray-900 text-sm">C++</h2>
                    <div className="flex items-center space-x-1">
                        <DesktopComputerIcon className="w-5 h-5" />
                        <span className="capitalize text-xs">
                            systik, helpdesk TIK
                        </span>
                    </div>
                </div>
            </div> */}
            {/* <button className="w-full block text-white bg-[#2E55A3] py-2 rounded-xl">
        Edit Skills
      </button> */}
            <Modal />
        </div>
    );
}

export default Skills;
