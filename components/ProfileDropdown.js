/* This example requires Tailwind CSS v2.0+ */
import { Fragment } from "react";
import { Menu, Transition } from "@headlessui/react";
import {
    ChevronDownIcon,
    CogIcon,
    DotsHorizontalIcon,
    QuestionMarkCircleIcon,
    UserIcon,
} from "@heroicons/react/solid";
import Link from "next/link";

function classNames(...classes) {
    return classes.filter(Boolean).join(" ");
}

export default function ProfileDropdown({ username, email, dropdownList }) {
    return (
        <Menu
            as="div"
            className="hidden sm:inline-block relative text-left z-50"
        >
            <div className="flex items-center justify-center gap-x-2">
                {username && <img src="/avatar.png" alt="avatar" />}
                <div>
                    <p>{username}</p>
                    <p>{email}</p>
                </div>
                <Menu.Button className="">
                    {username ? (
                        <ChevronDownIcon className="h-4 w-4 md:h-6 md:w-6 cursor-pointer text-slate-400 hover:text-slate-700 transition duration-200 ease-out" />
                    ) : (
                        <DotsHorizontalIcon className="h-4 w-4 md:h-6 md:w-6 cursor-pointer text-slate-400 hover:text-slate-700 transition duration-200 ease-out" />
                    )}
                </Menu.Button>
            </div>

            <Transition
                as={Fragment}
                enter="transition ease-out duration-100"
                enterFrom="transform opacity-0 scale-95"
                enterTo="transform opacity-100 scale-100"
                leave="transition ease-in duration-75"
                leaveFrom="transform opacity-100 scale-100"
                leaveTo="transform opacity-0 scale-95"
            >
                <Menu.Items className="origin-top-right absolute right-0 mt-2 w-60 rounded-md shadow-xl bg-white focus:outline-none">
                    <div className="pl-6 px-2 py-4 space-y-2">
                        <Menu.Item className="flex items-center gap-x-3">
                            {({ active }) => (
                                <Link href="/profile">
                                    <a
                                        className={classNames(
                                            active
                                                ? "text-gray-900"
                                                : "text-slate-600",
                                            "flex items-center gap-x-2.5 text-sm hover:text-black"
                                        )}
                                    >
                                        <UserIcon
                                            className={`w-4 h-4 ${
                                                (active
                                                    ? "text-red-900"
                                                    : "text-slate-600",
                                                "hover:text-black")
                                            }`}
                                        />{" "}
                                        Your Profile
                                    </a>
                                </Link>
                            )}
                        </Menu.Item>
                        <Menu.Item className="flex items-center gap-x-3">
                            {({ active }) => (
                                <Link href="/" className="">
                                    <a
                                        href="#"
                                        className={classNames(
                                            active
                                                ? "bg-gray-100 text-gray-900"
                                                : "text-gray-700",
                                            "flex items-center gap-x-2.5 text-sm hover:text-black"
                                        )}
                                    >
                                        <CogIcon
                                            className={`w-4 h-4 ${
                                                (active
                                                    ? "bg-gray-100 text-gray-900"
                                                    : "text-gray-700",
                                                "hover:text-black")
                                            }`}
                                        />{" "}
                                        Setting
                                    </a>
                                </Link>
                            )}
                        </Menu.Item>
                        <Menu.Item className="flex items-center gap-x-3">
                            {({ active }) => (
                                <Link href="/" className="">
                                    <a
                                        href="#"
                                        className={classNames(
                                            active
                                                ? "bg-gray-100 text-gray-900"
                                                : "text-gray-700",
                                            "flex items-center gap-x-2.5 text-sm hover:text-black"
                                        )}
                                    >
                                        <QuestionMarkCircleIcon
                                            className={`w-4 h-4 ${
                                                (active
                                                    ? "bg-gray-100 text-gray-900"
                                                    : "text-gray-700",
                                                "hover:text-black")
                                            }`}
                                        />{" "}
                                        Help & Support
                                    </a>
                                </Link>
                            )}
                        </Menu.Item>
                    </div>
                </Menu.Items>
            </Transition>
        </Menu>
    );
}
