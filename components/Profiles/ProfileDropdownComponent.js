import { Menu, Transition } from "@headlessui/react";
import { forwardRef, Fragment, useEffect, useRef, useState } from "react";
import {
    ChevronDownIcon,
    UserIcon,
    CogIcon,
    QuestionMarkCircleIcon,
    LogoutIcon,
} from "@heroicons/react/solid";
import Link from "next/link";
import { useRouter } from "next/router";
import { userData } from "../../atoms/atomState";
import { useRecoilState, useRecoilValue } from "recoil";
import { useAuth } from "../../hooks/auth";
import { parseCookies, setCookie, destroyCookie } from "nookies";
import useSWR from "swr";

const MyLink = forwardRef((props, ref) => {
    let { href, children, ...rest } = props;
    return (
        <Link href={href}>
            <a ref={ref} {...rest}>
                {children}
            </a>
        </Link>
    );
});
MyLink.displayName = "MyLink"

export default function ProfileDropdownComponent({data}) {
    const router = useRouter();
    const { getProfile } = useAuth();
    const profile = data ? data.data.data : {}

    

    return (
        <Menu as="div" className="dropdown__menu">
            <div>
                <Menu.Button className="dropdown__menu-btn">
                    <div className="hidden md:flex">
                        <div className="dropdown__menu-profile">
                            <img
                                className="dropdown__menu-profile-img"
                                layout="fill"
                                src={profile?.photo_profile}
                            />
                            <div className="dropdown__menu-profile-data">
                                <span className="truncate md:leading-4 md:text-[.78rem] lg:text-[.82rem]">
                                    {profile?.name}
                                </span>
                                <span className="truncate md:leading-4 md:text-[.7rem] lg:text-xs text-slate-500 font-normal">
                                    {profile?.email}
                                </span>
                            </div>
                        </div>
                        <ChevronDownIcon
                            className="dropdown__arrow"
                            aria-hidden="true"
                        />
                    </div>

                    {/* Mobile Version */}
                    <div className="w-full text-[#ABAEBA] flex flex-col md:hidden items-center gap-1 hover:bg-white hover:text-[#2E55A3] duration-200 py-1">
                        <UserIcon className="w-[1.2rem]" />
                        <span className="font-medium text-[.65rem]">Saya</span>
                    </div>
                    {/* Mobile Version */}
                </Menu.Button>
            </div>
            <Transition
                as={Fragment}
                enter="transition ease-out duration-100"
                enterFrom="transform opacity-0 scale-95"
                enterTo="transform opacity-100 scale-100"
                leave="transition ease-in duration-75"
                leaveFrom="transform opacity-100 scale-100"
                leaveTo="transform opacity-0 scale-95"
            >
                <Menu.Items className="dropdown__menu-items -right-4 sm:-right-5 bottom-0 mb-[4rem] md:right-0 md:mt-2 md:mb-0 md:bottom-auto shadow-lg">
                    <div className="dropdown__menu-items-container">
                        <Menu.Item>
                            <div className="dropdown__menu-profile mb-3">
                                <img
                                    className="dropdown__menu-profile-img"
                                    layout="fill"
                                    src={profile.photo_profile}
                                />
                                <div className="dropdown__menu-profile-data">
                                    <span className="leading-4 text-[.78rem] lg:text-[.82rem] truncate">
                                        {profile.name}
                                    </span>
                                    <span className="leading-4 text-[.7rem] lg:text-xs text-slate-500 font-normal">
                                        {profile.email}
                                    </span>
                                </div>
                            </div>
                        </Menu.Item>

                        <Menu.Item>
                            <MyLink href="/profile">
                                <div
                                    className={`${
                                        router.pathname == "/profile"
                                            ? "bg-[#E2EEFF] text-[#2E55A3]"
                                            : "text-gray-900"
                                    } group dropdown__menu-item`}
                                >
                                    <UserIcon
                                        className="dropdown__menu-item-icon"
                                        aria-hidden="true"
                                    />
                                    Your Profile
                                </div>
                            </MyLink>
                        </Menu.Item>

                        <Menu.Item>
                            <MyLink href="/setting">
                                <div
                                    className={`${
                                        router.pathname == "/setting"
                                            ? "bg-[#E2EEFF] text-[#2E55A3]"
                                            : "text-gray-900"
                                    } group dropdown__menu-item`}
                                >
                                    <CogIcon
                                        className="dropdown__menu-item-icon"
                                        aria-hidden="true"
                                    />
                                    Setting
                                </div>
                            </MyLink>
                        </Menu.Item>

                        <Menu.Item>
                            <MyLink href="/help">
                                <div
                                    className={`${
                                        router.pathname == "/help"
                                            ? "bg-[#E2EEFF] text-[#2E55A3]"
                                            : "text-gray-900"
                                    } group dropdown__menu-item`}
                                >
                                    <QuestionMarkCircleIcon
                                        className="dropdown__menu-item-icon"
                                        aria-hidden="true"
                                    />
                                    Help & Support
                                </div>
                            </MyLink>
                        </Menu.Item>

                        {/* Mobile Version */}
                        <Menu.Item
                            as={`div`}
                            className="border-t mt-1 pt-1 md:hidden"
                        >
                            <MyLink href="/auth">
                                <div
                                    className={`group text-red-600 dropdown__menu-item hover:bg-red-100 hover:text-red-600 duration-300`}
                                >
                                    <LogoutIcon
                                        className="dropdown__menu-item-icon"
                                        aria-hidden="true"
                                    />
                                    Logout
                                </div>
                            </MyLink>
                        </Menu.Item>
                        {/* Mobile Version */}
                    </div>
                </Menu.Items>
            </Transition>
        </Menu>
    );
}
