import SidebarLink from "./SidebarLink";
import { AiFillAppstore } from "react-icons/ai";
import { MdPhoto, MdCast, MdViewCompact } from "react-icons/md";
import { LogoutIcon } from "@heroicons/react/outline";
import Link from "next/link";

function Sidebar() {
    return (
        <div className="min-h-screen relative outlined hidden sm:flex flex-col items-center justify-self-start xl:items-start xl:w-[267px] p-2 h-full rounded-xl text-[#ABAEBA] bg-white xl:ml-14">
            <div className="hidden xl:flex items-center justify-center w-60 h-60 p-0 mx-auto">
                <img src="/greetings.png" width={330} height={330} />
            </div>
            <div className="space-y-1 mb-2.5 xl:ml-4">
                <Link href="/">
                    <a>
                        <SidebarLink
                            text="News Feed"
                            Icon={MdViewCompact}
                            active
                        />
                    </a>
                </Link>
                <Link href="/test">
                    <a>
                        <SidebarLink text="Apps" Icon={AiFillAppstore} />
                    </a>
                </Link>
                <Link href="/photos">
                    <a>
                        <SidebarLink text="Photos" Icon={MdPhoto} />
                    </a>
                </Link>
                <Link href="/">
                    <a>
                        <SidebarLink text="Blogs" Icon={MdCast} />
                    </a>
                </Link>
            </div>
            <div className="absolute bottom-0 flex gap-x-1 xl:ml-4 text-red-500 hoverAnimation">
                <LogoutIcon className="h-7" /> Logout
            </div>
        </div>
    );
}

export default Sidebar;
