function SidebarLink({ Icon, text, active }) {
    return (
        <div
            className={`text-[#d9d9d9] flex items-center justify-center xl:justify-start text-xl space-x-3 hoverAnimation ${
                active && "font-bold !text-[#2E55A3]"
            }`}
        >
            <Icon
                className={`h-7 w-7 transition duration-200 ease-out ${
                    active ? "text-[#2E55A3]" : "text-[#ABAEBA]"
                } `}
            />
            <span className="hidden xl:inline text-lg">{text}</span>
        </div>
    );
}

export default SidebarLink;
