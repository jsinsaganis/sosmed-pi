import { DesktopComputerIcon, DeviceMobileIcon } from "@heroicons/react/solid";
import Image from "next/image";
import Link from "next/link";

function Workspace() {
    return (
        <div className="mt-5 lg:mt-0 ">
            <div className="sticky w-full md:top-[6rem] lg:top-[7.2rem] flex flex-col gap-3 md:gap-5">
                <div className="border border-slate-200 rounded-xl bg-white">
                    <h3 className="text-xs font-medium text-[#818598] p-3 lg:p-4">
                        MY WORKSPACES
                    </h3>
                    <div className="flex flex-col pb-2">
                        <Link href={`/`}>
                            <div className="flex flex-row items-center gap-3 cursor-pointer hover:bg-[#E2EEFF] px-3 lg:px-4 py-1 lg:py-2 duration-200">
                                <img
                                    src={"images/apps/apps-1.png"}
                                    className="w-8 h-8 lg:w-12 lg:h-12 rounded-md object-cover"
                                />
                                <div className="flex flex-col gap-1">
                                    <p className="text-[.75rem] leading-3 lg:leading-4 lg:text-[.7rem] font-semibold line-clamp-2">
                                        DOF (Digital Office)
                                    </p>
                                    <div className="flex flex-row gap-1 text-[#818598] items-center">
                                        <DesktopComputerIcon className="w-[.7rem] sm:w-[.8rem] lg:w-[.9rem]" />
                                        <p className="text-[.7rem] lg:text-[.6rem]">
                                            Web Apps
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </Link>

                        <Link href={`/`}>
                            <div className="flex flex-row items-center gap-3 cursor-pointer hover:bg-[#E2EEFF] px-3 lg:px-4 py-1 lg:py-2 duration-200">
                                <img
                                    src={"images/apps/apps-2.png"}
                                    className="w-8 h-8 lg:w-12 lg:h-12 rounded-md object-cover"
                                />
                                <div className="flex flex-col gap-1">
                                    <p className="text-[.75rem] leading-3 lg:leading-4 lg:text-[.7rem] font-semibold line-clamp-2">
                                        DEMPLON
                                    </p>
                                    <div className="flex flex-row gap-1 text-[#818598] items-center">
                                        <DeviceMobileIcon className="w-[.7rem] sm:w-[.8rem] lg:w-[.9rem]" />
                                        <p className="text-[.7rem] lg:text-[.6rem]">
                                            Android
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </Link>

                        <Link href={`/`}>
                            <div className="flex flex-row items-center gap-3 cursor-pointer hover:bg-[#E2EEFF] px-3 lg:px-4 py-1 lg:py-2 duration-200">
                                <img
                                    src={"images/apps/apps-3.png"}
                                    className="w-8 h-8 lg:w-12 lg:h-12 rounded-md object-cover"
                                />
                                <div className="flex flex-col gap-1">
                                    <p className="text-[.75rem] leading-3 lg:leading-4 lg:text-[.7rem] font-semibold line-clamp-2">
                                        SYSTIK, Helpdesk TIK
                                    </p>
                                    <div className="flex flex-row gap-1 text-[#818598] items-center">
                                        <DesktopComputerIcon className="w-[.7rem] sm:w-[.8rem] lg:w-[.9rem]" />
                                        <p className="text-[.7rem] lg:text-[.6rem]">
                                            Web Apps
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </Link>

                        <div className="px-3 lg:px-4 py-1 lg:py-2 mt-3">
                            <button className="button py-2 text-sm font-medium text-white">
                                Edit Workspaces
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Workspace;
