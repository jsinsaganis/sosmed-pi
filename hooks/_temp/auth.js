import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import useSWR from "swr";
import api from "../lib/axios";
import { parseCookies, setCookie, destroyCookie } from 'nookies'

export const useAuth = ({ middleware } = {}) => {
    const router = useRouter();

    //IsLoading
    const [isLoading, setIsLoading] = useState(true);

    //User
    const { data: user, error, mutate } = useSWR("/api/v1/user",
        () => api()
            .get("/api/v1/user")
            .then(response => response.data.data)
            .catch(error => {
                if (error.response.status != 409)
                    throw error
            }),
    )

    //CSRF
    const csrf = () => api().get("/sanctum/csrf-cookie");

    //Login
    const login = async ({ setErrors, ...props }) => {
        setErrors([]);

        await csrf;

        axios
            .post("/login", props)
            .then(() => mutate() && router.push(""))
            .catch((error) => {
                if (error.response.status != 422) throw error;

                setErrors(Object.values(error.response.data.errors).flat());
            });
    };

    //Logout
    const logout = async () => {
        await api().post("/logout");
        
        mutate(null);
        
        // destroyCookie(ctx, 'token');
        router.push("/auth")
    }

    useEffect(() => {
        if (user || error) setIsLoading(false);

        if ((middleware == "guest" && user) || (middleware == "auth" && error))
            router.push("/login");
    });

    return {
        user,
        csrf,
        isLoading,
        login,
        logout,
    };
};
