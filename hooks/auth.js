import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import api from "../lib/axios";
import { parseCookies, setCookie, destroyCookie } from "nookies";
import nookies from 'nookies'
import Axios from "axios";
import { userData } from "../atoms/atomState";
import { useRecoilState } from "recoil";
import { mutate } from "swr";
import request from "../utils/request";

import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

export const useAuth = () => {
    const router = useRouter();
    const [user, setUser] = useRecoilState(userData);
    //IsLoading
    const [isLoading, setIsLoading] = useState(false);
    const [error, setError] = useState('')
    const signIn = async (payload) => {
        setIsLoading(true);
        await Axios.post(
            "https://api-pismart-dev.pupuk-indonesia.com/oauth_api/user/login",
            payload
        )
            .then((res) => {
                router.replace("/");
                // document.cookie = `token=${res.data.token.access_token}; path=/`;
                if(res.data.token.access_token){
                    nookies.set(null, 'token', res.data.token.access_token)
                    nookies.set(null, 'nik', res.data.nik)
                }
                
                setUser(res.data);
                getProfile(res.data.nik, res.data.token.access_token);
            })
            .catch((err) => toast.error(err.response.data.message, {
                pauseOnHover: false,
                position: "bottom-right",
                autoClose: 3000,
                theme: "colored",
                options: "5000 slide 1 0"
            })).finally(() => setIsLoading(false))
            
    };

    const getProfile = async (nik, token) => {
        await Axios.get(
            `https://api-pismart-dev.pupuk-indonesia.com/oauth_api/api/mobile/showProfile/${nik}`,
            {
                headers: {
                    Authorization: `Bearer ${token}`,
                },
            }
        )
            .then((res) => {
                setUser(res.data.data);
            })
            .catch((err) => console.log(err));
    };

    useEffect(() => {
        const cookies = parseCookies();
        const jwt = { cookies }; 

        if(!jwt.cookies.token){
            router.push('/auth')
        }
    },[]);

    //Logout
    const logout = async () => {
        const cookies = parseCookies();
        const jwt = { cookies }; 
        destroyCookie(null, 'token')
        // mutate(`https://api-pismart-dev.pupuk-indonesia.com/oauth_api/api/mobile/showProfile/${jwt.cookies.nik}`)
        router.push('/auth')
    };

    return {
        isLoading,
        error,
        logout,
        signIn,
        getProfile
    };
};
