/** @type {import('next').NextConfig} */

const nextConfig = {
    reactStrictMode: true,
    swcMinify: true,
    images: {
        domains: ["bit.ly", "via.placeholder.com","www.google.com", "pismart-dev.pupuk-indonesia.com", "t3.ftcdn.net", "storage.googleapis.com", "silk.petrokimia-gresik.com", "www.nicepng.com"],
        formats: ["image/webp"]
    },
};

module.exports = nextConfig;
