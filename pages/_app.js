import NextNProgress from "nextjs-progressbar";
import Layout from "../components/Layout";
import "../styles/globals.css";
import React from "react";
import {
    RecoilRoot,
    atom,
    selector,
    useRecoilState,
    useRecoilValue,
} from "recoil";
import { SWRConfig } from "swr";
import axios from "axios";
import { parseCookies } from "nookies";

import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

function MyApp({ Component, pageProps }) {
    const cookies = parseCookies();
    const jwt = { cookies };
    
    return (
        <RecoilRoot>
            {/* <SWRConfig value={{fetcher: (url) => axios.post(url).then(r => r.data)}}></SWRConfig> */}
            <Layout>
                <NextNProgress color="#2E55A3" />
                <Component {...pageProps} />
            </Layout>
            <ToastContainer 
                pauseOnFocusLoss={false}
            />
        </RecoilRoot>
        
    );
}

export default MyApp;
