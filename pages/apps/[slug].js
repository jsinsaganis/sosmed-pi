import axios from "axios";
import { parseCookies } from "nookies";
import AppsDetailComponent from "../../components/Apps/AppsDetailComponent";
import HeadComponent from "../../components/Layouts/HeadComponent";
import request from "../../utils/request";

export default function apps({detail}) {
    return (
        <>
            <HeadComponent title={`Apps Detail`} />
            <AppsDetailComponent data={detail}/>
        </>
    );
}

export async function getServerSideProps(context) {
    const token = `Bearer ${parseCookies(context, 'token').token}`

    const detail = await axios.get(`${request.getDetailApp + context.params.slug}`, {
        headers: {
            'Authorization': token
        }
    })

    const data = detail.data.data

    return {
        props: {
            detail: data
        }
    }
}
