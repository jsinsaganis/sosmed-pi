import axios from "axios";
import { parseCookies } from "nookies";
import useSWR from "swr";
import AppsComponent from "../../components/Apps/AppsComponent";
import HeadComponent from "../../components/Layouts/HeadComponent";
import request from "../../utils/request";

export default function Apps({ categories}) {
    const cookies = parseCookies();
    const jwt = { cookies };
    const allCategory = {
        name: 'All Category'
    }

    const fetcher = (url) =>
        axios
            .get(url, {
                headers: {
                    Authorization: `Bearer ${jwt.cookies.token}`,
                },
            })
            .then((res) => res.data.data);

    const { data: apps } = useSWR(request.filterApps, fetcher, {
        revalidateIfStale: false,
        revalidateOnFocus: false
    })
    
    return (
        <>
            <HeadComponent title={`Apps`} />
            {apps && <AppsComponent apps={apps} categories={[allCategory, ...categories]}/>}
        </>
    );
}

export async function getServerSideProps (context) {
    const categoriesRes = await axios.get(`${request.getCategoryApps}`, {
            headers: {
                'Authorization': `Bearer ${parseCookies(context, 'token').token}`
            }
        })

    const categories = categoriesRes.data.data

    return {
        props: {
            categories

        }
    }
}