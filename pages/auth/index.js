import Image from "next/image";
// import {
//     UserIcon,
//     LockClosedIcon,
//     EyeIcon,
//     ExclamationIcon
// } from "@heroicons/react/solid";
import HeadComponent from "../../components/Layouts/HeadComponent";
import { useAuth } from "../../hooks/auth";
import { useEffect, useState } from "react";
import { useRouter } from "next/router";
import {
    UserIcon,
    LockClosedIcon,
    EyeIcon,
    ExclamationIcon,
    EyeOffIcon,
} from "@heroicons/react/solid";
import { useForm } from "react-hook-form";
import { parseCookies, setCookie, destroyCookie } from "nookies";
import { nookies } from "nookies";

import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const Login = () => {
    const router = useRouter();

    const [changePassword, setChangePassword] = useState(true);
    const changeIcon = changePassword === true ? false : true;
    //States
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");
    // const [errors, setErrors] = useState([])
    const [status, setStatus] = useState(null);
    const { signIn, signUp, isLoading, error } = useAuth();
    const [login, setLogin] = useState(false);
    const cookies = parseCookies();
    const jwt = { cookies };

    const {
        register,
        handleSubmit,
        watch,
        formState: { errors },
    } = useForm();

    const onSubmit = async ({ username, password }) => {
        if (login) {
            await signIn({ username, password });
        } else {
            await signUp(username, password);
        }
    };

    const notify = () => toast("Wow so easy!");

    useEffect(() => {
        if (jwt.cookies.token) {
            router.push("/");
        }
    }, []);

    return (
        <>
            <HeadComponent title={`Login`} />
            <div className="login__container font-inter">
                <div className="login__images-container">
                    <div className="login__images">
                        <div className="login__img">
                            <Image
                                layout="responsive"
                                src="/images/auth/login-image.png"
                                alt="PT Pupuk Indonesia Holding Company"
                                width={200}
                                height={200}
                            />
                        </div>
                    </div>
                </div>

                <div className="login__forms">
                    <div className="login__forms-text">
                        <h3 className="login__form-title">
                            Halo, &#128075; <br />
                            Selamat Datang!
                        </h3>
                        <p className="login__form-subtitle">
                            Silahkan mengisi username dan password di bawah ini!
                        </p>
                    </div>

                    <div className="login__form">
                        <form onSubmit={handleSubmit(onSubmit)}>
                            <div className="input__flex">
                                <label
                                    htmlFor="username"
                                    className="input__groups-label"
                                >
                                    Username
                                </label>
                                <div className="input__groups">
                                    <div className="input__groups-icon">
                                        <UserIcon className="input__icon" />
                                    </div>
                                    <input
                                        id="username"
                                        type="text"
                                        className={`input__group ${
                                            errors.username &&
                                            "border-b-2 border-red-500"
                                        }`} //Add "invalid" class for error
                                        onChange={(e) =>
                                            setUsername(e.target.value)
                                        }
                                        autoFocus
                                        autoComplete="off"
                                        placeholder="Ketikan username"
                                        {...register("username", {
                                            required: true,
                                        })}
                                    />
                                </div>
                                {/* Invalid Input*/}
                                {/* <div className="invalid-feedback">
                                    <ExclamationIcon className={`w-[12px]`}></ExclamationIcon>
                                    <p>Harap isi bidang ini.</p>
                                </div> */}
                                {errors.username && (
                                    <div className="invalid-feedback">
                                        <ExclamationIcon
                                            className={`w-[12px]`}
                                        ></ExclamationIcon>
                                        <p>Harap isi bidang ini.</p>
                                    </div>
                                )}
                            </div>

                            <div className="input__flex">
                                <label
                                    htmlFor="password"
                                    className="input__groups-label"
                                >
                                    Password
                                </label>
                                <div className="input__groups">
                                    <div className="input__groups-icon">
                                        <LockClosedIcon className="input__icon" />
                                    </div>
                                    <input
                                        id="password"
                                        type={
                                            changePassword ? "password" : "text"
                                        }
                                        className="input__group input-password" //Add "invalid" class for error
                                        onChange={(e) =>
                                            setPassword(e.target.value)
                                        }
                                        required
                                        autoFocus
                                        autoComplete="off"
                                        placeholder="Ketikan password"
                                        {...register("password", {
                                            required: true,
                                        })}
                                    />
                                    <div
                                        onClick={() => {
                                            setChangePassword(changeIcon);
                                        }}
                                        className="input__groups-icon-password"
                                    >
                                        {changeIcon ? (
                                            <EyeIcon className="input__icon" />
                                        ) : (
                                            <EyeOffIcon className="input__icon" />
                                        )}
                                    </div>
                                </div>
                                {/* Invalid Input*/}
                                {errors.password && (
                                    <div className="invalid-feedback">
                                        <ExclamationIcon
                                            className={`w-[12px]`}
                                        ></ExclamationIcon>
                                        <p>Harap isi bidang ini.</p>
                                    </div>
                                )}
                            </div>

                            <div className="login__remember">
                                <label className="checkbox__remember">
                                    <input
                                        type="checkbox"
                                        className="input__checkbox"
                                    />
                                    Remember Me
                                </label>

                                <span className="forgot__pass">
                                    <a href="login">Forget Your Password?</a>
                                </span>
                            </div>

                            <button
                                disabled={isLoading}
                                onClick={() => setLogin(true)}
                                type="submit"
                                className={`button btn-login ${
                                    isLoading && "!bg-red-800"
                                }`}
                            >
                                Login
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </>
    );
};

export default Login;
