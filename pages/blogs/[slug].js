import axios from "axios";
import { parseCookies } from "nookies";
import BlogsDetailComponent from "../../components/Blogs/BlogsDetailComponent";
import HeadComponent from "../../components/Layouts/HeadComponent";
import request from "../../utils/request";

export default function blogDetail({ blog, latest_blogs, trending_blogs }) {
    return (
        <>
            <HeadComponent title={`Blogs Detail`} />
            <BlogsDetailComponent
                blog={blog}
                latest_blogs={latest_blogs}
                trending_blogs={trending_blogs}
            />
        </>
    );
}

export async function getServerSideProps(context) {
    const token = `Bearer ${parseCookies(context, 'token').token}`

    const detail = await axios.get(`${request.getAllBlog + '/' + context.params.slug}`, {
        headers: {
            'Authorization': token
        }
    })

    const latest = await axios.get(`${request.getAllBlog}`, {
        headers: {
            'Authorization': token
        }
    })

    const trending = await axios.get(`${request.getAllBlogTrending}`, {
        headers: {
            'Authorization': token
        }
    })

    const blog = await detail.data.blog
    const latest_blogs = await latest.data.data.blogs
    const trending_blogs = await trending.data.data.blogs

    return {
        props: {
            blog,
            latest_blogs,
            trending_blogs
        }
    }
}
