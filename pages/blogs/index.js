
import axios from "../../lib/axios"; 
import HeadComponent from "../../components/Layouts/HeadComponent";
import BlogsComponent from "../../components/Blogs/BlogsComponent";
import request from "../../utils/request";
import { parseCookies } from "nookies";
// import {requests, axios} from "../../lib/axios"

export default function blogs({ blogs }) {
    return (
        <>
            <HeadComponent title="Blog" />
            <BlogsComponent blogs={blogs} />
        </>
    );
}

export async function getServerSideProps (context) {
    const res = await axios.get(`${request.getAllBlog}`, {
        headers: {
            'Authorization': `Bearer ${parseCookies(context, 'token').token}`
        }
    })

    const blogs = await res.data.data.blogs

    return {
        props: {
            blogs

        }
    }
}
