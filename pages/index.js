import Feed from "../components/Feeds/Feed";
import HeadComponent from "../components/Layouts/HeadComponent";
import { parseCookies, setCookie, destroyCookie } from "nookies";
import axios from "axios";
import { userData } from "../atoms/atomState";
import { contentFeeds } from "../atoms/atomState";
import { useRecoilState } from "recoil";
import { useEffect, useState } from "react";
import request from "../utils/request";

const Home = () => {
    const [feeds] = useState([])
    return (
        <>
            <HeadComponent title={"News Feed"}></HeadComponent>
            <Feed content={feeds}/>
        </>
    );
};

// export async function getServerSideProps(context) {
//     const response = await axios.post(request.fetchContentFeeds,
//         { nik: parseCookies(context, "nik").nik },
//         {
//             headers: {
//                 "X-Requested-With": "XMLHttpRequest",
//                 Authorization: `Bearer ${parseCookies(context, "token").token}`,
//             },
//         }
//     );
//     const feeds = response.data.data;

//     return {
//         props: {
//             feeds,
//         },
//     };
// }

export default Home;
