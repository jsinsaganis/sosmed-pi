import HeadComponent from "../components/Layouts/HeadComponent";
import NotificationsAllComponent from "../components/Notifications/NotificationsAllComponent";

function notifications() {
    return (
        <>
            <HeadComponent title={`Notifications`} />
            <NotificationsAllComponent />
        </>
    );
}

export default notifications;
