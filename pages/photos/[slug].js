import axios from "axios";
import Image from "next/image";
import { parseCookies } from "nookies";
import { useState } from "react";
import HeadComponent from "../../components/Layouts/HeadComponent";
import ModalPhotos from "../../components/Photo/ModalPhotos";
import request from "../../utils/request";

function cn(...classes) {
    return classes.filter(Boolean).join(" ");
}

export async function getServerSideProps(context) {
    const res = await axios.get(request.getPhotoByAlbumId+context.params.slug, {
        headers: {
            Authorization: `Bearer ${parseCookies(context, "token").token}`
        }
    }).then(res => res.data.photo_album)

    return {
        props: {
            list: res
        }
    }
}

const Detail = ({ list }) => {
    const [isLoading, setLoading] = useState(true);
    const [openModal, setOpenModal] = useState(false);

    return (
        <>
            <HeadComponent title="Gallery" />
            <div className="">
                <ul className="grid grid-cols-1 sm:grid-cols-2 xl:grid-cols-3 gap-2 w-full">
                    <ModalPhotos list={list.photos.photos} />
                </ul>
            </div>
        </>
    );
};

export default Detail;
