import axios from "axios";
import Image from "next/image";
import Link from "next/link";
import { parseCookies } from "nookies";
import React, { useState } from "react";
import useSWR from "swr";
import HeadComponent from "../../components/Layouts/HeadComponent";
import request from "../../utils/request";

function cn(...classes) {
  return classes.filter(Boolean).join(" ");
}

export default function Album({album}) {
    const [isLoading, setLoading] = useState(true);

    return (
        <div className="min-h-[90vh]">
            <HeadComponent title="Gallery" />
            <div className="grid gap-6 grid-cols-1 sm:grid-cols-2 xl:grid-cols-3 ">
                {album && album.photo_albums.map((album) => (
                        <article
                            key={album.id}
                            className="bg-white relative rounded-2xl overflow-hidden md:min-w-[240px] shadow-md hover:opacity-70 transform duration-200 ease-out"
                        >
                            <Link href={`photos/${album.slug}`}>
                                <a>
                                        <div className="grid grid-cols-2 grid-rows-2 gap-2 h-64">
                                            {album.thumbnails.map((photo, index) => (
                                              
                                                    <div
                                                        key={index}
                                                        className={`relative ${
                                                            album.thumbnails.length >
                                                            2
                                                                ? "first:row-span-2"
                                                                : album.thumbnails.length ==
                                                                  2
                                                                ? "row-span-2"
                                                                : "row-span-2 col-span-2"
                                                        }`}
                                                    >
                                                        <Image
                                                            alt="album poto"
                                                            layout="fill"
                                                            src={photo.photo}
                                                            objectFit="cover"
                                                            className={cn(
                                                              "group-hover:opacity-75 duration-700 ease-in-out",
                                                              isLoading
                                                                  ? "grayscale blur-2xl scale-110"
                                                                  : "grayscale-0 blur-0 scale-100"
                                                          )}
                                                          onLoadingComplete={() => setLoading(false)}
                                                        />
                                                    </div>
                                                ))}
                                        </div>
                                    <section className="p-6">
                                        <h2 className="font-bold mb-2 text-sm">
                                            {album.name}
                                        </h2>
                                        <p className="flex items-center gap-x-2 mb-1 text-xs">
                                            {album.total_photos} photo{album.total_photos > 1 ? <>s</>: <></>}
                                            <span className="inline-block w-1.5 h-1.5 rounded-full bg-slate-400"></span>
                                            {album.created_at}
                                        </p>
                                        <span className="text-[#818598] text-xs">
                                            {album.title}
                                        </span>
                                    </section>
                                </a>
                            </Link>
                        </article>
                    ))}
            </div>
        </div>
    );
}

export async function getServerSideProps (context) {
  const res = await axios.get(`${request.getPhotoAlbum}`, {
      headers: {
          'Authorization': `Bearer ${parseCookies(context, 'token').token}`
      }
  })

  const album = await res.data.data

  return {
      props: {
          album

      }
  }
}
