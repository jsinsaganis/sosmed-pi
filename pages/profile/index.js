import { PencilAltIcon } from "@heroicons/react/solid";
import axios from "axios";
import Image from "next/image";
import { parseCookies } from "nookies";
import React from "react";
import useSWR from "swr";
import ProfileTab from "../../components/Profile/ProfileTab";
import request from "../../utils/request";

export default function profile() {
    

    return (
        <div className="w-full space-y-6">
            <div className="bg-white w-full rounded-xl h-fit overflow-hidden outlined">
                <div className="relative w-full h-52">
                    <Image
                        src="/sample-banner2.webp"
                        layout="fill"
                        objectFit="cover"
                        priority
                        alt="banner"
                    />
                    <div className="absolute bottom-10 left-6 w-24 h-24 rounded-xl border-4 border-white overflow-hidden">
                        <Image
                            className="rounded-md"
                            src="/sample-baner.png"
                            layout="fill"
                            objectFit="cover"
                            alt="banner"
                        />
                    </div>
                    <a
                        href="#"
                        className="flex items-center space-x-1 py-2 px-3 absolute bg-[#EBEEFF] rounded-xl bottom-10 right-8"
                    >
                        <PencilAltIcon className="w-4 h-4 text-[#001A8F]" />
                        <span className="text-[#001A8F] text-sm font-bold">
                            Edit Cover
                        </span>
                    </a>
                </div>
                <div className="space-y-2 relative mt-6 px-6 pb-4">
                    <div className="sm:absolute p-4 right-20 -top-3 flex justify-center space-x-8">
                        <div className="flex flex-col space-y-2 justify-center items-center">
                            <p className="font-bold">1.2K</p>
                            <span className="font-semibold capitalize text-sm">
                                following
                            </span>
                        </div>
                        <div className="flex flex-col space-y-2 justify-center items-center">
                            <p className="font-bold">1.29B</p>
                            <span className="font-semibold capitalize text-sm">
                                following
                            </span>
                        </div>
                    </div>
                    <h2 className="text-md font-bold flex flex-col md:flex-row md:items-center gap-x-2">
                        Kaido The Kid
                        <span className="hidden md:inline-block w-1.5 h-1.5 rounded-full bg-slate-400"></span>
                        <span className="text-md text-slate-600 font-normal">
                            kaido@onepiece.inc
                        </span>
                    </h2>
                    <h3 className="text-sm font-semibold flex items-center gap-x-2">
                        Pirates Captain
                        <span className="inline-block w-1.5 h-1.5 rounded-full bg-slate-400"></span>
                        <span className="text-sm text-slate-600 font-normal">
                            1.000.000.000
                        </span>
                    </h3>
                    <p className="pt-3 text-slate-600 leading-6 text-sm">
                        Lorem ipsum dolor sit amet consectetur, adipisicing
                        elit. Obcaecati exercitationem nemo ratione cumque
                        laboriosam blanditiis est consequatur laborum delectus
                        atque! Hic, esse quaerat. Dolore, debitis non. Illum
                        fuga cupiditate minima.
                    </p>
                </div>
            </div>
            <ProfileTab />
        </div>
    );
}
