const BASE_URL =`http://192.168.188.232:8011`
const BASE_URL2 = `https://api-pismart-dev.pupuk-indonesia.com/oauth_api`
const BASE_URL_LOCAL = 'http://127.0.0.1:8000'

const request = {
    fetchContentFeeds: `${BASE_URL}/api/mobile/getFeeds`,
    postLike2: `${BASE_URL2}/api/mobile/postLike`,
    postLike: `${BASE_URL2}/api/mobile/postLike`,
    postFeed: `${BASE_URL2}/api/mobile/postFeeds`,
    showComment: `${BASE_URL2}/api/mobile/showComment/`,
    share: `${BASE_URL}/api/`,
    postComment2: `${BASE_URL2}/api/mobile/postComment`,
    postComment: `${BASE_URL2}/api/mobile/postComment`,
    getSkill: `${BASE_URL}/api/mobile/getSkill/`,
    storeSkill: `${BASE_URL}/api/mobile/storeSkill`,
    storeAboutUs: `${BASE_URL}/api/mobile/storeAboutUs`,
    getAboutUs: `${BASE_URL}/api/mobile/showAboutUs/`,
    getFeedLocal: `${BASE_URL2}/api/web/v1/feeds?page=`,
    getReels: `${BASE_URL2}/api/web/v1/reels `,
    getReelsById: `${BASE_URL2}/api/web/v1/reels-user/`,
    storeReels: `${BASE_URL2}/api/web/v1/reels`,
    getPhotoAlbum: `${BASE_URL2}/api/web/v1/photo-albums`,
    getPhotoByAlbumId: `${BASE_URL2}/api/web/v1/photo-albums/`,
    getPhotoById: `${BASE_URL2}/api/web/v1/photos/`,
    getAllBlog: `${BASE_URL2}/api/web/v1/blogs`,
    getAllBlogTrending: `${BASE_URL2}/api/web/v1/blogs-by-category/trending`,
    postPhotoComment: `${BASE_URL2}/api/web/v1/photo-comments`,
    getApps: `${BASE_URL2}/api/web/v1/apps`,
    filterApps: `${BASE_URL2}/api/web/v1/apps?search=`,
    getCategoryApps: `${BASE_URL2}/api/web/v1/app-categories`,
    getDetailApp: `${BASE_URL2}/api/web/v1/apps/`,
    postReview: `${BASE_URL2}/api/web/v1/app-reviews/`,
    postWorkspace: `${BASE_URL2}/api/web/v1/app-workspaces/`,
    getNotification: `${BASE_URL2}/api/web/v1/notifications/`,
}

export default request